<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');

    }

    public function get_admin_details() {
        return $this->db->get_where('users', array('role_id' => 1));
    }

    public function get_user($user_id = 0,$all_users=true) {
        if ($user_id > 0) {
            $this->db->where('id', $user_id);
        }
        $this->db->where('role_id', 2);
        if(!$all_users){
            $this->db->where('is_instructor', '0');
            $this->db->where('is_mentor','0');
        }
        return $this->db->get('users');
    }

    public function get_all_user($user_id = 0) {
        if ($user_id > 0) {
            $this->db->where('id', $user_id);
        }
        return $this->db->get('users');
    }

    public function add_user($is_instructor = false) {
        $validity = $this->check_duplication('on_create', $this->input->post('email'));
        if ($validity == false) {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }else {
            $data['first_name'] = html_escape($this->input->post('first_name'));
            $data['last_name'] = html_escape($this->input->post('last_name'));
            $data['email'] = html_escape($this->input->post('email'));
            $data['password'] = sha1(html_escape($this->input->post('password')));
            $social_link['facebook'] = html_escape($this->input->post('facebook_link'));
            $social_link['twitter'] = html_escape($this->input->post('twitter_link'));
            $social_link['linkedin'] = html_escape($this->input->post('linkedin_link'));
            $data['social_links'] = json_encode($social_link);
            $data['biography'] = $this->input->post('biography');
            $data['role_id'] = 2;
            $data['date_added'] = strtotime(date("Y-m-d H:i:s"));
            $data['wishlist'] = json_encode(array());
            $data['watch_history'] = json_encode(array());
            $data['status'] = 1;
            $data['qualification']  = $this->input->post('highest_qualification');
            $data['expertise_area']  = $this->input->post('expertise_area');

            // Add paypal keys
            $paypal_info = array();
            $paypal['production_client_id']  = html_escape($this->input->post('paypal_client_id'));
            $paypal['production_secret_key'] = html_escape($this->input->post('paypal_secret_key'));
            array_push($paypal_info, $paypal);
            $data['paypal_keys'] = json_encode($paypal_info);

            // Add Stripe keys
            $stripe_info = array();
            $stripe_keys = array(
                'public_live_key' => html_escape($this->input->post('stripe_public_key')),
                'secret_live_key' => html_escape($this->input->post('stripe_secret_key'))
            );
            array_push($stripe_info, $stripe_keys);
            $data['stripe_keys'] = json_encode($stripe_info);

            if ($is_instructor) {
                $data['is_instructor'] = 1;
            }

            $this->db->insert('users', $data);
            $user_id = $this->db->insert_id();
            $this->upload_user_image($user_id);
            $this->session->set_flashdata('flash_message', get_phrase('user_added_successfully'));
        }
    }

    public function check_duplication($action = "", $email = "", $user_id = "") {
        $duplicate_email_check = $this->db->get_where('users', array('email' => $email));

        if ($action == 'on_create') {
            if ($duplicate_email_check->num_rows() > 0) {
                return false;
            }else {
                return true;
            }
        }elseif ($action == 'on_update') {
            if ($duplicate_email_check->num_rows() > 0) {
                if ($duplicate_email_check->row()->id == $user_id) {
                    return true;
                }else {
                    return false;
                }
            }else {
                return true;
            }
        }
    }

    public function edit_user($user_id = "") { // Admin does this editing
        $validity = $this->check_duplication('on_update', $this->input->post('email'), $user_id);
        if ($validity) {
            $data['first_name'] = html_escape($this->input->post('first_name'));
            $data['last_name'] = html_escape($this->input->post('last_name'));
            $data['college'] = html_escape($this->input->post('college'));
            $data['course'] = html_escape($this->input->post('course'));
            $data['semester'] = html_escape($this->input->post('semester'));
            $data['qualification'] = html_escape($this->input->post('highest_qualification'));
            $data['expertise_area'] = html_escape($this->input->post('expertise_area'));


            if (isset($_POST['email'])) {
                $data['email'] = html_escape($this->input->post('email'));
            }
            $social_link['facebook'] = html_escape($this->input->post('facebook_link'));
            $social_link['twitter'] = html_escape($this->input->post('twitter_link'));
            $social_link['linkedin'] = html_escape($this->input->post('linkedin_link'));
            $data['social_links'] = json_encode($social_link);
            $data['biography'] = $this->input->post('biography');
            $data['title'] = html_escape($this->input->post('title'));
            $data['last_modified'] = strtotime(date("Y-m-d H:i:s"));

            // Update paypal keys
            $paypal_info = array();
            $paypal['production_client_id']  = html_escape($this->input->post('paypal_client_id'));
            $paypal['production_secret_key'] = html_escape($this->input->post('paypal_secret_key'));
            array_push($paypal_info, $paypal);
            $data['paypal_keys'] = json_encode($paypal_info);
            // Update Stripe keys
            $stripe_info = array();
            $stripe_keys = array(
                'public_live_key' => html_escape($this->input->post('stripe_public_key')),
                'secret_live_key' => html_escape($this->input->post('stripe_secret_key'))
            );
            array_push($stripe_info, $stripe_keys);
            $data['stripe_keys'] = json_encode($stripe_info);
            $this->db->where('id', $user_id);
            $this->db->update('users', $data);
            $this->upload_user_image($user_id);
            $this->session->set_flashdata('flash_message', get_phrase('user_update_successfully'));
        }else {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }

        $this->upload_user_image($user_id);
    }
    public function delete_user($user_id = "") {
        $this->db->where('id', $user_id);
        $this->db->delete('users');
        $this->session->set_flashdata('flash_message', get_phrase('user_deleted_successfully'));
    }

    public function unlock_screen_by_password($password = "") {
        $password = sha1($password);
        return $this->db->get_where('users', array('id' => $this->session->userdata('user_id'), 'password' => $password))->num_rows();
    }

    public function register_user($data) {
        $this->db->insert('users', $data);
        return $this->db->insert_id();
    }

    public function my_courses($user_id = "") {
        if ($user_id == "") {
            $user_id = $this->session->userdata('user_id');
        }
        return $this->db->get_where('enrol', array('user_id' => $user_id));
    }

    public function my_student_courses($user_id = "",$status='') {
        $filter = array();
        if ($user_id == "") {
            $user_id = $this->session->userdata('user_id');
            $filter['user_id'] = $user_id;
        }
        if ($status != "") {
            $filter['status'] = $status;
        }
        return $this->db->get_where('student_course', $filter);
    }

    public function my_enrol_student_courses($user_id = "",$course_id='',$status='') {
        $filter = array();
        if ($user_id == "") {
            $user_id = $this->session->userdata('user_id');
            $this->db->where('student_enrol.user_id',$user_id);
        }
        if ($status != "") {
            $this->db->where('student_course.status',$status);
        }
        if ($course_id != "") {
            $this->db->where('student_course.id',$course_id);
        }
        $this->db->select('student_enrol.document as student_document,student_course.*');
        $this->db->join('student_enrol','student_enrol.course_id = student_course.id');
        return $this->db->get('student_course');
    }

    public function get_all_enrol_student_list($course_id=''){
        $this->db->select('CONCAT(users.first_name," ",users.last_name) as student_name');
        $this->db->where('student_enrol.course_id',$course_id);
        $this->db->join('users','users.id = student_enrol.user_id');
        return $this->db->get('student_enrol');
    }

    public function upload_user_image($user_id) {
        if (isset($_FILES['user_image']) && $_FILES['user_image']['name'] != "") {
            move_uploaded_file($_FILES['user_image']['tmp_name'], 'uploads/user_image/'.$user_id.'.jpg');
            $this->session->set_flashdata('flash_message', get_phrase('user_update_successfully'));
        }
    }

    public function update_account_settings($user_id) {
        $validity = $this->check_duplication('on_update', $this->input->post('email'), $user_id);
        if ($validity) {
            if (!empty($_POST['current_password']) && !empty($_POST['new_password']) && !empty($_POST['confirm_password'])) {
                $user_details = $this->get_user($user_id)->row_array();
                $current_password = $this->input->post('current_password');
                $new_password = $this->input->post('new_password');
                $confirm_password = $this->input->post('confirm_password');
                if ($user_details['password'] == sha1($current_password) && $new_password == $confirm_password) {
                    $data['password'] = sha1($new_password);
                }else {
                    $this->session->set_flashdata('error_message', get_phrase('mismatch_password'));
                    return;
                }
            }
            $data['email'] = html_escape($this->input->post('email'));
            $this->db->where('id', $user_id);
            $this->db->update('users', $data);
            $this->session->set_flashdata('flash_message', get_phrase('updated_successfully'));
        }else {
            $this->session->set_flashdata('error_message', get_phrase('email_duplication'));
        }
    }

    public function change_password($user_id) {
        $data = array();
        if (!empty($_POST['current_password']) && !empty($_POST['new_password']) && !empty($_POST['confirm_password'])) {
            $user_details = $this->get_all_user($user_id)->row_array();
            $current_password = $this->input->post('current_password');
            $new_password = $this->input->post('new_password');
            $confirm_password = $this->input->post('confirm_password');

            if ($user_details['password'] == sha1($current_password) && $new_password == $confirm_password) {
                $data['password'] = sha1($new_password);
            }else {
                $this->session->set_flashdata('error_message', get_phrase('mismatch_password'));
                return;
            }
        }

        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
        $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
    }


    public function get_instructor($id = 0,$ignore_current_instructor=false) {
        if ($ignore_current_instructor && $this->session->userdata('is_instructor')) {
            $this->db->where('id != '.$this->session->userdata('user_id'));
        }
        if ($id > 0) {
            return $this->db->get_where('users', array('id' => $id, 'is_instructor' => 1));
        }else{
            return $this->db->get_where('users', array('is_instructor' => 1));
        }
    }

    public function get_mentor($id = 0,$ignore_current_mentor=false) {
        if ($ignore_current_mentor && $this->session->userdata('is_mentor')) {
            $this->db->where('id != '.$this->session->userdata('user_id'));
        }
        if ($id > 0) {
            return $this->db->get_where('users', array('id' => $id, 'is_mentor' => '1'));
        }else{
            return $this->db->get_where('users', array('is_mentor' => '1'));
        }
    }

    public function get_number_of_active_courses_of_instructor($instructor_id) {
        $checker = array(
          'user_id' => $instructor_id,
          'status'  => 'active'
        );
        $result = $this->db->get_where('course', $checker)->num_rows();
        return $result;
    }

    public function get_user_image_url($user_id) {

         if (file_exists('uploads/user_image/'.$user_id.'.jpg'))
             return base_url().'uploads/user_image/'.$user_id.'.jpg';
        else
            return base_url().'uploads/user_image/placeholder.png';
    }
    public function get_instructor_student_list() {
    	$ses_user_id = $this->session->userdata('user_id');
    	if($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor')) {
    		// when instructor or mentor logged in the then fetch all assign instructors of courses > Get all users in which current user is assigned.
    		$final_user_id_list = $all_courses_id = array();
    		if($this->session->userdata('is_instructor')) {
	    		$this->db->select('add_instructor.user_id,course.id,course.user_id as owner_id');
	    		$this->db->where('add_instructor.user_id',$ses_user_id);
	    		$this->db->join('add_instructor','add_instructor.course_id = course.id');
	    		$course_list_obj = $this->db->get('course');

	    		if (!empty($course_list_obj) && $course_list_obj->num_rows() > 0) {
	    			$course_list = $course_list_obj->result_array();
	    			foreach ($course_list as $key => $value) {
	    				$final_user_id_list[] = $value['user_id'];
	    				$final_user_id_list[] = $value['owner_id'];
	    				$all_courses_id[] = $value['course_id'];
	    			}
	    		}
	    		// Get all users who are assigned by current user
	    		$this->db->select('add_instructor.user_id,course.id');
	    		$this->db->where('course.user_id',$ses_user_id);
	    		$this->db->join('add_instructor','add_instructor.course_id = course.id','left');
	    		$course_list_obj = $this->db->get('course');

	    		if (!empty($course_list_obj) && $course_list_obj->num_rows() > 0) {
	    			$course_list = $course_list_obj->result_array();
	    			foreach ($course_list as $key => $value) {
	    				$final_user_id_list[] = $value['user_id'];
	    				$all_courses_id[] = $value['id'];
	    			}
	    		}
    		}
    		if($this->session->userdata('is_mentor')) {
	    		$this->db->select('add_mentor.user_id,course.id,course.user_id as owner_id');
	    		$this->db->where('add_mentor.user_id',$ses_user_id);
	    		$this->db->where('add_mentor.status IS NULL');
	    		$this->db->join('add_mentor','add_mentor.course_id = course.id');
	    		$course_list_obj = $this->db->get('course');
	    		if (!empty($course_list_obj) && $course_list_obj->num_rows() > 0) {
	    			$course_list = $course_list_obj->result_array();
	    			foreach ($course_list as $key => $value) {
	    				$final_user_id_list[] = $value['user_id'];
	    				$final_user_id_list[] = $value['owner_id'];
	    				$all_courses_id[] = $value['course_id'];
	    			}
	    		}
	    		// Get all users who are assigned by current user
	    		$this->db->select('add_mentor.user_id,course.id');
	    		$this->db->where('course.user_id',$ses_user_id);
	    		$this->db->where('add_mentor.status IS NULL');
	    		$this->db->join('add_mentor','add_mentor.course_id = course.id','left');
	    		$course_list_obj = $this->db->get('course');

	    		if (!empty($course_list_obj) && $course_list_obj->num_rows() > 0) {
	    			$course_list = $course_list_obj->result_array();
	    			foreach ($course_list as $key => $value) {
	    				$final_user_id_list[] = $value['user_id'];
	    				$all_courses_id[] = $value['id'];
	    			}
	    		}
	    	}
    		// fetch students who got enrolled in his/her courses
    		$this->db->where_in('course_id',$all_courses_id);
    		$student_list_obj = $this->db->get('enrol');
    		if (!empty($student_list_obj) && $student_list_obj->num_rows() > 0) {
    			$student_list = $student_list_obj->result_array();
    			foreach ($student_list as $key => $value) {
    				$final_user_id_list[] = $value['user_id'];
    			}
    		}

    		// fetch student courses in which student has invited the logged in user
    		$all_student_courses_id = array();

    		$this->db->where('user_id',$ses_user_id);
    		$this->db->where('status','approved');
    		$invited_courses_obj = $this->db->get('add_mentor');
    		if (!empty($invited_courses_obj) && $invited_courses_obj->num_rows() > 0) {
    			$invited_courses = $invited_courses_obj->result_array();
    			foreach ($invited_courses as $key => $value) {
    				$all_student_courses_id[] = $value['course_id'];
    			}
    		}
    		$this->db->where('email',$this->session->userdata('email'));
    		$this->db->where('status','approved');
    		$invited_courses_obj = $this->db->get('instructor_requested_projects');
    		if (!empty($invited_courses_obj) && $invited_courses_obj->num_rows() > 0) {
    			$invited_courses = $invited_courses_obj->result_array();
    			foreach ($invited_courses as $key => $value) {
    				$all_student_courses_id[] = $value['course_id'];
    			}
    		}
    		if (!empty($all_student_courses_id)) {
    			// Fetch student course owner and invited student and invited instructors and mentors
				$this->db->select('requested_projects.email,requested_projects.requested_user_id,instructor_requested_projects.email as instructor_email,requested_projects.status,add_mentor.user_id');
				$this->db->where_in('requested_projects.course_id',$all_student_courses_id);
				$this->db->join('instructor_requested_projects','instructor_requested_projects.course_id = requested_projects.course_id AND instructor_requested_projects.status = "approved"','left');
				$this->db->join('add_mentor','add_mentor.course_id = requested_projects.course_id AND add_mentor.status = "approved"','left');
				$student_list_obj = $this->db->get('requested_projects');
				if (!empty($student_list_obj) && $student_list_obj->num_rows() > 0) {
	    			$student_list = $student_list_obj->result_array();
	    			$instructor_student_email_list = array();
	    			foreach ($student_list as $key => $value) {
	    				if ($value['status'] == 'approved') {
	    					$instructor_student_email_list[] = $value['email'];
	    				}
	    				$instructor_student_email_list[] = $value['instructor_email'];
	    				$final_user_id_list[] = $value['requested_user_id'];
	    				$final_user_id_list[] = $value['user_id'];
	    			}
	    			$instructor_student_email_list = array_filter($instructor_student_email_list);
	    			if (!empty($instructor_student_email_list)) {
	    				$this->db->where_in('email', $instructor_student_email_list);
			            $student_user_ids_obj = $this->db->get('users');
			            if (!empty($student_user_ids_obj) && $student_user_ids_obj->num_rows() > 0) {
			            	$student_user_ids =  $student_user_ids_obj->result_array();
			            	foreach ($student_user_ids as $key1 => $value1) {
			            		$final_user_id_list[] = $value1['id'];
			            	}
			            }
	    			}
	    		}
    		}
    		$final_user_id_list = array_filter(array_unique($final_user_id_list));
	        $this->db->where_in('id', $final_user_id_list);
	        $this->db->where('id != '.$ses_user_id);
	        $query_result = $this->db->get('users');
	        if (empty($query_result)) {
	            $query_result = $this->get_admin_details();
	        }
	        return $query_result;
    	}
        $query1 = $this->db->get_where('enrol', array('user_id' => $ses_user_id))->result_array();
        $course_ids = array();
        $query_result = array();
        foreach ($query1 as $row1) {
            if (!in_array($row1['course_id'], $course_ids) && $row1['course_id'] != "") {
                array_push($course_ids, $row1['course_id']);
            }
        }
        $final_user_id_list = array();
        if (count($course_ids) > 0) {

            // echo("<br><br>");
            // echo("---- Course Ids ---");
            // print_r(json_encode($course_ids));
            // echo("<br><br>");

        	// Get all instrtors and mentor ids in which loggin user is enrolled.
        	$this->db->select('course.user_id as user_id,add_instructor.user_id as instructor_id,add_mentor.user_id as mentor_id');
            $this->db->where_in('course.id', $course_ids);
            $this->db->join('add_instructor','course.id = add_instructor.course_id','left');
            $this->db->join('add_mentor','course.id = add_mentor.course_id AND add_mentor.status IS NULL','left');
            $user_ids = $this->db->get('course')->result_array();

            foreach ($user_ids as $key => $value) {
            	$final_user_id_list[] = $value['user_id'];
            	$final_user_id_list[] = $value['instructor_id'];
            	$final_user_id_list[] = $value['mentor_id'];
            }
            // echo("<br><br>");
            // echo("---- Get all instrtors and mentor ids ---");
            // print_r(json_encode($final_user_id_list));
            // echo("<br><br>");
            // Get all students ids which are added during group purchase.
            $this->db->where_in('course_id', $course_ids);
            $this->db->where('email',$this->session->userdata('email'));
            $group_owner_ids_obj = $this->db->get('temp_group_members');

            if(!empty($group_owner_ids_obj) && $group_owner_ids_obj->num_rows() > 0) {
            	$email_ids_obj = $group_owner_ids_obj->result_array();
	            $owner_ids = array();
	            foreach ($email_ids_obj as $row3) {
		            if (!in_array($row3['user_id'], $owner_ids) && $row3['user_id'] != "") {
                        array_push($owner_ids, $row3['user_id']);
                        $final_user_id_list[] = $row3['user_id'];
		            }
		        }
	    	}
            $owner_ids[] = $ses_user_id;// Fetch members in which logged in user is project owner
            // echo("<br><br>");
            // echo("---- Get all students ids ---");
            // print_r($owner_ids);
            // echo("<br><br>");
            

	        $this->db->where_in('course_id', $course_ids);
	        $this->db->where_in('user_id', $owner_ids);
            $email_ids_obj = $this->db->get('temp_group_members')->result_array();
            $email_list = array();
            if(!empty($email_ids_obj)) {
            	//$email_ids_obj = $email_ids_obj->result_array();
	            foreach ($email_ids_obj as $row4) {
		            if (!in_array($row4['email'], $owner_ids) && $row4['email'] != "") {
		                array_push($email_list, $row4['email']);
		            }
		        }
	    	}
	        // All user ids whose are group in projects in which current user is involved.
	        $this->db->where_in('email', $email_list);
            $student_group_user_ids_obj = $this->db->get('users');
            if (!empty($student_group_user_ids_obj) && $student_group_user_ids_obj->num_rows() > 0) {
            	$student_group_user_ids =  $student_group_user_ids_obj->result_array();
            	foreach ($student_group_user_ids as $key1 => $value1) {
            		$final_user_id_list[] = $value1['id'];
            	}
            }
        }
        // echo("<br><br>");
        //     echo("---- Added by this user during Groupd Purchase Temp table ---");
        //     print_r(json_encode($final_user_id_list));
        //     echo("<br><br>");
        //  Get mentor ids of student projects
        $this->db->where('email',$this->session->userdata('email'));
        $this->db->or_where('requested_user_id',$this->session->userdata('user_id'));
        $my_enrol_courses_obj = $this->db->get('requested_projects');
        if (!empty($my_enrol_courses_obj) && $my_enrol_courses_obj->num_rows() > 0) {
        	$my_enrol_courses =  $my_enrol_courses_obj->result_array();
        }
        $student_enrol_course_ids = array();
        $enrol_email_list = array();
        if (!empty($my_enrol_courses)) {
	        foreach ($my_enrol_courses as $row5) {
	            if (!in_array($row5['course_id'], $student_enrol_course_ids) && $row5['course_id'] != "" && $row5['status'] == 'approved') {
	                array_push($student_enrol_course_ids, $row5['course_id']);
	            }
	            if (!in_array($row5['email'], $student_enrol_course_ids) && $row5['email'] != ""&& $row5['status'] == 'approved') {
	                array_push($enrol_email_list, $row5['email']);
	            }
	        }
    	}
    	// get mentor and project owner
        $this->db->select('student_course.user_id,add_mentor.user_id as mentor_id,student_course.id');
        if(!empty($student_enrol_course_ids))
        	$this->db->where_in('student_course.id', $student_enrol_course_ids);

        $this->db->or_where('student_course.user_id', $ses_user_id);
        $this->db->join('add_mentor','student_course.id = add_mentor.course_id AND add_mentor.status = "approved"','left');
        $all_student_enrol_user_ids_obj = $this->db->get('student_course');	

        if (!empty($all_student_enrol_user_ids_obj) && $all_student_enrol_user_ids_obj->num_rows() > 0) {
        	$all_student_enrol_user_ids =  $all_student_enrol_user_ids_obj->result_array();
        	foreach ($all_student_enrol_user_ids as $key2 => $value2) {
        		$final_user_id_list[] = $value2['user_id'];
        		$final_user_id_list[] = $value2['mentor_id'];
        	}
        }
        
        if(!empty($student_enrol_course_ids)) {
        	// Get invited instructor ids
        	$this->db->where_in('course_id', $student_enrol_course_ids);
        	$instructor_emails_obj = $this->db->get('instructor_requested_projects');
        	if (!empty($instructor_emails_obj) && $instructor_emails_obj->num_rows() > 0) {
        		$instructor_emails = $instructor_emails_obj->result_array();
        		foreach ($instructor_emails as $row6) {
	        		if (!in_array($row6['email'], $enrol_email_list)) {
		                array_push($enrol_email_list, $row6['email']);
		            }
		        }
        	}
			// Get other invited students
        	$this->db->where_in('course_id', $student_enrol_course_ids);
        	$student_emails_obj = $this->db->get('requested_projects');
        	if (!empty($student_emails_obj) && $student_emails_obj->num_rows() > 0) {
        		$student_emails = $student_emails_obj->result_array();
        		foreach ($student_emails as $row6) {
	        		if (!in_array($row6['email'], $enrol_email_list)) {
		                array_push($enrol_email_list, $row6['email']);
		            }
		        }
        	}
        }
        $this->db->where_in('email', $enrol_email_list);
        $all_student_project_members_obj = $this->db->get('users');
        if (!empty($all_student_project_members_obj) && $all_student_project_members_obj->num_rows() > 0) {
        	$all_student_project_members =  $all_student_project_members_obj->result_array();
        	foreach ($all_student_project_members as $key3 => $value3) {
        		$final_user_id_list[] = $value3['id'];
        	}
        }        
        $final_user_id_list = array_filter(array_unique($final_user_id_list));
        $this->db->where_in('id', $final_user_id_list);
        $this->db->where('id != '.$ses_user_id);
        $query_result = $this->db->get('users');
        if (empty($query_result)) {
            $query_result = $this->get_admin_details();
        }
        return $query_result;
    }

    public function update_instructor_paypal_settings($user_id = '') {
        // Update paypal keys
        $paypal_info = array();
        $paypal['production_client_id'] = html_escape($this->input->post('paypal_client_id'));
        $paypal['production_secret_key'] = html_escape($this->input->post('paypal_secret_key'));
        array_push($paypal_info, $paypal);
        $data['paypal_keys'] = json_encode($paypal_info);
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }
    public function update_instructor_stripe_settings($user_id = '') {
        // Update Stripe keys
        $stripe_info = array();
        $stripe_keys = array(
            'public_live_key' => html_escape($this->input->post('stripe_public_key')),
            'secret_live_key' => html_escape($this->input->post('stripe_secret_key'))
        );
        array_push($stripe_info, $stripe_keys);
        $data['stripe_keys'] = json_encode($stripe_info);
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }

    // POST INSTRUCTOR APPLICATION FORM AND INSERT INTO DATABASE IF EVERYTHING IS OKAY
    public function post_instructor_application() {
        // FIRST GET THE USER DETAILS
        $user_details = $this->get_all_user($this->input->post('id'))->row_array();

        // CHECK IF THE PROVIDED ID AND EMAIL ARE COMING FROM VALID USER
        if ($user_details['email'] == $this->input->post('email')) {

            // GET PREVIOUS DATA FROM APPLICATION TABLE
            $previous_data = $this->get_applications($user_details['id'], 'user')->num_rows();
            // CHECK IF THE USER HAS SUBMITTED FORM BEFORE
            if($previous_data > 0) {
                $this->session->set_flashdata('error_message', get_phrase('already_submitted'));
                redirect(site_url('user/become_an_instructor'), 'refresh');
            }
            $data['user_id'] = $this->input->post('id');
            $data['address'] = $this->input->post('address');
            $data['phone'] = $this->input->post('phone');
            $data['message'] = $this->input->post('message');
            if (isset($_FILES['document']) && $_FILES['document']['name'] != "") {
                if (!file_exists('uploads/document')) {
                    mkdir('uploads/document', 0777, true);
                }
                $accepted_ext = array('doc', 'docs', 'pdf', 'txt', 'png', 'jpg', 'jpeg');
                $path = $_FILES['document']['name'];
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                if (in_array(strtolower($ext), $accepted_ext)) {
                    $document_custom_name = random(15).'.'.$ext;
                    $data['document'] = $document_custom_name;
                    move_uploaded_file($_FILES['document']['tmp_name'], 'uploads/document/'.$document_custom_name);
                }else{
                    $this->session->set_flashdata('error_message', get_phrase('invalide_file'));
                    redirect(site_url('user/become_an_instructor'), 'refresh');
                }

            }
            $this->db->insert('applications', $data);
            $this->session->set_flashdata('flash_message', get_phrase('application_submitted_successfully'));
            redirect(site_url('user/become_an_instructor'), 'refresh');
        }else{
            $this->session->set_flashdata('error_message', get_phrase('user_not_found'));
            redirect(site_url('user/become_an_instructor'), 'refresh');
        }
    }


    // GET INSTRUCTOR APPLICATIONS
    public function get_applications($id = "", $type = "") {
        if ($id > 0 && !empty($type)) {
            if ($type == 'user') {
                $applications = $this->db->get_where('applications', array('user_id' => $id));
                return $applications;
            }else {
                $applications = $this->db->get_where('applications', array('id' => $id));
                return $applications;
            }
        }else{
            $this->db->order_by("id", "DESC");
            $applications = $this->db->get_where('applications');
            return $applications;
        }
    }

    // GET APPROVED APPLICATIONS
    public function get_approved_applications() {
        $applications = $this->db->get_where('applications', array('status' => 1));
        return $applications;
    }

    // GET PENDING APPLICATIONS
    public function get_pending_applications() {
        $applications = $this->db->get_where('applications', array('status' => 0));
        return $applications;
    }

    //UPDATE STATUS OF INSTRUCTOR APPLICATION
    public function update_status_of_application($status, $application_id) {
        $application_details = $this->get_applications($application_id, 'application');
        if ($application_details->num_rows() > 0) {
            $application_details = $application_details->row_array();
            if ($status == 'approve') {
                $application_data['status'] = 1;
                $this->db->where('id', $application_id);
                $this->db->update('applications', $application_data);

                $instructor_data['is_instructor'] = 1;
                $this->db->where('id', $application_details['user_id']);
                $this->db->update('users', $instructor_data);

                $this->session->set_flashdata('flash_message', get_phrase('application_approved_successfully'));
                redirect(site_url('admin/instructor_application'), 'refresh');
            }else{
                $this->db->where('id', $application_id);
                $this->db->delete('applications');
                $this->session->set_flashdata('flash_message', get_phrase('application_deleted_successfully'));
                redirect(site_url('admin/instructor_application'), 'refresh');
            }
        }else{
            $this->session->set_flashdata('error_message', get_phrase('invalid_application'));
            redirect(site_url('admin/instructor_application'), 'refresh');
        }
    }
    function createGoogleSignUpInstantce($action='',$page_from='student'){
        include_once APPPATH . "libraries/google-api-php-client-master/src/Google/Client.php";
        include_once APPPATH . "libraries/google-api-php-client-master/src/Google/Service/Oauth2.php";

        // Store values in variables from project created in Google Developer Console
      //  $client_id = '881597685633-lpt4gkk2ipotljl6teuv2rqvjhdep1iq.apps.googleusercontent.com';
        $client_id = '194545429600-jfp2472ukug22vasr0dpbdsubt9e9rnq.apps.googleusercontent.com';
        $client_secret = '_8KikJoOKCnlPZpQ5cbQbAvs';
       // $client_secret = 'EYsWzCpM-E_a77PxFdQ4HiWB';
        $method_name = 'checkGoogleCode';
        $userData = array();
        if ($page_from == 'instructor') {
            $method_name = 'checkGoogleCodeForInstructor';
            $userData['is_instructor'] = '1';
        }elseif ($page_from == 'mentor') {
            $method_name = 'checkGoogleCodeForMentor';
            $userData['is_mentor'] = '1';
        }
        $redirect_uri = site_url('login/'.$method_name);
        $simple_api_key = 'AIzaSyC2sL1cDi2KPeEwf1wE7ox_NxaKWyt3juE';

        // Create Client Request to access Google API
        $client = new Google_Client();
        $client->setApplicationName("PHP Google OAuth Login Example");
        $client->setClientId($client_id);
        $client->setClientSecret($client_secret);
        $client->setRedirectUri($redirect_uri);
        $client->setDeveloperKey($simple_api_key);
        $client->addScope("https://www.googleapis.com/auth/userinfo.email");
        $client->addScope('profile');
		//$_SESSION['access_token'] = '';
        // Send Client Request
        $objOAuthService = new Google_Service_Oauth2($client);
        if ($action == 'get_user_data') {
            $code = $this->input->get('code');
            if (!empty($code)) {
                $client->authenticate($code);
                //$token = $client->fetchAccessTokenWithAuthCode($code);
                //if (!isset($token['error'])) {
                    $access_token = $client->getAccessToken();
                    $this->session->set_userdata('access_token',$client->getAccessToken());
                    $client->setAccessToken($access_token);
                    $userResponseData = $objOAuthService->userinfo->get();
                    $userData['email'] = $userResponseData->email;
                    $userData['first_name'] = $userResponseData->givenName;
                    $userData['last_name'] = $userResponseData->familyName;
                    $userData['status'] = '1';
                    $userData['date_added'] = strtotime(date("Y-m-d H:i:s"));
                    $userData['oauthid'] = $userResponseData->id;
                    $userData['oauth_provider'] = 'google';
                    $userData['role_id'] = 2;
                    $password = '';
                    $query = $this->db->get_where('users',array('email' => $userData['email']));
                    if ($query->num_rows() > 0) {
                        $row = $query->row();
                        $user_id = $row->id;
                        $password = $row->password;
                        $userData['role_id'] = $row->role_id;
                        $this->db->where('id', $user_id);
                        $this->db->update('users', $userData);
                    }else{
                        $userData['wishlist'] = json_encode(array());
                        $userData['watch_history'] = json_encode(array());
                        $this->register_user($userData);
                        $user_id = $this->db->insert_id();
                    }
                    $this->session->set_userdata('user_id', $user_id);
                    $this->session->set_userdata('role_id', $row->role_id);
                    $this->session->set_userdata('role', get_user_role('user_role', $user_id));
                    $this->session->set_userdata('name', $userData['first_name'].' '.$userData['last_name']);
                    $this->session->set_userdata('is_instructor', $row->is_instructor);
                    $this->session->set_userdata('user_login', '1');
                    if (empty($password)) {
                        redirect(site_url('home/new_password'), 'refresh');    
                    }else{
                        $this->session->set_flashdata('flash_message', get_phrase('welcome').' '.$this->session->userdata('name'));
                        redirect(site_url('home'), 'refresh');
                    }
                    
                //}
            }
            // Set Access Token to make Request
            $access_token_value = $this->session->userdata('access_token');
            if (!empty($access_token_value)) {
                $client->setAccessToken($access_token_value);
            }

            // Get User Data from Google and store them in $data
            if ($client->getAccessToken()) {
                $this->session->set_userdata('access_token',$client->getAccessToken());
            }
        }else{
            $authUrl = $client->createAuthUrl();
            return $authUrl;    
        }
    }

    public function createFBSignUpInstantce($action='',$page_from='student'){
        $this->load->library('facebook');
        $method_name = 'fbLogin';
        $userData = array();
        if ($page_from == 'instructor') {
            $method_name = 'fbLoginForInstructor';
            $userData['is_instructor'] = '1';
        }elseif ($page_from == 'mentor') {
            $method_name = 'fbLoginForMentor';
            $userData['is_mentor'] = '1';
        }
        $redirect_uri = site_url('login/'.$method_name);
        if($this->facebook->is_authenticated()){
            // Get user info from facebook
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');
            // Preparing data for database insertion
            $userData['oauth_provider'] = 'facebook';
            $userData['oauthid']  = !empty($fbUser['id'])?$fbUser['id']:'';
            $userData['first_name'] = !empty($fbUser['first_name'])?$fbUser['first_name']:'';
            $userData['last_name']  = !empty($fbUser['last_name'])?$fbUser['last_name']:'';
            $userData['email']      = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['status'] = '1';
            $userData['date_added'] = strtotime(date("Y-m-d H:i:s"));
            $userData['role_id'] = 2;
            
            $query = $this->db->get_where('users',array('email' => $userData['email']));
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $user_id = $row->id;
                $password = $row->password;
                $userData['role_id'] = $row->role_id;
                $this->db->where('id', $user_id);
                $this->db->update('users', $userData);
            }else{
                $userData['wishlist'] = json_encode(array());
                $userData['watch_history'] = json_encode(array());
                $this->register_user($userData);
                $user_id = $this->db->insert_id();
            }
            $this->session->set_userdata('user_id', $user_id);
            $this->session->set_userdata('role_id', $row->role_id);
            $this->session->set_userdata('role', get_user_role('user_role', $user_id));
            $this->session->set_userdata('name', $userData['first_name'].' '.$userData['last_name']);
            $this->session->set_userdata('is_instructor', $row->is_instructor);
            $this->session->set_userdata('user_login', '1');
            $this->facebook->destroy_session();
            if (empty($password)) {
                redirect(site_url('home/new_password'), 'refresh');    
            }else{
                $this->session->set_flashdata('flash_message', get_phrase('welcome').' '.$this->session->userdata('name'));
                redirect(site_url('home'), 'refresh');
            }
        }else{
            // Facebook authentication url
            return $this->facebook->login_url($redirect_uri);
        }
    }

    public function createlinkedinSignUpInstantce($action='',$page_from='student'){
        $this->load->library('linkedin');
        $method_name = 'linkedinLogin';
        $userData = array();
        if ($page_from == 'instructor') {
            $method_name = 'linkedinLoginForInstructor';
            $userData['is_instructor'] = '1';
        }elseif ($page_from == 'mentor') {
            $method_name = 'linkedinLoginForMentor';
            $userData['is_mentor'] = '1';
        }
        $redirect_uri = site_url('login/'.$method_name);

        $CI =& get_instance();
        $CI->config->load('linkedin');
        
        // Include the oauth client library

        $client = new oauth_client_class;
            $client->client_id = $CI->config->item('linkedin_api_key');
            $client->client_secret = $CI->config->item('linkedin_api_secret');
            $client->redirect_uri = $redirect_uri;
            $client->scope = $CI->config->item('linkedin_scope');
            $client->debug = 1;
            $client->debug_http = 1;
            $application_line = __LINE__;
            
            if(strlen($client->client_id) == 0 || strlen($client->client_secret) == 0){
                die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , '.
                    'create an application, and in the line '.$application_line.
                    ' set the client_id to Consumer key and client_secret with Consumer secret. '.
                    'The Callback URL must be '.$client->redirect_uri.'. Make sure you enable the '.
                    'necessary permissions to execute the API calls your application needs.');
            }
            $this->client = $client;
            
        // Get status and user info from session
        $oauthStatus = $this->session->userdata('oauth_status');
        $sessUserData = $this->session->userdata('userData');
        if((isset($_GET["oauth_init"]) && $_GET["oauth_init"] == 1) || (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) || (isset($_GET['code']) && isset($_GET['state']))){
            // Authenticate with LinkedIn
            if($this->linkedin->authenticate()){
                
                // Get the user account info
                $userInfo = $this->linkedin->getUserInfo();
                
                // Preparing data for database insertion
                $userData['oauth_provider'] = 'linkedin';
                $userData['oauthid']  = !empty($userInfo['account']->id)?$userInfo['account']->id:'';
                $userData['first_name'] = !empty($userInfo['account']->firstName->localized->en_US)?$userInfo['account']->firstName->localized->en_US:'';
                $userData['last_name']  = !empty($userInfo['account']->lastName->localized->en_US)?$userInfo['account']->lastName->localized->en_US:'';
                $userData['email']      = !empty($userInfo['email']->elements[0]->{'handle~'}->emailAddress)?$userInfo['email']->elements[0]->{'handle~'}->emailAddress:'';
            $userData['status'] = '1';
            $userData['date_added'] = strtotime(date("Y-m-d H:i:s"));
            $userData['role_id'] = 2;
            $password = '';
            $query = $this->db->get_where('users',array('email' => $userData['email']));
            
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $user_id = $row->id;
                $password = $row->password;
                $userData['role_id'] = $row->role_id;
                $this->db->where('id', $user_id);
                $this->db->update('users', $userData);
            }else{
                $userData['wishlist'] = json_encode(array());
                $userData['watch_history'] = json_encode(array());
                $this->register_user($userData);
                $user_id = $this->db->insert_id();
            }
            $this->session->set_userdata('user_id', $user_id);
            $this->session->set_userdata('role_id', $row->role_id);
            $this->session->set_userdata('role', get_user_role('user_role', $user_id));
            $this->session->set_userdata('name', $userData['first_name'].' '.$userData['last_name']);
            $this->session->set_userdata('is_instructor', $row->is_instructor);
            $this->session->set_userdata('user_login', '1');
            if (empty($password)) {
                redirect(site_url('home/new_password'), 'refresh');    
            }else{
                $this->session->set_flashdata('flash_message', get_phrase('welcome').' '.$this->session->userdata('name'));
                redirect(site_url('home'), 'refresh');
            }

            }else{
                 $data['error_msg'] = 'Error connecting to LinkedIn! try again later! <br/>'.$this->linkedin->client->error;
            }
        }elseif(!empty($_GET['error'])){
            redirect(site_url('login'));
        }else{
            // linkedin authentication url
            return $redirect_uri.'?oauth_init=1';
        }
    }

    public function add_invited_friend($data) {
        $this->db->insert('requested_projects', $data);
        return $this->db->insert_id();
    }

    public function add_master_invited_friend($data) {
        $this->db->insert('master_project_requests', $data);
        return $this->db->insert_id();
    }

    public function add_invited_instructor($data) {
        $this->db->insert('instructor_requested_projects', $data);
        return $this->db->insert_id();
    }

    public function get_requested_projects($email='',$course_id='',$table='requested_projects'){
        if(empty($email)) {
            $email = $this->session->userdata('email');
        }
        $this->db->where('email',$email);
        if(!empty($course_id)) {
            $this->db->where('course_id',$course_id);
        }
        return $this->db->get($table);
    }

    public function get_requested_projects_by_userid($id='',$course_id='',$table='instructor_requested_projects'){
        if(empty($id)) {
            $id = $this->session->userdata('user_id');
        }
        $this->db->where('requested_user_id',$id);
        if(!empty($course_id)) {
            $this->db->where('course_id',$course_id);
        }
        $this->db->order_by("id", "DESC");
        return $this->db->get($table);
    }

    public function get_group_course_discount_price($course_id,$total_members=1){
        $this->db->where('id',$course_id);
        $course_details = $this->db->get('course')->row_array();

        $price = $course_details['price'];

        $this->db->select('MAX(group_discount) as max_discount');
        $this->db->where('course_id',$course_id);
        $this->db->where('min_members <= '.$total_members);
        $this->db->where('max_members >= '.$total_members);
        $group_discount_details =  $this->db->get('group_discount')->row_array();
        $max_discount = $group_discount_details['max_discount'];

        if(empty($max_discount)){
            $max_discount = 1;
            $discounted_price = $group_course_price = ($price * $total_members);
        }else{
            $group_course_price = ($price * $total_members);
            $discount = ($group_course_price * $max_discount) / 100; 
            $discounted_price = $group_course_price - $discount;
        }
        return array($group_course_price,$discounted_price,$group_discount_details['max_discount']);   

    }
}
