<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lazyloaddata_model extends CI_Model {

  // constructor
	function __construct()
	{
		parent::__construct();
	}

  // Servre side testing
  function courses($limit, $start, $col, $dir, $filter_data,$table="course")
  {
  	if ($table == 'student_course' && !empty($filter_data['selected_mentor'])) {
    	$course_ids = $this->get_mentor_course_ids($filter_data['selected_mentor']);
    	if (!empty($course_ids)) {
    		$this->db->where('id IN ('.$course_ids.')');
    	}else{
        return false;
      }
    }
    if ($filter_data['selected_instructor_id'] != "all" && !empty($filter_data['selected_instructor_id'])) {
      $this->db->where('user_id',$filter_data['selected_instructor_id']);
      $courseData = $this->db->get('add_instructor')->result_array();
    }
    $this->db->limit($limit,$start);
    $this->db->order_by($col,$dir);

    // apply the filter data
    // check if the user is admin. Admin can not see the draft courses
    
    if (strtolower($this->session->userdata('role')) == 'admin') {
        $this->db->where("status !=", 'draft');
    }
    if ($filter_data['selected_category_id'] != 'all') {
      $this->db->where('sub_category_id', $filter_data['selected_category_id']);
    }
    if ($filter_data['selected_price'] != "all") {
      if ($filter_data['selected_price'] == "paid") {
        $this->db->where('is_free_course', null);
      }elseif ($filter_data['selected_price'] == "free") {
        $this->db->where('is_free_course', 1);
      }
    }
    if ($filter_data['selected_status'] != "all") {
      $this->db->where('status', $filter_data['selected_status']);
    }
    if (!empty($courseData)) {
        $course_ids = array();
        foreach ($courseData as $key => $value) {
            $course_ids[] = $value['course_id'];
        }
        $courseids = implode(",", $course_ids);
        $this->db->where('(id IN ('.$courseids.') OR user_id = '.$filter_data['selected_instructor_id'].')');        
    }elseif ($filter_data['selected_instructor_id'] != "all" && $table == 'course') {
        $this->db->where('user_id',$filter_data['selected_instructor_id']);
    }
    if (!empty($filter_data['selected_student']) && $table == 'student_course') {
      $this->db->where('user_id',$filter_data['selected_student']); 
    }
    $query = $this->db->get($table);
    if($query->num_rows() > 0)
    return $query->result();
    else
    return null;

  }

  function course_search($limit, $start, $search, $col, $dir, $filter_data,$table='course')
  {
  	if ($table == 'student_course' && !empty($filter_data['selected_mentor'])) {
    	$course_ids = $this->get_mentor_course_ids($filter_data['selected_mentor']);
    	if (!empty($course_ids)) {
    		$this->db->where('id IN ('.$course_ids.')');
    	}
    }
    if ($filter_data['selected_instructor_id'] != "all" && !empty($filter_data['selected_instructor_id'])) {
      $this->db->where('user_id',$filter_data['selected_instructor_id']);
      $courseData = $this->db->get('add_instructor')->result_array();
    }
    $this->db->like('title', $search);
    $this->db->limit($limit, $start);
    $this->db->order_by($col, $dir);
    // apply the filter data
    // check if the user is admin. Admin can not see the draft courses
    
    if (strtolower($this->session->userdata('role')) == 'admin') {
        $this->db->where("status !=", 'draft');
    }
    if ($filter_data['selected_category_id'] != 'all') {
      $this->db->where('sub_category_id', $filter_data['selected_category_id']);
    }
    if ($filter_data['selected_price'] != "all") {
      if ($filter_data['selected_price'] == "paid") {
        $this->db->where('is_free_course', null);
      }elseif ($filter_data['selected_price'] == "free") {
        $this->db->where('is_free_course', 1);
      }
    }
    if ($filter_data['selected_status'] != "all") {
      $this->db->where('status', $filter_data['selected_status']);
    }
    if (!empty($courseData)) {
        $course_ids = array();
        foreach ($courseData as $key => $value) {
            $course_ids[] = $value['course_id'];
        }
        $courseids = implode(",", $course_ids);
        $this->db->where('(id IN ('.$courseids.') OR user_id = '.$filter_data['selected_instructor_id'].')');        
    }elseif ($filter_data['selected_instructor_id'] != "all" && $table == 'course') {
        $this->db->where('user_id',$filter_data['selected_instructor_id']);
    }
    if ($table == 'student_course' && !empty($filter_data['selected_student'])) {
      $this->db->where('user_id',$filter_data['selected_student']);
    }
    $query = $this->db->get($table);
    if($query->num_rows() > 0)
    return $query->result();
    else
    return null;
  }

  function course_search_count($search,$table="course")
  {
    $query = $this
    ->db
    ->like('title', $search)
    ->get('course');

    return $query->num_rows();
  }

  function count_all_courses($filter_data = array(),$table='course') {
    // apply the filter data
    // check if the user is admin. Admin can not see the draft courses
  	if ($table == 'student_course' && !empty($filter_data['selected_mentor'])) {
    	$course_ids = $this->get_mentor_course_ids($filter_data['selected_mentor']);
    	if (!empty($course_ids)) {
    		$this->db->where('id IN ('.$course_ids.')');
    	}else{
        return false;
      }
    }
    if ($filter_data['selected_instructor_id'] != "all" && !empty($filter_data['selected_instructor_id'])) {
      $this->db->where('user_id',$filter_data['selected_instructor_id']);
      $courseData = $this->db->get('add_instructor')->result_array();
    }
    if (strtolower($this->session->userdata('role')) == 'admin') {
        $this->db->where("status !=", 'draft');
    }
    if ($filter_data['selected_category_id'] != 'all') {
      $this->db->where('sub_category_id', $filter_data['selected_category_id']);
    }
    if ($filter_data['selected_price'] != "all") {
      if ($filter_data['selected_price'] == "paid") {
        $this->db->where('is_free_course', null);
      }elseif ($filter_data['selected_price'] == "free") {
        $this->db->where('is_free_course', 1);
      }
    }
    if (!empty($courseData)) {
        $course_ids = array();
        foreach ($courseData as $key => $value) {
            $course_ids[] = $value['course_id'];
        }
        $courseids = implode(",", $course_ids);
        $this->db->where('(id IN ('.$courseids.') OR user_id = '.$filter_data['selected_instructor_id'].')');        
    }elseif ($filter_data['selected_instructor_id'] != "all" && $table == 'course') {
        $this->db->where('user_id',$filter_data['selected_instructor_id']);
    }
    if ($table == 'student_course' && !empty($filter_data['selected_student'])) {
      $this->db->where('user_id',$filter_data['selected_student']);
    }
    if ($filter_data['selected_status'] != "all") {
      $this->db->where('status', $filter_data['selected_status']);
    }
    $query = $this->db->get($table);
    return $query->num_rows();
  }

  public function get_mentor_course_ids($metor_id){
  	$this->db->where('user_id',$metor_id);
    $this->db->where('status','approved');
    $courseData = $this->db->get('add_mentor');
    if ($courseData->num_rows() > 0) {
      $mentor_courses = $courseData->result_array();
      $course_ids = array();
      foreach ($mentor_courses as $key => $value) {
          $course_ids[] = $value['course_id'];
      }
      return $courseids = implode(",", $course_ids);
    }else{
      return $courseids = '0';
    }
  }
}
