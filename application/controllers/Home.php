<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
        $this->load->database();
        $this->load->library('session');
        // $this->load->library('stripe');
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');

        // CHECK CUSTOM SESSION DATA
        $this->session_data();
    }

    public function index() {
        $this->home();
    }

    public function home() {
        $page_data['page_name'] = "home";
        $page_data['page_title'] = site_phrase('home');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function shopping_cart() {
        if (!$this->session->userdata('cart_items')) {
            $this->session->set_userdata('cart_items', array());
        }
        $page_data['page_name'] = "shopping_cart";
        $page_data['page_title'] = site_phrase('shopping_cart');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function courses() {
        if (!$this->session->userdata('layout')) {
            $this->session->set_userdata('layout', 'list');
        }
        $layout = $this->session->userdata('layout');
        $selected_category_id = "all";
        $selected_price = "all";
        $selected_level = "all";
        $selected_language = "all";
        $selected_rating = "all";
        // Get the category ids
        if (isset($_GET['category']) && !empty($_GET['category'] && $_GET['category'] != "all")) {
            $selected_category_id = $this->crud_model->get_category_id($_GET['category']);
        }

        // Get the selected price
        if (isset($_GET['price']) && !empty($_GET['price'])) {
            $selected_price = $_GET['price'];
        }

        // Get the selected level
        if (isset($_GET['level']) && !empty($_GET['level'])) {
            $selected_level = $_GET['level'];
        }

        // Get the selected language
        if (isset($_GET['language']) && !empty($_GET['language'])) {
            $selected_language = $_GET['language'];
        }

        // Get the selected rating
        if (isset($_GET['rating']) && !empty($_GET['rating'])) {
            $selected_rating = $_GET['rating'];
        }


        if ($selected_category_id == "all" && $selected_price == "all" && $selected_level == 'all' && $selected_language == 'all' && $selected_rating == 'all') {
            $this->db->where('status', 'active');
            $total_rows = $this->db->get('course')->num_rows();
            $config = array();
            $config = pagintaion($total_rows, 6);
            $config['base_url']  = site_url('home/courses/');
            $this->pagination->initialize($config);
            $this->db->where('status', 'active');
            $page_data['courses'] = $this->db->get('course', $config['per_page'], $this->uri->segment(3))->result_array();
        }else {
            $courses = $this->crud_model->filter_course($selected_category_id, $selected_price, $selected_level, $selected_language, $selected_rating);
            $page_data['courses'] = $courses;
        }

        $page_data['page_name']  = "courses_page";
        $page_data['page_title'] = site_phrase('courses');
        $page_data['layout']     = $layout;
        $page_data['selected_category_id']     = $selected_category_id;
        $page_data['selected_price']     = $selected_price;
        $page_data['selected_level']     = $selected_level;
        $page_data['selected_language']     = $selected_language;
        $page_data['selected_rating']     = $selected_rating;
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function set_layout_to_session() {
        $layout = $this->input->post('layout');
        $this->session->set_userdata('layout', $layout);
    }

    public function course($slug = "", $course_id = "") {
        $this->access_denied_courses($course_id);
        $page_data['course_id'] = $course_id;
        $page_data['page_name'] = "course_page";
        $page_data['page_title'] = site_phrase('course');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function student_course($slug = "", $course_id = "") {
        //$this->access_denied_courses($course_id);
        $page_data['course_id'] = $course_id;
        $page_data['page_name'] = "student_course_page";
        $page_data['page_title'] = site_phrase('course');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function instructor_page($instructor_id = "") {
        $page_data['page_name'] = "instructor_page";
        $page_data['page_title'] = site_phrase('instructor_page');
        $page_data['instructor_id'] = $instructor_id;

        if (isset($_POST['star_rating'])) {
            $data['review'] = $this->input->post('review');
            $data['ratable_id'] = $this->input->post('instructor_id');
            $data['ratable_type'] = 'user';
            $data['rating'] = $this->input->post('star_rating');
            $data['date_added'] = strtotime(date('D, d-M-Y'));
            $data['user_id'] = $this->session->userdata('user_id');
            $this->crud_model->rate($data);
        }
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function my_courses() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        if ($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor')) {
             redirect(site_url('home'), 'refresh');
        }
        $page_data['page_name'] = "my_courses";
        $page_data['page_title'] = site_phrase("my_courses");
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function my_created_courses() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        if ($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor')) {
             redirect(site_url('home'), 'refresh');
        }
        $page_data['page_name'] = "my_created_courses";
        $page_data['page_title'] = site_phrase("my_created_courses");
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function my_messages($param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        if ($param1 == 'read_message') {
            $page_data['message_thread_code'] = $param2;
        }
        elseif ($param1 == 'group') {
            $page_data['group_thread_code'] = $param2;
        }
        elseif ($param1 == 'send_new') {
            $message_thread_code = $this->crud_model->send_new_private_message();
            $this->session->set_flashdata('flash_message', site_phrase('message_sent'));
            redirect(site_url('home/my_messages/read_message/' . $message_thread_code), 'refresh');
        }
        elseif ($param1 == 'send_reply') {
            $this->crud_model->send_reply_message($param2); //$param2 = message_thread_code
            $this->session->set_flashdata('flash_message', site_phrase('message_sent'));
            redirect(site_url('home/my_messages/read_message/' . $param2), 'refresh');
        }
        elseif ($param1 == 'send_group_reply') {
            $this->crud_model->send_group_reply_message($param2); //$param2 = group_thread_code
            $this->session->set_flashdata('flash_message', site_phrase('message_sent'));
            redirect(site_url('home/my_messages/group/' . $param2), 'refresh');
        }
        $page_data['page_name'] = "my_messages";
        $page_data['page_title'] = site_phrase('my_messages');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function new_group() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        
        $message_thread_code = $this->crud_model->create_group();
        $this->session->set_flashdata('flash_message', site_phrase('group created'));
        redirect(site_url('home/my_messages/group/' . $message_thread_code), 'refresh');
        
    }

    public function my_notifications() {
        $page_data['page_name'] = "my_notifications";
        $page_data['page_title'] = site_phrase('my_notifications');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function my_wishlist() {
        if (!$this->session->userdata('cart_items')) {
            $this->session->set_userdata('cart_items', array());
        }
        if ($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor')) {
             redirect(site_url('home'), 'refresh');
        }
        $my_courses = $this->crud_model->get_courses_by_wishlists();
        $page_data['my_courses'] = $my_courses;
        $page_data['page_name'] = "my_wishlist";
        $page_data['page_title'] = site_phrase('my_wishlist');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function purchase_history() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        if ($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor')) {
             redirect(site_url('home'), 'refresh');
        }
        $total_rows = $this->crud_model->purchase_history($this->session->userdata('user_id'))->num_rows();
        $config = array();
        $config = pagintaion($total_rows, 10);
        $config['base_url']  = site_url('home/purchase_history');
        $this->pagination->initialize($config);
        $page_data['per_page']   = $config['per_page'];

        if(addon_status('offline_payment') == 1):
            $this->load->model('addons/offline_payment_model');
            $page_data['pending_offline_payment_history'] = $this->offline_payment_model->pending_offline_payment($this->session->userdata('user_id'))->result_array();
        endif;

        $page_data['page_name']  = "purchase_history";
        $page_data['page_title'] = site_phrase('purchase_history');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function profile($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }

        if ($param1 == 'user_profile') {
            $page_data['page_name'] = "user_profile";
            $page_data['page_title'] = site_phrase('user_profile');
        }elseif ($param1 == 'user_credentials') {
            $page_data['page_name'] = "user_credentials";
            $page_data['page_title'] = site_phrase('credentials');
        }elseif ($param1 == 'user_photo') {
            $page_data['page_name'] = "update_user_photo";
            $page_data['page_title'] = site_phrase('update_user_photo');
        }
        $page_data['courses'] = $this->crud_model->get_all_courses();
        $page_data['colleges'] = $this->crud_model->get_all_colleges();
        $page_data['semesters'] = $this->crud_model->get_all_semesters();
        $page_data['user_details'] = $this->user_model->get_user($this->session->userdata('user_id'));
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function update_profile($param1 = "") {
        if ($param1 == 'update_basics') {
            $this->user_model->edit_user($this->session->userdata('user_id'));
            redirect(site_url('home/profile/user_profile'), 'refresh');
        }elseif ($param1 == "update_credentials") {
            $this->user_model->update_account_settings($this->session->userdata('user_id'));
            redirect(site_url('home/profile/user_credentials'), 'refresh');
        }elseif ($param1 == "update_photo") {
            $this->user_model->upload_user_image($this->session->userdata('user_id'));
            $this->session->set_flashdata('flash_message', site_phrase('updated_successfully'));
            redirect(site_url('home/profile/user_photo'), 'refresh');
        }

    }

    public function handleWishList($return_number = "") {
        if ($this->session->userdata('user_login') != 1) {
            echo false;
        }else {
            if (isset($_POST['course_id'])) {
                $course_id = $this->input->post('course_id');
                $this->crud_model->handleWishList($course_id);
            }
            if($return_number == 'true'){
                echo sizeof($this->crud_model->getWishLists());
            }else{
                $this->load->view('frontend/'.get_frontend_settings('theme').'/wishlist_items');
            }
        }
    }
    public function handleCartItems($return_number = "") {
        if (!$this->session->userdata('cart_items')) {
            $this->session->set_userdata('cart_items', array());
        }

        $course_id = $this->input->post('course_id');
        $previous_cart_items = $this->session->userdata('cart_items');
        if (in_array($course_id, $previous_cart_items)) {
            $key = array_search($course_id, $previous_cart_items);
            unset($previous_cart_items[$key]);
        }else {
            array_push($previous_cart_items, $course_id);
        }

        $this->session->set_userdata('cart_items', $previous_cart_items);
        if($return_number == 'true'){
            echo sizeof($previous_cart_items);
        }else{
            $this->load->view('frontend/'.get_frontend_settings('theme').'/cart_items');
        }
    }

    public function handleCartItemForBuyNowButton() {
        if (!$this->session->userdata('cart_items')) {
            $this->session->set_userdata('cart_items', array());
        }

        $course_id = $this->input->post('course_id');
        $previous_cart_items = $this->session->userdata('cart_items');
        if (!in_array($course_id, $previous_cart_items)) {
            array_push($previous_cart_items, $course_id);
        }
        $this->session->set_userdata('cart_items', $previous_cart_items);
        $this->load->view('frontend/'.get_frontend_settings('theme').'/cart_items');
    }

    public function refreshWishList() {
        $this->load->view('frontend/'.get_frontend_settings('theme').'/wishlist_items');
    }

    public function refreshShoppingCart() {
        $this->load->view('frontend/'.get_frontend_settings('theme').'/shopping_cart_inner_view');
    }

    public function isLoggedIn() {
        if ($this->session->userdata('user_login') == 1)
        echo true;
        else
        echo false;
    }

    //choose payment gateway
    public function payment(){
        if ($this->session->userdata('user_login') != 1)
        redirect('login', 'refresh');

        $page_data['total_price_of_checking_out'] = $this->session->userdata('total_price_of_checking_out');
        $page_data['page_title'] = site_phrase("payment_gateway");
        $this->load->view('payment/index', $page_data);
    }

    // SHOW PAYPAL CHECKOUT PAGE
    public function paypal_checkout($payment_request = "only_for_mobile") {
        if ($this->session->userdata('user_login') != 1 && $payment_request != 'true')
        redirect('home', 'refresh');

        //checking price
        if($this->session->userdata('total_price_of_checking_out') == $this->input->post('total_price_of_checking_out')):
            $total_price_of_checking_out = $this->input->post('total_price_of_checking_out');
        else:
            $total_price_of_checking_out = $this->session->userdata('total_price_of_checking_out');
        endif;
        $page_data['payment_request'] = $payment_request;
        $page_data['user_details']    = $this->user_model->get_user($this->session->userdata('user_id'))->row_array();
        $page_data['amount_to_pay']   = $total_price_of_checking_out;
        $this->load->view('frontend/'.get_frontend_settings('theme').'/paypal_checkout', $page_data);
    }

    // PAYPAL CHECKOUT ACTIONS
    public function paypal_payment($user_id = "", $amount_paid = "", $paymentID = "", $paymentToken = "", $payerID = "", $payment_request_mobile = "") {
        $paypal_keys = get_settings('paypal');
        $paypal = json_decode($paypal_keys);

        if ($paypal[0]->mode == 'sandbox') {
            $paypalClientID = $paypal[0]->sandbox_client_id;
            $paypalSecret   = $paypal[0]->sandbox_secret_key;
        }else{
            $paypalClientID = $paypal[0]->production_client_id;
            $paypalSecret   = $paypal[0]->production_secret_key;
        }

        //THIS IS HOW I CHECKED THE PAYPAL PAYMENT STATUS
        $status = $this->payment_model->paypal_payment($paymentID, $paymentToken, $payerID, $paypalClientID, $paypalSecret);
        if (!$status) {
            $this->session->set_flashdata('error_message', site_phrase('an_error_occurred_during_payment'));
            redirect('home', 'refresh');
        }
        $this->crud_model->enrol_student($user_id);
        $this->crud_model->course_purchase($user_id, 'paypal', $amount_paid);
        $this->email_model->course_purchase_notification($user_id, 'paypal', $amount_paid);
        $this->session->set_flashdata('flash_message', site_phrase('payment_successfully_done'));
        if($payment_request_mobile == 'true'):
            $course_id = $this->session->userdata('cart_items');
            redirect('home/payment_success_mobile/'.$course_id[0].'/'.$user_id.'/paid', 'refresh');
        else:
            $this->session->set_userdata('cart_items', array());
            redirect('home', 'refresh');
        endif;

    }

    // SHOW STRIPE CHECKOUT PAGE
    public function stripe_checkout($payment_request = "only_for_mobile") {
        if ($this->session->userdata('user_login') != 1 && $payment_request != 'true')
        redirect('home', 'refresh');

        //checking price
        $total_price_of_checking_out = $this->session->userdata('total_price_of_checking_out');
        $page_data['payment_request'] = $payment_request;
        $page_data['user_details']    = $this->user_model->get_user($this->session->userdata('user_id'))->row_array();
        $page_data['amount_to_pay']   = $total_price_of_checking_out;
        $this->load->view('payment/stripe/stripe_checkout', $page_data);
    }

    // STRIPE CHECKOUT ACTIONS
    public function stripe_payment($user_id = "", $payment_request_mobile = "") {
        //THIS IS HOW I CHECKED THE STRIPE PAYMENT STATUS
        $response = $this->payment_model->stripe_payment($user_id);

        if ($response['payment_status'] === 'succeeded') {
            // STUDENT ENROLMENT OPERATIONS AFTER A SUCCESSFUL PAYMENT
            $this->crud_model->enrol_student($user_id);
            $this->crud_model->course_purchase($user_id, 'stripe', $response['paid_amount']);
            $this->email_model->course_purchase_notification($user_id, 'stripe', $response['paid_amount']);

            if($payment_request_mobile == 'true'):
                $course_id = $this->session->userdata('cart_items');
                $this->session->set_flashdata('flash_message', site_phrase('payment_successfully_done'));
                redirect('home/payment_success_mobile/'.$course_id[0].'/'.$user_id.'/paid', 'refresh');
            else:
                $this->session->set_userdata('cart_items', array());
                $this->session->set_flashdata('flash_message', site_phrase('payment_successfully_done'));
                redirect('home', 'refresh');
            endif;
        }else{
            if($payment_request_mobile == 'true'):
                $course_id = $this->session->userdata('cart_items');
                $this->session->set_flashdata('flash_message', $response['status_msg']);
                redirect('home/payment_success_mobile/'.$course_id[0].'/'.$user_id.'/error', 'refresh');
            else:
                $this->session->set_flashdata('error_message', $response['status_msg']);
                redirect('home', 'refresh');
            endif;

        }

    }

    function razorpay_payment($payment_request_mobile=''){
        $total_price = $this->session->userdata('total_price_of_checking_out');
        $user_id = $_REQUEST['hidden'];
        $this->crud_model->enrol_student($user_id);
        $this->crud_model->course_purchase($user_id, 'razorpay', $total_price);
        $this->email_model->course_purchase_notification($user_id, 'razorpay', $total_price);

        if($payment_request_mobile == 'true'):
            $course_id = $this->session->userdata('cart_items');
            $this->session->set_flashdata('flash_message', site_phrase('payment_successfully_done'));
            redirect('home/payment_success_mobile/'.$course_id[0].'/'.$user_id.'/paid', 'refresh');
        else:
            $this->session->set_userdata('cart_items', array());
            $this->session->set_flashdata('flash_message', site_phrase('payment_successfully_done'));
            redirect('home', 'refresh');
        endif;
    }

    public function lesson($slug = "", $course_id = "", $lesson_id = "") {
        if ($this->session->userdata('user_login') != 1){
            if ($this->session->userdata('admin_login') != 1){
                redirect('home', 'refresh');
            }
        }

        $course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
        $sections = $this->crud_model->get_section('course', $course_id);
        if ($sections->num_rows() > 0) {
            $page_data['sections'] = $sections->result_array();
            if ($lesson_id == "") {
                $default_section = $sections->row_array();
                $page_data['section_id'] = $default_section['id'];
                $lessons = $this->crud_model->get_lessons('section', $default_section['id']);
                if ($lessons->num_rows() > 0) {
                    $default_lesson = $lessons->row_array();
                    $lesson_id = $default_lesson['id'];
                    $page_data['lesson_id']  = $default_lesson['id'];
                }else {
                    $page_data['page_name'] = 'empty';
                    $page_data['page_title'] = site_phrase('no_lesson_found');
                    $page_data['page_body'] = site_phrase('no_lesson_found');
                }
            }else {
                $page_data['lesson_id']  = $lesson_id;
                $section_id = $this->db->get_where('lesson', array('id' => $lesson_id))->row()->section_id;
                $page_data['section_id'] = $section_id;
            }

        }else {
            $page_data['sections'] = array();
            $page_data['page_name'] = 'empty';
            $page_data['page_title'] = site_phrase('no_section_found');
            $page_data['page_body'] = site_phrase('no_section_found');
        }

        // Check if the lesson contained course is purchased by the user
        if (isset($page_data['lesson_id']) && $page_data['lesson_id'] > 0) {
            $lesson_details = $this->crud_model->get_lessons('lesson', $page_data['lesson_id'])->row_array();
            $lesson_id_wise_course_details = $this->crud_model->get_course_by_id($lesson_details['course_id'])->row_array();
            if ($this->session->userdata('role_id') != 1 && $lesson_id_wise_course_details['user_id'] != $this->session->userdata('user_id')) {
                if (!is_purchased($lesson_details['course_id'])) {
                    redirect(site_url('home/course/'.slugify($course_details['title']).'/'.$course_details['id']), 'refresh');
                }
            }
        }else {
            if (!is_purchased($course_id)) {
                $this->session->set_flashdata('error_message', site_phrase('you_are_not_enrolled'));                
                redirect(site_url('home/course/'.slugify($course_details['title']).'/'.$course_details['id']), 'refresh');
            }
        }

        $page_data['course_id']  = $course_id;
        $page_data['page_name']  = 'lessons';
        $page_data['page_title'] = $course_details['title'];
        $this->load->view('lessons/index', $page_data);
    }

    public function my_courses_by_category() {
        $category_id = $this->input->post('category_id');
        $course_details = $this->crud_model->get_my_courses_by_category_id($category_id)->result_array();
        $page_data['my_courses'] = $course_details;
        $page_data['table'] = 'course';
        
        $this->load->view('frontend/'.get_frontend_settings('theme').'/reload_my_courses', $page_data);
    }


    public function my_student_courses_by_category() {
        $category_id = $this->input->post('category_id');
        $course_details = $this->crud_model->get_student_courses_by_category_id($category_id)->result_array();
        $page_data['my_courses'] = $course_details;
        $page_data['table'] = 'student_course';
        
        $this->load->view('frontend/'.get_frontend_settings('theme').'/reload_my_courses', $page_data);
    }

    public function search($search_string = "") {
        if (isset($_GET['query']) && !empty($_GET['query'])) {
            $search_string = $_GET['query'];
            $page_data['courses'] = $this->crud_model->get_courses_by_search_string($search_string)->result_array();
        }else {
            $this->session->set_flashdata('error_message', site_phrase('no_search_value_found'));
            redirect(site_url(), 'refresh');
        }

        if (!$this->session->userdata('layout')) {
            $this->session->set_userdata('layout', 'list');
        }
        $page_data['layout']     = $this->session->userdata('layout');
        $page_data['page_name'] = 'courses_page';
        $page_data['search_string'] = $search_string;
        $page_data['page_title'] = site_phrase('search_results');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }
    public function my_courses_by_search_string() {
        $search_string = $this->input->post('search_string');
        $course_details = $this->crud_model->get_my_courses_by_search_string($search_string)->result_array();
        $page_data['my_courses'] = $course_details;
        $page_data['table`'] = 'course';
        $this->load->view('frontend/'.get_frontend_settings('theme').'/reload_my_courses', $page_data);
    }
    public function my_student_courses_by_search_string() {
        $search_string = $this->input->post('search_string');
        $course_details = $this->crud_model->get_my_student_courses_by_search_string($search_string)->result_array();
        $page_data['my_courses'] = $course_details;
        $page_data['table'] = 'student_course';
        $this->load->view('frontend/'.get_frontend_settings('theme').'/reload_my_courses', $page_data);
    }

    public function get_instructor_by_category() {
        $search_string = $this->input->post('search_string');
        $course_details = $this->crud_model->get_instructor_by_category($search_string)->row_array();
        //$page_data['my_courses'] = $course_details;
        echo $course_details['id'];
    }

    public function get_my_wishlists_by_search_string() {
        $search_string = $this->input->post('search_string');
        $course_details = $this->crud_model->get_courses_of_wishlists_by_search_string($search_string);
        $page_data['my_courses'] = $course_details;
        $this->load->view('frontend/'.get_frontend_settings('theme').'/reload_my_wishlists', $page_data);
    }

    public function reload_my_wishlists() {
        $my_courses = $this->crud_model->get_courses_by_wishlists();
        $page_data['my_courses'] = $my_courses;
        $this->load->view('frontend/'.get_frontend_settings('theme').'/reload_my_wishlists', $page_data);
    }

    public function get_course_details() {
        $course_id = $this->input->post('course_id');
        $course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
        echo $course_details['title'];
    }

    public function rate_course() {
        $data['review'] = $this->input->post('review');
        $data['ratable_id'] = $this->input->post('course_id');
        $data['ratable_type'] = 'course';
        $data['rating'] = $this->input->post('starRating');
        $data['date_added'] = strtotime(date('D, d-M-Y'));
        $data['user_id'] = $this->session->userdata('user_id');
        $this->crud_model->rate($data);
    }

    public function about_us() {
        $page_data['page_name'] = 'about_us';
        $page_data['page_title'] = site_phrase('about_us');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function terms_and_condition() {
        $page_data['page_name'] = 'terms_and_condition';
        $page_data['page_title'] = site_phrase('terms_and_condition');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function privacy_policy() {
        $page_data['page_name'] = 'privacy_policy';
        $page_data['page_title'] = site_phrase('privacy_policy');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }
    public function cookie_policy() {
        $page_data['page_name'] = 'cookie_policy';
        $page_data['page_title'] = site_phrase('cookie_policy');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }


    // Version 1.1
    public function dashboard($param1 = "") {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }

        if ($param1 == "") {
            $page_data['type'] = 'active';
        }else {
            $page_data['type'] = $param1;
        }

        $page_data['page_name']  = 'instructor_dashboard';
        $page_data['page_title'] = site_phrase('instructor_dashboard');
        $page_data['user_id']    = $this->session->userdata('user_id');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function create_course() {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }

        $page_data['page_name'] = 'create_course';
        $page_data['page_title'] = site_phrase('create_course');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function edit_course($param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }

        if ($param2 == "") {
            $page_data['type']   = 'edit_course';
        }else {
            $page_data['type']   = $param2;
        }
        $page_data['page_name']  = 'manage_course_details';
        $page_data['course_id']  = $param1;
        $page_data['page_title'] = site_phrase('edit_course');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function course_action($param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }

        if ($param1 == 'create') {
            if (isset($_POST['create_course'])) {
                $this->crud_model->add_course();
                redirect(site_url('home/create_course'), 'refresh');
            }else {
                $this->crud_model->add_course('save_to_draft');
                redirect(site_url('home/create_course'), 'refresh');
            }
        }elseif ($param1 == 'edit') {
            if (isset($_POST['publish'])) {
                $this->crud_model->update_course($param2, 'publish');
                redirect(site_url('home/dashboard'), 'refresh');
            }else {
                $this->crud_model->update_course($param2, 'save_to_draft');
                redirect(site_url('home/dashboard'), 'refresh');
            }
        }
    }


    public function sections($action = "", $course_id = "", $section_id = "") {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }

        if ($action == "add") {
            $this->crud_model->add_section($course_id);

        }elseif ($action == "edit") {
            $this->crud_model->edit_section($section_id);

        }elseif ($action == "delete") {
            $this->crud_model->delete_section($course_id, $section_id);
            $this->session->set_flashdata('flash_message', site_phrase('section_deleted'));
            redirect(site_url("home/edit_course/$course_id/manage_section"), 'refresh');

        }elseif ($action == "serialize_section") {
            $container = array();
            $serialization = json_decode($this->input->post('updatedSerialization'));
            foreach ($serialization as $key) {
                array_push($container, $key->id);
            }
            $json = json_encode($container);
            $this->crud_model->serialize_section($course_id, $json);
        }
        $page_data['course_id'] = $course_id;
        $page_data['course_details'] = $this->crud_model->get_course_by_id($course_id)->row_array();
        return $this->load->view('frontend/'.get_frontend_settings('theme').'/reload_section', $page_data);
    }

    public function manage_lessons($action = "", $course_id = "", $lesson_id = "") {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }
        if ($action == 'add') {
            $this->crud_model->add_lesson();
            $this->session->set_flashdata('flash_message', site_phrase('lesson_added'));
        }
        elseif ($action == 'edit') {
            $this->crud_model->edit_lesson($lesson_id);
            $this->session->set_flashdata('flash_message', site_phrase('lesson_updated'));
        }
        elseif ($action == 'delete') {
            $this->crud_model->delete_lesson($lesson_id);
            $this->session->set_flashdata('flash_message', site_phrase('lesson_deleted'));
        }
        redirect('home/edit_course/'.$course_id.'/manage_lesson');
    }

    public function lesson_editing_form($lesson_id = "", $course_id = "") {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }
        $page_data['type']      = 'manage_lesson';
        $page_data['course_id'] = $course_id;
        $page_data['lesson_id'] = $lesson_id;
        $page_data['page_name']  = 'lesson_edit';
        $page_data['page_title'] = site_phrase('update_lesson');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function download($filename = "") {
        $tmp           = explode('.', $filename);
        $fileExtension = strtolower(end($tmp));
        $yourFile = base_url().'uploads/lesson_files/'.$filename;
        $file = @fopen($yourFile, "rb");

        header('Content-Description: File Transfer');
        header('Content-Type: text/plain');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($yourFile));
        while (!feof($file)) {
            print(@fread($file, 1024 * 8));
            ob_flush();
            flush();
        }
    }

    // Version 1.3 codes
    public function get_enrolled_to_free_course($course_id) {
        if ($this->session->userdata('user_login') == 1) {
            $this->crud_model->enrol_to_free_course($course_id, $this->session->userdata('user_id'));
            redirect(site_url('home/my_courses'), 'refresh');
        }else {
            redirect(site_url('login'), 'refresh');
        }
    }

    // Version 1.4 codes
    public function login() {
        if ($this->session->userdata('admin_login')) {
            redirect(site_url('admin'), 'refresh');
        }elseif ($this->session->userdata('user_login')) {
            redirect(site_url('user'), 'refresh');
        }
        $page_data['authUrl'] = $this->user_model->createGoogleSignUpInstantce();
        $page_data['fbauthUrl'] = $this->user_model->createFBSignUpInstantce();
        $page_data['oauthURL'] = $this->user_model->createlinkedinSignUpInstantce();
        $page_data['page_name'] = 'login';
        $page_data['page_title'] = site_phrase('login');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function sign_up() {
        if ($this->session->userdata('admin_login')) {
            redirect(site_url('admin'), 'refresh');
        }elseif ($this->session->userdata('user_login')) {
            redirect(site_url('user'), 'refresh');
        }
        $page_data['authUrl'] = $this->user_model->createGoogleSignUpInstantce();
        $page_data['fbauthUrl'] = $this->user_model->createFBSignUpInstantce();
        $page_data['oauthURL'] = $this->user_model->createlinkedinSignUpInstantce();
        $page_data['page_name'] = 'sign_up';
        $page_data['page_title'] = site_phrase('sign_up');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }
    public function instructor_sign_up(){
        if ($this->session->userdata('admin_login')) {
            redirect(site_url('admin'), 'refresh');
        }elseif ($this->session->userdata('user_login')) {
            redirect(site_url('user'), 'refresh');
        }
        $page_data['authUrl'] = $this->user_model->createGoogleSignUpInstantce('','instructor');
        $page_data['fbauthUrl'] = $this->user_model->createFBSignUpInstantce('','instructor');
        $page_data['oauthURL'] = $this->user_model->createlinkedinSignUpInstantce('','instructor');
        $page_data['page_name'] = 'instructor_sign_up';
        $page_data['page_title'] = site_phrase('sign_up');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function mentor_sign_up(){
        if ($this->session->userdata('admin_login')) {
            redirect(site_url('admin'), 'refresh');
        }elseif ($this->session->userdata('user_login')) {
            redirect(site_url('user'), 'refresh');
        }
        $page_data['authUrl'] = $this->user_model->createGoogleSignUpInstantce('','mentor');
        $page_data['fbauthUrl'] = $this->user_model->createFBSignUpInstantce('','mentor');
        $page_data['oauthURL'] = $this->user_model->createlinkedinSignUpInstantce('','mentor');
        $page_data['page_name'] = 'mentor_sign_up';
        $page_data['page_title'] = site_phrase('sign_up');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function forgot_password() {
        if ($this->session->userdata('admin_login')) {
            redirect(site_url('admin'), 'refresh');
        }elseif ($this->session->userdata('user_login')) {
            redirect(site_url('user'), 'refresh');
        }
        $page_data['page_name'] = 'forgot_password';
        $page_data['page_title'] = site_phrase('forgot_password');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function submit_quiz($from = "") {
        $submitted_quiz_info = array();
        $container = array();
        $quiz_id = $this->input->post('lesson_id');
        $quiz_questions = $this->crud_model->get_quiz_questions($quiz_id)->result_array();
        $total_correct_answers = 0;
        foreach ($quiz_questions as $quiz_question) {
            $submitted_answer_status = 0;
            $correct_answers = json_decode($quiz_question['correct_answers']);
            $submitted_answers = array();
            foreach ($this->input->post($quiz_question['id']) as $each_submission) {
                if (isset($each_submission)) {
                    array_push($submitted_answers, $each_submission);
                }
            }
            sort($correct_answers);
            sort($submitted_answers);
            if ($correct_answers == $submitted_answers) {
                $submitted_answer_status = 1;
                $total_correct_answers++;
            }
            $container = array(
                "question_id" => $quiz_question['id'],
                'submitted_answer_status' => $submitted_answer_status,
                "submitted_answers" => json_encode($submitted_answers),
                "correct_answers"  => json_encode($correct_answers),
            );
            array_push($submitted_quiz_info, $container);
        }
        $page_data['submitted_quiz_info']   = $submitted_quiz_info;
        $page_data['total_correct_answers'] = $total_correct_answers;
        $page_data['total_questions'] = count($quiz_questions);
        if ($from == 'mobile') {
            $this->load->view('mobile/quiz_result', $page_data);
        }else{
            $this->load->view('lessons/quiz_result', $page_data);
        }
    }

    private function access_denied_courses($course_id){
        $course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
        if ($course_details['status'] == 'draft' && $course_details['user_id'] != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('error_message', site_phrase('you_do_not_have_permission_to_access_this_course'));
            redirect(site_url('home'), 'refresh');
        }elseif ($course_details['status'] == 'pending') {
            if ($course_details['user_id'] != $this->session->userdata('user_id') && $this->session->userdata('role_id') != 1) {
                $this->session->set_flashdata('error_message', site_phrase('you_do_not_have_permission_to_access_this_course'));
                redirect(site_url('home'), 'refresh');
            }
        }
    }

    public function invoice($purchase_history_id = '') {
        if ($this->session->userdata('user_login') != 1){
            redirect('home', 'refresh');
        }
        $purchase_history = $this->crud_model->get_payment_details_by_id($purchase_history_id);
        if ($purchase_history['user_id'] != $this->session->userdata('user_id')) {
            redirect('home', 'refresh');
        }
        $page_data['payment_info'] = $purchase_history;
        $page_data['page_name'] = 'invoice';
        $page_data['page_title'] = 'invoice';
        $page_data['purchase_history_id'] = $purchase_history_id;
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function page_not_found() {
        $page_data['page_name'] = '404';
        $page_data['page_title'] = site_phrase('404_page_not_found');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    // AJAX CALL FUNCTION FOR CHECKING COURSE PROGRESS
    function check_course_progress($course_id) {
        echo course_progress($course_id);
    }

    // This is the function for rendering quiz web view for mobile
    public function quiz_mobile_web_view($lesson_id = "") {
        $data['lesson_details'] = $this->crud_model->get_lessons('lesson', $lesson_id)->row_array();
        $data['page_name'] = 'quiz';
        $this->load->view('mobile/index', $data);
    }


    // CHECK CUSTOM SESSION DATA
    public function session_data() {
        // SESSION DATA FOR CART
        if (!$this->session->userdata('cart_items')) {
            $this->session->set_userdata('cart_items', array());
        }

        // SESSION DATA FOR FRONTEND LANGUAGE
        if (!$this->session->userdata('language')) {
            $this->session->set_userdata('language', get_settings('language'));
        }

    }

    // SETTING FRONTEND LANGUAGE
    public function site_language() {
        $selected_language = $this->input->post('language');
        $this->session->set_userdata('language', $selected_language);
        echo true;
    }


    //FOR MOBILE
    public function course_purchase($auth_token = '', $course_id  = ''){
        $this->load->model('jwt_model');
        if(empty($auth_token) || $auth_token == "null"){
            $page_data['cart_item'] = $course_id;
            $page_data['user_id'] = '';
            $page_data['is_login_now'] = 0;
            $page_data['enroll_type'] = null;
            $page_data['page_name'] = 'shopping_cart';
            $this->load->view('mobile/index', $page_data);
        }else{

            $logged_in_user_details = json_decode($this->jwt_model->token_data_get($auth_token), true);

            if ($logged_in_user_details['user_id'] > 0) {

                $credential = array('id' => $logged_in_user_details['user_id'], 'status' => 1, 'role_id' => 2);
                $query = $this->db->get_where('users', $credential);
                if ($query->num_rows() > 0) {
                    $row = $query->row();
                    $page_data['cart_item'] = $course_id;
                    $page_data['user_id'] = $row->id;
                    $page_data['is_login_now'] = 1;
                    $page_data['enroll_type'] = null;
                    $page_data['page_name'] = 'shopping_cart';

                    $cart_item = array($course_id);
                    $this->session->set_userdata('cart_items', $cart_item);
                    $this->session->set_userdata('user_login', '1');
                    $this->session->set_userdata('user_id', $row->id);
                    $this->session->set_userdata('role_id', $row->role_id);
                    $this->session->set_userdata('role', get_user_role('user_role', $row->id));
                    $this->session->set_userdata('name', $row->first_name.' '.$row->last_name);
                    $this->load->view('mobile/index', $page_data);
                }
            }

        }
    }

    //FOR MOBILE
    public function get_enrolled_to_free_course_mobile($course_id ="", $user_id ="", $get_request = "") {
        if ($get_request == "true") {
            $this->crud_model->enrol_to_free_course_mobile($course_id, $user_id);
        }
    }

    //FOR MOBILE
    public function payment_success_mobile($course_id = "", $user_id = "", $enroll_type = ""){
        if($course_id > 0 && $user_id > 0):
            $page_data['cart_item'] = $course_id;
            $page_data['user_id'] = $user_id;
            $page_data['is_login_now'] = 1;
            $page_data['enroll_type'] = $enroll_type;
            $page_data['page_name'] = 'shopping_cart';

            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('role_id');
            $this->session->unset_userdata('role');
            $this->session->unset_userdata('name');
            $this->session->unset_userdata('user_login');
            $this->session->unset_userdata('cart_items');

            $this->load->view('mobile/index', $page_data);
        endif;
    }

    //FOR MOBILE
    public function payment_gateway_mobile($course_id = "", $user_id = ""){
        if($course_id > 0 && $user_id > 0):
            $page_data['page_name'] = 'payment_gateway';
            $this->load->view('mobile/index', $page_data);
        endif;
    }

    public function add_course() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        if ($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor')) {
             redirect(site_url('home'), 'refresh');
        }
        $page_data['categories'] = $this->crud_model->get_categories();
        $page_data['page_name'] = "add_student_course";
        $page_data['page_title'] = site_phrase("add_course");
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }
    public function edit_student_course($param) {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        $page_data['categories'] = $this->crud_model->get_categories();
        $page_data['page_name'] = "edit_student_course";
        $page_data['page_title'] = site_phrase("edit_course");
        $page_data['course_id'] = $param;
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }
    public function student_course_actions($param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == "add") {
            $course_id = $this->crud_model->add_student_course();
            $this->session->set_flashdata('flash_message', site_phrase('project_successfully_submitted._admin_will_approve_soon'));
            redirect(site_url('home/my_created_courses'), 'refresh');  
        }
        elseif ($param1 == "edit") {
            $this->crud_model->update_student_course($param2);
            redirect(site_url('home/my_created_courses'), 'refresh');

        }
        elseif ($param1 == 'delete') {
            $course_id = $this->input->post('course_id');
            $this->crud_model->delete_student_course($course_id);
            $this->session->set_flashdata('flash_message', site_phrase('course_has_been_deleted_successfully'));
            redirect(site_url('home/my_created_courses'), 'refresh');
        }
    }
    public function search_mentor_by_name(){
        $search_string = $this->input->get('query');
        $category = $this->input->get('category');
        $this->db->where('users.is_mentor', '1');       
        $this->db->where('users.status', '1');       
        $this->db->select('users.*');
        if (!empty($search_string)) {
            $this->db->where('( users.first_name LIKE "%'.$search_string.'%" OR users.last_name LIKE "%'.$search_string.'%")'); 
        }
        if(!empty($category)) {
            $this->db->where('users.expertise_area LIKE "%'.$category.'%" ');
        }
        $this->db->join('rating', 'rating.user_id = users.id AND ratable_type = "user" ','left');
        $this->db->order_by('rating.rating', 'desc');
        $json_data = array();
        $resultData = $this->db->get('users');
        if (!empty($resultData)) {
            $instructorData = $resultData->result_array();   
            foreach ($instructorData as $key => $value) {
                $json_data[] = array('name'=>$value['first_name'].' '.$value['last_name'],'id'=>$value['id']);
            }
        }
        echo json_encode($json_data);
        exit();
    }

    public function search_all_name(){
        $search_string = $this->input->get('query');
        $category = $this->input->get('users');

        $group_members = $this->user_model->get_instructor_list()->result_array();
        $user_id_list = array();
        foreach ($group_members as $user_ids) {
	        array_push($user_id_list, $user_ids['id']);
	    }
	    $this->db->where_in('id', $user_id_list);
        $this->db->where('users.status', '1');       
        $this->db->select('users.*');
        if (!empty($search_string)) {
            $this->db->where('( users.first_name LIKE "%'.$search_string.'%" OR users.last_name LIKE "%'.$search_string.'%")'); 
        }
        $json_data = array();
        $resultData = $this->db->get('users');
        if (!empty($resultData)) {
            $instructorData = $resultData->result_array();   
            foreach ($instructorData as $key => $value) {
                $json_data[] = array('name'=>$value['first_name'].' '.$value['last_name'],'id'=>$value['id']);
            }
        }
        echo json_encode($json_data);
        exit();
    }


    public function new_password(){
        $page_data['page_name'] = "new_password";
        $page_data['page_title'] = site_phrase('new_password');
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function download_document($filename,$subfolder='student_document'){
        $file_with_path =  'uploads/'.$subfolder.'/'.rawurldecode($filename);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.$filename);
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file_with_path));
        readfile($file_with_path);
    }

    public function invite_friends(){
        $course_id = $this->input->post('course_id');
        $to_email = $this->input->post('to_email');
        $type = $this->input->post('type');
        $to_email = array_filter($to_email);
        $mail_count= count($to_email);
        $table = 'student_course';
        if ($type == 'master') {
            $table = 'course';
        }
        for($i=0;$i<$mail_count;$i++)
        {
            $mail_id = TRIM($to_email[$i]);

            $this->db->where('email',$mail_id);
            $this->db->where('course_id',$course_id);

            if($table == 'master'){
                $dataExists = $this->db->get('master_project_requests')->row_array();
                if(empty($dataExists)){
                    $this->db->where('email',$mail_id);
                    $userData = $this->db->get('users')->row_array();
                    $this->db->where('user_id',$userData['id']);
                    $this->db->where('course_id',$course_id);  
                    $dataExists = $this->db->get('enrol')->row_array();  // Check student was already enrol in this project or not (self).                
                }
            }else{
                $dataExists = $this->db->get('requested_projects')->row_array();    
            }
            
            if(!empty($dataExists)) continue; // If student is already enrol then do not send invitation
            $this->email_model->sent_mail_to_friends($course_id,$mail_id,$table);
            $data = array();
            $data['course_id'] = $course_id;
            $data['email'] = $mail_id;
            $data['requested_user_id'] = $this->session->userdata('user_id');
            $data['status'] = 'pending';
            $data['added_date'] = strtotime(date("Y-m-d H:i:s"));
            if ($type == 'master') {
                $this->user_model->add_master_invited_friend($data);
            }else{
                $this->user_model->add_invited_friend($data);    
            }
            
        }
        $this->session->set_flashdata('flash_message', site_phrase('mail_successfully_sent_to_your_friend'));
        if ($type == 'master') {
            redirect(site_url('home/my_courses'), 'refresh');
        }else{
            redirect(site_url('home/my_created_courses'), 'refresh');    
        }
        
    }

    public function requested_projects(){
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('home'), 'refresh');
        }
        if ($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor')) {
             redirect(site_url('home'), 'refresh');
        }
        $page_data['page_name'] = "requested_projects";
        $page_data['page_title'] = site_phrase("project_request");
        $this->load->view('frontend/'.get_frontend_settings('theme').'/index', $page_data);
    }

    public function approve_reject_requested_projects(){
       $course_string = $this->input->post('course_id');
       $status = $this->input->post('status'); 
       $idarray = explode('_', $course_string);
       $id = $idarray[1];
       $this->db->where('course_id',$id);
       if ($status == '0') {
            $this->db->where('email',$this->session->userdata('email'));
            $this->db->delete('requested_projects');
            $this->session->set_flashdata('flash_message', site_phrase('project_successfully_rejected'));
       }else{
            $this->db->update('requested_projects',array('status'=>'approved'));

            $data = array()            ;
            $data['user_id'] = $this->session->userdata('user_id');
            $data['course_id'] = $id;
            $this->db->insert('student_enrol',$data);
            $this->session->set_flashdata('flash_message', site_phrase('project_successfully_accepted'));
       }
       echo "success";
       exit();
    }

    public function approve_reject_master_requested_projects(){
       $course_string = $this->input->post('course_id');
       $status = $this->input->post('status'); 
       $idarray = explode('_', $course_string);
       $id = $idarray[1];
       $this->db->where('course_id',$id);
       if ($status == '0') {
            $this->db->where('email',$this->session->userdata('email'));
            $this->db->delete('master_project_requests');
            $this->session->set_flashdata('flash_message', site_phrase('project_successfully_rejected'));
       }else{
            $this->db->update('master_project_requests',array('status'=>'approved'));
            $data = array()            ;
            $data['user_id'] = $this->session->userdata('user_id');
            $data['course_id'] = $id;
            $data['date_added'] = strtotime(date("Y-m-d H:i:s"));;
            $this->db->insert('enrol',$data);
            $this->session->set_flashdata('flash_message', site_phrase('project_successfully_accepted'));
       }
       echo "success";
       exit();
    }

    public function invite_instructors(){
        $course_id = $this->input->post('course_id');
        $to_email = $this->input->post('to_email');
        $to_email = array_filter($to_email);
        $mail_count= count($to_email);
        $table = 'student_course';
        for($i=0;$i<$mail_count;$i++)
        {
            $mail_id = TRIM($to_email[$i]);

            $this->db->where('email',$mail_id);
            $this->db->where('course_id',$course_id);
            $dataExists = $this->db->get('requested_projects')->row_array();    
            
            if(!empty($dataExists)) continue; // If student is already enrol then do not send invitation
            $this->email_model->sent_mail_to_friends($course_id,$mail_id,$table);
            $data = array();
            $data['course_id'] = $course_id;
            $data['email'] = $mail_id;
            $data['requested_user_id'] = $this->session->userdata('user_id');
            $data['status'] = 'pending';
            $data['added_date'] = strtotime(date("Y-m-d H:i:s"));
            $this->user_model->add_invited_instructor($data);    
        }
        $this->session->set_flashdata('flash_message', site_phrase('mail_successfully_sent_to_instructor'));
        redirect(site_url('home/my_created_courses'), 'refresh');    
    }

    public function send_approval_request_to_next_mentor(){
        $this->db->where('status','pending');
        $mentor_request_status = $this->db->get('add_mentor')->result_array();
        foreach ($mentor_request_status as $key => $value) {
            $expiry_date = strtotime('+1 day', $value['last_modified']);
            $current_date = time();
            if ($expiry_date < $current_date) {
                $this->db->where('id',$value['id']);
                $this->db->update('add_mentor',array('status'=>'rejected'));

                $this->db->where('status','waiting');
                $this->db->where('course_id',$value['course_id']);
                $this->db->limit(1);
                $mentor_request_status = $this->db->get('add_mentor')->row_array();

                if (empty($mentor_request_status)) {
                    continue;
                }
                $this->db->where('id',$mentor_request_status['id']);
                $this->db->update('add_mentor',array('status'=>'pending','last_modified'=>strtotime(date("Y-m-d H:i:s"))));

                $this->db->select('email');
                $this->db->where('id',$mentor_course_detail['user_id']);
                $user_details = $this->db->get('users')->row_array();

                $this->email_model->sent_mail_to_mentor($course_id,$user_details['email']);
            }
        }
    }

    public function group_purchase_action(){

        $course_id = $this->input->post('course_id');
        $to_name   = $this->input->post('to_name');
        $to_email  = $this->input->post('to_email');
        $to_phone  = $this->input->post('to_phone');
        $action    = $this->input->post('action');
        $total_members = count(array_filter($to_email)) + 1;

        if (empty(array_filter($to_name)) || empty(array_filter($to_phone)) || empty(array_filter($to_email))) {
            echo "fail";
            exit();
        }
        $discountpricedetails = $this->user_model->get_group_course_discount_price($course_id,$total_members);
        $group_course_price = $discountpricedetails[0];
        $discounted_price = $discountpricedetails[1];


        if ($action == 'showprice') {

            if($discounted_price != $group_course_price) {
                $message = '<span class="original-price" style="text-decoration:line-through;color:#686f7a;">'.currency($group_course_price).'</span>
                      <span class="current-price" style="color:#ec5252;font-weight:900;">'.currency($discounted_price).'</span><span class="text-danger"> ( '.$discountpricedetails[2].'% '.site_phrase('discount_applied').' )';
            }else{
                $message = '<span class="current-price" style="color:#ec5252;font-weight:900;">'.currency($discounted_price);
            }
            echo $message;
        }elseif ($action == 'buynow' || $action == 'add_to_cart') {

            $this->db->where('user_id',$this->session->userdata('user_id'));
            $this->db->where('course_id',$course_id);
            $this->db->delete('temp_group_members');

            foreach ($to_email as $key => $value) {
                $data = array();
                if(empty($value)) continue;
                $data['course_id'] = $course_id;
                $data['user_id'] = $this->session->userdata('user_id');
                $data['name'] = $to_name[$key];
                $data['email'] = $to_email[$key];
                $data['phone'] = $to_phone[$key];
                $this->db->insert('temp_group_members',$data);
            }

            $previous_cart_items = $this->session->userdata('cart_items');
            if (!in_array($course_id, $previous_cart_items)) {
                array_push($previous_cart_items, $course_id);
            }
            $this->session->set_userdata('cart_items', $previous_cart_items);
            echo "success";
        }elseif ($action == 'added_to_cart') {

            $this->db->where('user_id',$this->session->userdata('user_id'));
            $this->db->where('course_id',$course_id);
            $this->db->delete('temp_group_members');
            $previous_cart_items = $this->session->userdata('cart_items');
            if (in_array($course_id, $previous_cart_items)) {
                $key = array_search($course_id, $previous_cart_items);
                unset($previous_cart_items[$key]);
            }
            $this->session->set_userdata('cart_items', $previous_cart_items);
            $this->load->view('frontend/'.get_frontend_settings('theme').'/cart_items');
            echo "success";
        }
        exit();
    }

    public function download_payment_receipt($param1=''){
        $payment_info = $this->crud_model->get_payment_details_by_id($param1);
        $course_details = $this->crud_model->get_course_by_id($payment_info['course_id'])->row_array();
        $buyer_details = $this->user_model->get_all_user($payment_info['user_id'])->row_array();
        $sub_category_details = $this->crud_model->get_category_details_by_id($course_details['sub_category_id'])->row_array();
        $instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
        $this->load->library('pdf');
            $pdf = new pdf();
            $this->dompdf->set_option('isHtml5ParserEnabled',true);
            $filename = $course_details['title'].'_'.date('ymd').rand(100,99999).'.pdf';
            $html = '<html><head></head><body>
                <div style="margin-left:auto;margin-right:auto;">
                    <link href='.base_url('assets/frontend/elegant/css/print.css').' rel="stylesheet">
                    <div style="background: #eceff4;padding: 1.5rem;">
                        <table>
                            <tr>
                                <td>
                                    <img src="uploads/system/logo-dark.png" height="40" style="display:inline-block;">
                                </td>
                                <td style="font-size: 22px;" class="text-right strong">'. strtoupper(site_phrase('invoice')).'</td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td style="font-size: 1.2rem;" class="strong">'.get_settings('system_name').'</td>
                                <td class="text-right"></td>
                            </tr>
                            <tr>
                                <td class="gry-color small">'.get_settings('system_email').'</td>
                                <td class="text-right"></td>
                            </tr>
                            <tr>
                                <td class="gry-color small">'.get_settings('address').'</td>
                                <td class="text-right small"><span class="gry-color small">'.site_phrase('payment_method').':</span> <span class="strong">'. ucfirst($payment_info['payment_type']).'</span></td>
                            </tr>
                            <tr>
                                <td class="gry-color small">'.site_phrase('phone').': '.get_settings('phone').'</td>
                                <td class="text-right small"><span class="gry-color small">'.site_phrase('purchase_date').':</span> <span class=" strong">'. date('D, d-M-Y', $payment_info['date_added']).'</span></td>
                            </tr>
                        </table>

                    </div>

                    <div style="border-bottom:1px solid #eceff4;margin: 0 1.5rem;"></div>
                    <div style="padding: 1.5rem;">
                        <table>
                            <tr><td class="strong small gry-color">'.site_phrase('bill_to').':</td></tr>
                            <tr><td class="strong">'.$buyer_details['first_name'].' '.$buyer_details['last_name'].'</td></tr>
                            <tr><td class="gry-color small">'.site_phrase('email').': '. $buyer_details['email'].'</td></tr>
                        </table>
                    </div>
                    <div style="">
                        <table class="padding text-left border-bottom" style="width: 100%;">
                            <thead>
                                <tr class="gry-color" style="background: #eceff4;">
                                    <th width="50%">'.site_phrase('course_name').'</th>
                                    <th width="15%" style="padding-left : 10px;">'.site_phrase('category').'</th>
                                    <th width="15%">'.site_phrase('instructor').'</th>
                                    <th width="20%" class="text-right">'.site_phrase('total').'</th>
                                </tr>
                            </thead>
                            <tbody class="strong">
                                <tr class="">
                                    <td>'.$course_details['title'].'</td>
                                    <td class="gry-color" style="padding-left : 10px;">'.$sub_category_details['name'].'</td>
                                    <td class="gry-color">'.$instructor_details['first_name'].' '.$instructor_details['last_name'].'</td>
                                    <td class="text-right">Rs.'.currency($payment_info['amount']).'</td>
                                </tr>
                                <tr class="">
                                    <td></td>
                                    <td class="gry-color"></td>
                                    <td class="gry-color"> <strong>'.site_phrase('sub_total').':</strong> </td>
                                    <td class="text-right"><strong>Rs.'.currency($payment_info['amount']).'</strong></td>
                                </tr>
                                <tr class="">
                                    <td></td>
                                    <td class="gry-color"></td>
                                    <td class="gry-color strong"><strong>'.site_phrase('grand_total').'</strong>:</td>
                                    <td class="text-right"><strong>Rs.'. currency($payment_info['amount']).'</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
        </body></html>';
            $this->dompdf->load_html($html);
            
            $this->dompdf->setPaper('A4', 'landscape');
            
            $this->dompdf->render();
            
            //$pdfhtml = $this->dompdf->output();
            $this->dompdf->stream($filename);

    }
}