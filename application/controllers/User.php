<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->load->library('session');
        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');


        // SESSION DATA FOR IS INSTRUCTOR
        if (!$this->session->userdata('is_instructor') || !$this->session->userdata('is_mentor')) {
            $logged_in_user_details = $this->user_model->get_all_user($this->session->userdata('user_id'))->row_array();
            $this->session->set_userdata('is_instructor', $logged_in_user_details['is_instructor']);
            $this->session->set_userdata('is_mentor', $logged_in_user_details['is_mentor']);
        }

        // THIS FUNCTION DECIDES WHTHER THE ROUTE IS REQUIRES PUBLIC INSTRUCTOR.
        $this->get_protected_routes($this->router->method);

        // THIS MIDDLEWARE FUNCTION CHECKS WHETHER THE USER IS TRYING TO ACCESS INSTRUCTOR STUFFS.
        $this->instructor_authorization($this->router->method);

    }


    public function get_protected_routes($method) {
        // IF ANY FUNCTION DOES NOT REQUIRE PUBLIC INSTRUCTOR, PUT THE NAME HERE.
        $unprotected_routes = ['save_course_progress'];

        if (!in_array($method, $unprotected_routes)) {
            if (get_settings('allow_instructor') != 1){
                redirect(site_url('home'), 'refresh');
            }
        }
    }

    public function instructor_authorization($method) {
        // IF THE USER IS NOT AN INSTRUCTOR HE/SHE CAN NEVER ACCESS THE OTHER FUNCTIONS EXCEPT FOR BELOW FUNCTIONS.
        if ($this->session->userdata('is_instructor') != 1 && $this->session->userdata('is_mentor') != 1) {
            $unprotected_routes = ['become_an_instructor', 'manage_profile', 'save_course_progress'];

            if (!in_array($method, $unprotected_routes)) {
                redirect(site_url('user/become_an_instructor'), 'refresh');
            }
        }
    }

    public function index() {
        if ($this->session->userdata('user_login') == true) {
            $this->dashboard();
        }else {
            redirect(site_url('login'), 'refresh');
        }
    }

    public function dashboard() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        $page_data['page_name'] = 'dashboard';
        $page_data['page_title'] = get_phrase('dashboard');

        if ($this->session->userdata('is_instructor')) {
            $email = $this->session->userdata('email');
            $this->db->select('student_course.*,instructor_requested_projects.requested_user_id,instructor_requested_projects.status as ins_status');
            $this->db->where('instructor_requested_projects.email',$email);
            $this->db->join('student_course','student_course.id = instructor_requested_projects.course_id','left');
            $page_data['courses'] = $this->db->get('instructor_requested_projects')->result_array();
        }
        if ($this->session->userdata('is_mentor')) {
            $mentor_id = $this->session->userdata('user_id');
            $this->db->select('student_course.*,add_mentor.status as ins_status,add_mentor.id as request_id');
            $this->db->where('add_mentor.user_id',$mentor_id);
            $this->db->where('add_mentor.status != "waiting"');
            $this->db->join('student_course','student_course.id = add_mentor.course_id','left');
            $page_data['courses'] = $this->db->get('add_mentor');
        }
        $this->load->view('backend/index.php', $page_data);
    }

    public function courses() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $page_data['selected_category_id']   = isset($_GET['category_id']) ? $_GET['category_id'] : "all";
        $page_data['selected_instructor_id'] = $this->session->userdata('user_id');
        $page_data['selected_price']         = isset($_GET['price']) ? $_GET['price'] : "all";
        $page_data['selected_status']        = isset($_GET['status']) ? $_GET['status'] : "all";
        $page_data['courses']                = $this->crud_model->filter_course_for_backend($page_data['selected_category_id'], $page_data['selected_instructor_id'], $page_data['selected_price'], $page_data['selected_status']);
        $page_data['page_name']              = 'courses-server-side';
        $page_data['categories']             = $this->crud_model->get_categories();
        $page_data['page_title']             = get_phrase('active_courses');
        $this->load->view('backend/index', $page_data);
    }

    // This function is responsible for loading the course data from server side for datatable SILENTLY
    public function get_courses() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $courses = array();
        // Filter portion
        $filter_data['selected_category_id']   = $this->input->post('selected_category_id');
        $filter_data['selected_instructor_id'] = $this->input->post('selected_instructor_id');
        $filter_data['selected_price']         = $this->input->post('selected_price');
        $filter_data['selected_status']        = $this->input->post('selected_status');

        // Server side processing portion
        $columns = array(
            0 => '#',
            1 => 'title',
            2 => 'category',
            3 => 'lesson_and_section',
            4 => 'enrolled_student',
            5 => 'status',
            6 => 'price',
            7 => 'actions',
            8 => 'course_id'
        );

        // Coming from databale itself. Limit is the visible number of data
        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = "";
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->lazyload->count_all_courses($filter_data);
        $totalFiltered = $totalData;
        // This block of code is handling the search event of datatable
        if(empty($this->input->post('search')['value'])) {
            $courses = $this->lazyload->courses($limit, $start, $order, $dir, $filter_data);
        }
        else {
            $search = $this->input->post('search')['value'];
            $courses =  $this->lazyload->course_search($limit, $start, $search, $order, $dir, $filter_data);
            $totalFiltered = $this->lazyload->course_search_count($search);
        }

        // Fetch the data and make it as JSON format and return it.
        $data = array();
        if(!empty($courses)) {
            foreach ($courses as $key => $row) {
                $instructor_details = $this->user_model->get_all_user($row->user_id)->row_array();
                $category_details = $this->crud_model->get_category_details_by_id($row->sub_category_id)->row_array();
                $sections = $this->crud_model->get_section('course', $row->id);
                $lessons = $this->crud_model->get_lessons('course', $row->id);
                $enroll_history = $this->crud_model->enrol_history($row->id);

                $status_badge = "badge-success-lighten";
                if ($row->status == 'pending') {
                    $status_badge = "badge-danger-lighten";
                }elseif ($row->status == 'draft') {
                    $status_badge = "badge-dark-lighten";
                }

                $price_badge = "badge-dark-lighten";
                $price = 0;
                if ($row->is_free_course == null){
                    if ($row->discount_flag == 1) {
                        $price = currency($row->discounted_price);
                    }else{
                        $price = currency($row->price);
                    }
                }elseif ($row->is_free_course == 1){
                    $price_badge = "badge-success-lighten";
                    $price = get_phrase('free');
                }

                $view_course_on_frontend_url = site_url('home/course/'.rawurlencode(slugify($row->title)).'/'.$row->id);
                $edit_this_course_url = site_url('user/course_form/course_edit/'.$row->id);
                $section_and_lesson_url = site_url('user/course_form/course_edit/'.$row->id);

                if ($row->status == 'active' || $row->status == 'pending') {
                    $course_status_changing_action = "confirm_modal('".site_url('user/course_actions/draft/'.$row->id)."')";
                    $course_status_changing_message = get_phrase('mark_as_drafted');
                }else{
                    $course_status_changing_action = "confirm_modal('".site_url('user/course_actions/publish/'.$row->id)."')";
                    $course_status_changing_message = get_phrase('publish_this_course');
                }

                $delete_course_url = "confirm_modal('".site_url('user/course_actions/delete/'.$row->id)."')";
                $invite_students = "showAjaxModal('".site_url('modal/popup/invite_students/'.$row->id)."','".site_phrase('invite_students')."')";
                $action = '
                <div class="dropright dropright">
                <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
                </button>
                <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="'.$view_course_on_frontend_url.'" target="_blank">'.get_phrase("view_course_on_frontend").'</a></li>
                <li><a class="dropdown-item" href="'.$edit_this_course_url.'">'.get_phrase("edit_this_course").'</a></li>
                <li><a class="dropdown-item" href="'.$section_and_lesson_url.'">'.get_phrase("section_and_lesson").'</a></li>
                <li><a class="dropdown-item" href="javascript::" onclick="'.$course_status_changing_action.'">'.$course_status_changing_message.'</a></li>
                <li><a class="dropdown-item" href="javascript::"  onclick="'.$invite_students.'" id="students" >'.site_phrase('invite_students').'</a></li>
                <li><a class="dropdown-item" href="javascript::" onclick="'.$delete_course_url.'">'.get_phrase("delete").'</a></li>
                </ul>
                </div>
                ';

                $nestedData['#'] = $key+1;
                $user_label = ($instructor_details['is_mentor'] == '1') ? 'mentor': 'instructor' ;
                $nestedData['title'] = '<strong><a href="'.site_url('user/course_form/course_edit/'.$row->id).'">'.$row->title.'</a></strong><br>
                <small class="text-muted">'.get_phrase($user_label).': <b>'.$instructor_details['first_name'].' '.$instructor_details['last_name'].'</b></small>';

                $nestedData['category'] = '<span class="badge badge-dark-lighten">'.$category_details['name'].'</span>';

                $nestedData['lesson_and_section'] = '
                <small class="text-muted"><b>'.get_phrase('total_section').'</b>: '.$sections->num_rows().'</small><br>
                <small class="text-muted"><b>'.get_phrase('total_lesson').'</b>: '.$lessons->num_rows().'</small><br>';

                $nestedData['enrolled_student'] = '<small class="text-muted"><b>'.get_phrase('total_enrolment').'</b>: '.$enroll_history->num_rows().'</small>';

                $nestedData['status'] = '<span class="badge '.$status_badge.'">'.get_phrase($row->status).'</span>';

                $nestedData['price'] = '<span class="badge '.$price_badge.'">'.$price.'</span>';

                $nestedData['actions'] = $action;

                $nestedData['course_id'] = $row->id;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
    public function user_report($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        
        $page_data['page_name'] = 'user_report';
        $page_data['user_report'] = $this->crud_model->user_report();
        $page_data['page_title'] = get_phrase('user_report');

        $this->load->view('backend/index', $page_data);
    }

 // Upload file
 public function upload_assignment($course_id = "", $action = "") {
    if ($this->session->userdata('user_login') != true) {
        redirect(site_url('login'), 'refresh');
    }

    if ($action == 'add') {
        $this->crud_model->add_upload_assignment($course_id);
        $this->session->set_flashdata('flash_message', get_phrase('upload_assignment_has_been_added_successfully'));
    }
    
    redirect(site_url('user/course_form/course_edit/'.$course_id));
}

    public function assignment_reject($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        $this->crud_model->reject_assignment($param1);
        $this->session->set_flashdata('flash_message', get_phrase('report_return_with_suggestions'));
        redirect(site_url('user/user_report'), 'refresh');
    }

    public function assignment_approve($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        $this->db->where('id',$param1);
        $assignment_detail = $this->db->get('assignment')->row_array();
        $course_id = $assignment_detail['course_id'];
        $user_id = $assignment_detail['user_id'];
        $submission_date = date('d/m/Y',$assignment_detail['date_added']);
        $data = array();
        $data['rating'] = $this->input->post('rating');
        
        $courseDetails = $this->crud_model->get_course_by_id($course_id)->row_array();

        $userdata = array();
        $userdata[] = $this->db->get_where('users', array('id' => $user_id))->row_array();
        $this->db->where('course_id',$course_id);
        $this->db->where('requested_user_id',$user_id);
        $groupMembers = $this->db->get('master_project_requests');
        if ($groupMembers->num_rows() > 0) {
            $groupMembersArray = $groupMembers->result_array();
            foreach ($groupMembersArray as $key => $value) {
                $userdata[] = $this->db->get_where('users', array('email' => $value['email']))->row_array();
            }
        }
        $certificateArray = $certificateIds = array();

        foreach ($userdata as $key1 => $user_data) {
            $this->load->library('pdf');
            $pdf = new pdf();
            $this->dompdf->set_option('isHtml5ParserEnabled',true);
            $filename = $courseDetails['title'].'_'.date('ymd').rand(100,99999).'.pdf';
            $certificateArray[$user_data['id']] = $filename;
            $certificate_id = rand(30000,99999999);
            $certificateIds[$user_data['id']] = $certificate_id;
            $html = '<html><head></head><body>
            <div style="background:url(\'uploads/system/certificate-background.jpg\') no-repeat;width: 852px;height: 587px;">
         <div style="position: relative;left: 200px;top: 50px;width: 600px;">       
          <img src="uploads/system/academy-logo.jpg" style="width: 18%;" width="" height="">
                                <div style="font-family: sans-serif ;font-size: 18px;top: 10px;position: relative;">Certificate of Completion</div>
                                <div style="margin-bottom:0;margin-top:0.4rem; font-family: sans-serif ;font-size: 17px;top: 10px;position: relative; ">Project Title : '.$courseDetails['title'].'</div>
                                <div style="font-family: sans-serif ;font-size: 15px;top: 10px;position: relative;">Project was completed on  : '.$submission_date.'</div>
        
                                <p style="font-family: sans-serif;font-size: 15px;top: 10px;position: relative;">This is to certify that '.$user_data['first_name'].' '.$user_data['last_name'].' of '.$user_data['course'].' of '.$user_data['college'].' has successfully completed the project work mentioned above.</p>
                                <p style="font-family: sans-serif;font-size: 15px;top: 10px;position: relative;">The project was completed under the mentorship of '.$this->session->userdata('name').'. The project on evaluation fulfils all the stated criteria and the student’s findings are his/her original work.</p>
                                <p style="font-family: sans-serif;font-size: 15px;top: 10px;position: relative;">I hereby certify his/ her work was '.$data['rating'].' to the best of my knowledge.</p>
        
                                <div style="float:left;width:50%;font-family: sans-serif;font-size: 15px;top: 10px;position: relative;">
                                    <img src="uploads/system/vice-president-sign.jpg" style="width: 110px;"><br><br>Vice President (Projects)<br>
                                        Certificate ID: '.$certificate_id.'<br>
                                        Date of issue: '.date('d/m/Y').'
                                </div>
                                <div style="float:right;padding-top: 50px;"><img src="uploads/system/barcode-scan.jpg">
        </div>
            </div>
           </div>
        </body></html>';
           
            $this->dompdf->load_html($html);
            
            $this->dompdf->setPaper('A4', 'landscape');
            
            $this->dompdf->render();
            
            $pdfhtml = $this->dompdf->output();

            file_put_contents("uploads/certificates/".$filename,$pdfhtml);

            $this->email_model->send_certificate_attachment($course_id,$user_data['email'],"uploads/certificates/".$filename);
        }
        $data['certificate'] = json_encode($certificateArray);
        $data['certificate_id'] = json_encode($certificateIds);
        $data['suggestion'] = '';
        $this->crud_model->approve_assignment($param1,$data);
        $this->session->set_flashdata('flash_message', get_phrase('assignment_approved_successfully'));
        redirect(site_url('user/user_report'), 'refresh');
    }
    
    public function view_assignment($course_id) {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        $page_data['page_name'] = 'view_assignment';
        $page_data['view_assignment'] = $this->crud_model->view_assignment_by_course_id($course_id);
        $page_data['page_title'] = get_phrase('view_assignment');
        $this->load->view('backend/index', $page_data);
    }


    public function course_actions($param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == "add") {
            $course_id = $this->crud_model->add_course();
            redirect(site_url('user/course_form/course_edit/'.$course_id), 'refresh');

        }
        elseif ($param1 == "edit") {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->update_course($param2);

            // CHECK IF LIVE CLASS ADDON EXISTS, ADD OR UPDATE IT TO ADDON MODEL
            if (addon_status('live-class')) {
                $this->load->model('addons/Liveclass_model','liveclass_model');
                $this->liveclass_model->update_live_class($param2);
            }

            redirect(site_url('user/courses'), 'refresh');

        }
        elseif ($param1 == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->delete_course($param2);
            redirect(site_url('user/courses'), 'refresh');
        }
        elseif ($param1 == 'draft') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->change_course_status('draft', $param2);
            redirect(site_url('user/courses'), 'refresh');
        }
        elseif ($param1 == 'publish') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $this->crud_model->change_course_status('pending', $param2);
            redirect(site_url('user/courses'), 'refresh');
        }
    }

    public function course_form($param1 = "", $param2 = "") {

        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == 'add_course') {
            $page_data['languages'] = $this->crud_model->get_all_languages();
            $page_data['categories'] = $this->crud_model->get_categories();
            $page_data['page_name'] = 'course_add';
            $page_data['page_title'] = get_phrase('add_course');
            $this->load->view('backend/index', $page_data);

        }elseif ($param1 == 'course_edit') {
            $this->is_the_course_belongs_to_current_instructor($param2);
            $page_data['page_name'] = 'course_edit';
            $page_data['course_id'] =  $param2;
            $page_data['page_title'] = get_phrase('edit_course');
            $page_data['languages'] = $this->crud_model->get_all_languages();
            $page_data['categories'] = $this->crud_model->get_categories();
            $this->load->view('backend/index', $page_data);
        }
    }

    public function payout_settings($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == 'paypal_settings') {
            $this->user_model->update_instructor_paypal_settings($this->session->userdata('user_id'));
            $this->session->set_flashdata('flash_message', get_phrase('updated'));
            redirect(site_url('user/payout_settings'), 'refresh');
        }
        if ($param1 == 'stripe_settings') {
            $this->user_model->update_instructor_stripe_settings($this->session->userdata('user_id'));
            $this->session->set_flashdata('flash_message', get_phrase('updated'));
            redirect(site_url('user/payout_settings'), 'refresh');
        }

        $page_data['page_name'] = 'payment_settings';
        $page_data['page_title'] = get_phrase('payout_settings');
        $this->load->view('backend/index', $page_data);
    }

    public function sales_report($param1 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 != "") {
            $date_range                   = $this->input->get('date_range');
            $date_range                   = explode(" - ", $date_range);
            $page_data['timestamp_start'] = strtotime($date_range[0].' 00:00:00');
            $page_data['timestamp_end']   = strtotime($date_range[1].' 23:59:59');

        }else {
            $page_data['timestamp_start'] = strtotime(date("m/01/Y 00:00:00"));
            $page_data['timestamp_end']   = strtotime(date("m/t/Y 23:59:59"));
        }

        $page_data['payment_history'] = $this->crud_model->get_instructor_revenue($this->session->userdata('user_id'), $page_data['timestamp_start'], $page_data['timestamp_end']);
        $page_data['page_name'] = 'sales_report';
        $page_data['page_title'] = get_phrase('sales_report');
        $this->load->view('backend/index', $page_data);
    }

    public function preview($course_id = '') {
        if ($this->session->userdata('user_login') != 1)
        redirect(site_url('login'), 'refresh');

        $this->is_the_course_belongs_to_current_instructor($course_id);
        if ($course_id > 0) {
            $courses = $this->crud_model->get_course_by_id($course_id);
            if ($courses->num_rows() > 0) {
                $course_details = $courses->row_array();
                redirect(site_url('home/lesson/'.rawurlencode(slugify($course_details['title'])).'/'.$course_details['id']), 'refresh');
            }
        }
        redirect(site_url('user/courses'), 'refresh');
    }

    public function sections($param1 = "", $param2 = "", $param3 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param2 == 'add') {
            $this->is_the_course_belongs_to_current_instructor($param1);
            $this->crud_model->add_section($param1);
            $this->session->set_flashdata('flash_message', get_phrase('section_has_been_added_successfully'));
        }
        elseif ($param2 == 'edit') {
            $this->is_the_course_belongs_to_current_instructor($param1, $param3, 'section');
            $this->crud_model->edit_section($param3);
            $this->session->set_flashdata('flash_message', get_phrase('section_has_been_updated_successfully'));
        }
        elseif ($param2 == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($param1, $param3, 'section');
            $this->crud_model->delete_section($param1, $param3);
            $this->session->set_flashdata('flash_message', get_phrase('section_has_been_deleted_successfully'));
        }
        redirect(site_url('user/course_form/course_edit/'.$param1));
    }

    public function lessons($course_id = "", $param1 = "", $param2 = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        if ($param1 == 'add') {
            $this->is_the_course_belongs_to_current_instructor($course_id);
            $this->crud_model->add_lesson();
            $this->session->set_flashdata('flash_message', get_phrase('lesson_has_been_added_successfully'));
            redirect('user/course_form/course_edit/'.$course_id);
        }
        elseif ($param1 == 'edit') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $param2, 'lesson');
            $this->crud_model->edit_lesson($param2);
            $this->session->set_flashdata('flash_message', get_phrase('lesson_has_been_updated_successfully'));
            redirect('user/course_form/course_edit/'.$course_id);
        }
        elseif ($param1 == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $param2, 'lesson');
            $this->crud_model->delete_lesson($param2);
            $this->session->set_flashdata('flash_message', get_phrase('lesson_has_been_deleted_successfully'));
            redirect('user/course_form/course_edit/'.$course_id);
        }
        elseif ($param1 == 'filter') {
            redirect('user/lessons/'.$this->input->post('course_id'));
        }
        $page_data['page_name'] = 'lessons';
        $page_data['lessons'] = $this->crud_model->get_lessons('course', $course_id);
        $page_data['course_id'] = $course_id;
        $page_data['page_title'] = get_phrase('lessons');
        $this->load->view('backend/index', $page_data);
    }

    // Manage Quizes
    public function quizes($course_id = "", $action = "", $quiz_id = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($action == 'add') {
            $this->is_the_course_belongs_to_current_instructor($course_id);
            $this->crud_model->add_quiz($course_id);
            $this->session->set_flashdata('flash_message', get_phrase('quiz_has_been_added_successfully'));
        }
        elseif ($action == 'edit') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $quiz_id, 'quize');
            $this->crud_model->edit_quiz($quiz_id);
            $this->session->set_flashdata('flash_message', get_phrase('quiz_has_been_updated_successfully'));
        }
        elseif ($action == 'delete') {
            $this->is_the_course_belongs_to_current_instructor($course_id, $quiz_id, 'quize');
            $this->crud_model->delete_lesson($quiz_id);
            $this->session->set_flashdata('flash_message', get_phrase('quiz_has_been_deleted_successfully'));
        }
        redirect(site_url('user/course_form/course_edit/'.$course_id));
    }

    // Manage Quize Questions
    public function quiz_questions($quiz_id = "", $action = "", $question_id = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $quiz_details = $this->crud_model->get_lessons('lesson', $quiz_id)->row_array();

        if ($action == 'add') {
            $this->is_the_course_belongs_to_current_instructor($quiz_details['course_id'], $quiz_id, 'quize');
            $response = $this->crud_model->add_quiz_questions($quiz_id);
            echo $response;
        }

        elseif ($action == 'edit') {
            if($this->db->get_where('question', array('id' => $question_id, 'quiz_id' => $quiz_id))->num_rows() <= 0){
                $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_quiz_question'));
                redirect(site_url('user/courses'), 'refresh');
            }

            $response = $this->crud_model->update_quiz_questions($question_id);
            echo $response;
        }

        elseif ($action == 'delete') {
            if($this->db->get_where('question', array('id' => $question_id, 'quiz_id' => $quiz_id))->num_rows() <= 0){
                $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_quiz_question'));
                redirect(site_url('user/courses'), 'refresh');
            }

            $response = $this->crud_model->delete_quiz_question($question_id);
            $this->session->set_flashdata('flash_message', get_phrase('question_has_been_deleted'));
            redirect(site_url('user/course_form/course_edit/'.$quiz_details['course_id']));
        }
    }

    function manage_profile() {
        redirect(site_url('home/profile/user_profile'), 'refresh');
    }

    function invoice($payment_id = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $page_data['page_name'] = 'invoice';
        $page_data['payment_details'] = $this->crud_model->get_payment_details_by_id($payment_id);
        $page_data['page_title'] = get_phrase('invoice');
        $this->load->view('backend/index', $page_data);
    }


    function become_an_instructor() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        // CHEKING IF A FORM HAS BEEN SUBMITTED FOR REGISTERING AN INSTRUCTOR
        if (isset($_POST) && !empty($_POST)) {
            $this->user_model->post_instructor_application();
        }

        // CHECK USER AVAILABILITY
        $user_details = $this->user_model->get_all_user($this->session->userdata('user_id'));
        if ($user_details->num_rows() > 0) {
            $page_data['user_details'] = $user_details->row_array();
        }else{
            $this->session->set_flashdata('error_message', get_phrase('user_not_found'));
            $this->load->view('backend/index');
        }
        $page_data['page_name'] = 'become_an_instructor';
        $page_data['page_title'] = get_phrase('become_an_instructor');
        $this->load->view('backend/index', $page_data);
    }


    // PAYOUT REPORT
    public function payout_report() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        $page_data['page_name'] = 'payout_report';
        $page_data['page_title'] = get_phrase('payout_report');

        $page_data['payouts'] = $this->crud_model->get_payouts($this->session->userdata('user_id'), 'user');
        $page_data['total_pending_amount'] = $this->crud_model->get_total_pending_amount($this->session->userdata('user_id'));
        $page_data['total_payout_amount'] = $this->crud_model->get_total_payout_amount($this->session->userdata('user_id'));
        $page_data['requested_withdrawal_amount'] = $this->crud_model->get_requested_withdrawal_amount($this->session->userdata('user_id'));

        $this->load->view('backend/index', $page_data);
    }

    // HANDLED WITHDRAWAL REQUESTS
    public function withdrawal($action = "") {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }

        if ($action == 'request') {
            $this->crud_model->add_withdrawal_request();
        }

        if ($action == 'delete') {
            $this->crud_model->delete_withdrawal_request();
        }

        redirect(site_url('user/payout_report'), 'refresh');
    }
    // Ajax Portion
    public function ajax_get_video_details() {
        $video_details = $this->video_model->getVideoDetails($_POST['video_url']);
        echo $video_details['duration'];
    }

    // this function is responsible for managing multiple choice question
    function manage_multiple_choices_options() {
        $page_data['number_of_options'] = $this->input->post('number_of_options');
        $this->load->view('backend/user/manage_multiple_choices_options', $page_data);
    }

    // This function checks if this course belongs to current logged in instructor
    function is_the_course_belongs_to_current_instructor($course_id, $id = null, $type = null) {
        $course_details = $this->crud_model->get_course_by_id($course_id)->row_array();
        if ($course_details['user_id'] != $this->session->userdata('user_id')) {
            $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_course'));
            redirect(site_url('user/courses'), 'refresh');
        }

        if($type == 'section' && $this->db->get_where('section', array('id' => $id, 'course_id' => $course_id))->num_rows() <= 0){
            $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_section'));
            redirect(site_url('user/courses'), 'refresh');
        }
        if($type == 'lesson' && $this->db->get_where('lesson', array('id' => $id, 'course_id' => $course_id))->num_rows() <= 0){
            $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_lesson'));
            redirect(site_url('user/courses'), 'refresh');
        }
        if($type == 'quize' && $this->db->get_where('lesson', array('id' => $id, 'course_id' => $course_id))->num_rows() <= 0){
            $this->session->set_flashdata('error_message', get_phrase('you_do_not_have_right_to_access_this_quize'));
            redirect(site_url('user/courses'), 'refresh');
        }

    }

    public function ajax_sort_section() {
        $section_json = $this->input->post('itemJSON');
        $this->crud_model->sort_section($section_json);
    }
    public function ajax_sort_lesson() {
        $lesson_json = $this->input->post('itemJSON');
        $this->crud_model->sort_lesson($lesson_json);
    }
    public function ajax_sort_question() {
        $question_json = $this->input->post('itemJSON');
        $this->crud_model->sort_question($question_json);
    }

    // Mark this lesson as completed codes
    function save_course_progress() {
        $response = $this->crud_model->save_course_progress();
        echo $response;
    }

    public function student_courses() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }


        $page_data['selected_category_id']   = isset($_GET['category_id']) ? $_GET['category_id'] : "all";
        $page_data['selected_instructor_id'] = isset($_GET['instructor_id']) ? $_GET['instructor_id'] : "all";
        $page_data['selected_status']        = isset($_GET['status']) ? $_GET['status'] : "all";
        $page_data['selected_student']        = isset($_GET['student']) ? $_GET['student'] : "all";
        $filter_data['selected_mentor']        = $this->session->userdata('user_id');
        // Courses query is used for deciding if there is any course or not. Check the view you will get it
        $page_data['courses']                = $this->crud_model->filter_course_for_backend($page_data['selected_category_id'], $page_data['selected_instructor_id'], $page_data['selected_status'],'',$page_data['student'],'student_course',$filter_data['selected_mentor']);
        $page_data['status_wise_student_courses']    = $this->crud_model->get_status_wise_courses_for_mentor();
        $page_data['instructors']            = $this->user_model->get_instructor()->result_array();
        $page_data['page_name']              = 'student-courses-server-side';
        $page_data['categories']             = $this->crud_model->get_categories();
        $page_data['page_title']             = get_phrase('active_courses');
        $this->load->view('backend/index', $page_data);
    }

     // This function is responsible for loading the course data from server side for datatable SILENTLY
    public function get_student_courses() {
        if ($this->session->userdata('user_login') != true) {
            redirect(site_url('login'), 'refresh');
        }
        $courses = array();
        // Filter portion
        $filter_data['selected_category_id']   = $this->input->post('selected_category_id');
        $filter_data['selected_student']         = $this->input->post('selected_student');
        $filter_data['selected_status']        = $this->input->post('selected_status');
        $filter_data['selected_mentor']        = $this->session->userdata('user_id');
        // Server side processing portion
        $columns = array(
            0 => '#',
            1 => 'title',
            2 => 'category',
            5 => 'status',
            6 => 'student',
            7 => 'actions',
            8 => 'course_id'
        );

        // Coming from databale itself. Limit is the visible number of data
        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = "";
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->lazyload->count_all_courses($filter_data,'student_course');
        $totalFiltered = $totalData;

        // This block of code is handling the search event of datatable
        if(empty($this->input->post('search')['value'])) {
            $courses = $this->lazyload->courses($limit, $start, $order, $dir, $filter_data,'student_course');
        }
        else {
            $search = $this->input->post('search')['value'];
            $courses =  $this->lazyload->course_search($limit, $start, $search, $order, $dir, $filter_data,'student_course');
            $totalFiltered = $this->lazyload->course_search_count($search,'student_course');
        }

        // Fetch the data and make it as JSON format and return it.
        $data = array();
        if(!empty($courses)) {
            foreach ($courses as $key => $row) {
                $student_details = $this->user_model->get_all_user($row->user_id)->row_array();
                $category_details = $this->crud_model->get_category_details_by_id($row->sub_category_id)->row_array();
                $status_badge = "badge-success-lighten";
                if ($row->status == 'pending') {
                    $status_badge = "badge-danger-lighten";
                }elseif ($row->status == 'draft') {
                    $status_badge = "badge-dark-lighten";
                }

                $price_badge = "badge-dark-lighten";
                $price = 0;
                if ($row->is_free_course == null){
                    if ($row->discount_flag == 1) {
                        $price = currency($row->discounted_price);
                    }else{
                        $price = currency($row->price);
                    }
                }elseif ($row->is_free_course == 1){
                    $price_badge = "badge-success-lighten";
                    $price = get_phrase('free');
                }

                $view_course_on_frontend_url = site_url('home/student_course/'.rawurlencode(slugify($row->title)).'/'.$row->id);
                $edit_this_course_url = site_url('admin/student_course_form/course_edit/'.$row->id);

                if ($row->status == 'active') {
                    $course_status_changing_message = get_phrase('mark_as_pending');
                    $course_status_changing_action = "showAjaxModal('".site_url('modal/popup/mail_on_student_course_status_changing_modal/pending/'.$row->id.'/'.$filter_data['selected_category_id'].'/'.$filter_data['selected_instructor_id'].'/'.$filter_data['selected_price'].'/'.$filter_data['selected_status'])."', '".$course_status_changing_message."')";
                }else{
                    $course_status_changing_message = get_phrase('mark_as_active');
                    $course_status_changing_action = "showAjaxModal('".site_url('modal/popup/mail_on_student_course_status_changing_modal/active/'.$row->id.'/'.$filter_data['selected_category_id'].'/'.$filter_data['selected_instructor_id'].'/'.$filter_data['selected_price'].'/'.$filter_data['selected_status'])."', '".$course_status_changing_message."')";
                }
                $delete_course_url = "confirm_modal('".site_url('admin/student_course_actions/delete/'.$row->id)."')";
                $download_html = '';
                if (!empty($row->document)) {
                    $download_html = '<li><a class="dropdown-item" href="'.site_url('home/download_document/'.$row->document).'">'.get_phrase("download_document").'</a></li>';
                }
                $action = '
                <div class="dropright dropright">
                <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
                </button>
                <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="'.$view_course_on_frontend_url.'" target="_blank">'.get_phrase("view_course_on_frontend").'</a></li>
                <li><a class="dropdown-item" href="'.$edit_this_course_url.'">'.get_phrase("edit_this_course").'</a></li>
                '.$download_html.'
                <li><a class="dropdown-item" href="javascript::" onclick="'.$course_status_changing_action.'">'.$course_status_changing_message.'</a></li>
                <li><a class="dropdown-item" href="javascript::" onclick="'.$delete_course_url.'">'.get_phrase("delete").'</a></li>
                </ul>
                </div>
                ';
                $action = '
                <div class="dropright dropright">
                <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
                </button>
                <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="'.$view_course_on_frontend_url.'" target="_blank">'.get_phrase("view_course_on_frontend").'</a></li>
                '.$download_html.'
                </ul>
                </div>
                ';

                $nestedData['#'] = $key+1;

                $nestedData['title'] = '<strong><a href="'.site_url('admin/course_form/course_edit/'.$row->id).'">'.$row->title.'</a></strong>';
                

                $nestedData['category'] = '<span class="badge badge-dark-lighten">'.$category_details['name'].'</span>';

                $nestedData['status'] = '<span class="badge '.$status_badge.'">'.get_phrase($row->status).'</span>';

                $nestedData['student'] = '<strong>'.$student_details['first_name'].' '.$student_details['last_name'].'</strong>';

                $nestedData['actions'] = $action;

                $nestedData['course_id'] = $row->id;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    public function get_instructor_project_request(){
        if($this->session->userdata('is_instructor')) {
            $table = 'instructor_requested_projects';
            $email = $this->session->userdata('email');
            $this->db->select('student_course.*,instructor_requested_projects.requested_user_id,instructor_requested_projects.status as ins_status,instructor_requested_projects.id as request_id');
            $this->db->where('instructor_requested_projects.email',$email);
            $this->db->join('student_course','student_course.id = instructor_requested_projects.course_id','left');    
        }else{
            $table = 'add_mentor';
            $mentor_id = $this->session->userdata('user_id');
            $this->db->select('student_course.*,add_mentor.status as ins_status,add_mentor.id as request_id');
            $this->db->where('add_mentor.user_id',$mentor_id);
            $this->db->where('add_mentor.status != "waiting"');
            $this->db->join('student_course','student_course.id = add_mentor.course_id','left');
        }
        
        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $this->db->limit($limit,$start);
        if(!empty($this->input->post('search')['value'])) {
            $search = $this->input->post('search')['value'];
            $this->db->like('student_course.title',$search);
        }
        $project_request = $totalData = $this->db->get($table)->result();
        $totalFiltered = count($project_request);
        $data = array();
        if(!empty($project_request)) {
            foreach ($project_request as $key => $row) {
                $student_details = $this->user_model->get_all_user($row->user_id)->row_array();
                $category_details = $this->crud_model->get_category_details_by_id($row->sub_category_id)->row_array();
                $status_badge = "badge-success-lighten";
                if ($row->status == 'pending') {
                    $status_badge = "badge-danger-lighten";
                }elseif ($row->status == 'draft') {
                    $status_badge = "badge-dark-lighten";
                }

                $price_badge = "badge-dark-lighten";
                $price = 0;
                if ($row->is_free_course == null){
                    if ($row->discount_flag == 1) {
                        $price = currency($row->discounted_price);
                    }else{
                        $price = currency($row->price);
                    }
                }elseif ($row->is_free_course == 1){
                    $price_badge = "badge-success-lighten";
                    $price = get_phrase('free');
                }

                $view_course_on_frontend_url = site_url('home/student_course/'.rawurlencode(slugify($row->title)).'/'.$row->id);
                $edit_this_course_url = site_url('admin/student_course_form/course_edit/'.$row->id);

                if ($row->status == 'active') {
                    $course_status_changing_message = get_phrase('mark_as_pending');
                    $course_status_changing_action = "showAjaxModal('".site_url('modal/popup/mail_on_student_course_status_changing_modal/pending/'.$row->id.'/'.$filter_data['selected_category_id'].'/'.$filter_data['selected_instructor_id'].'/'.$filter_data['selected_price'].'/'.$filter_data['selected_status'])."', '".$course_status_changing_message."')";
                }else{
                    $course_status_changing_message = get_phrase('mark_as_active');
                    $course_status_changing_action = "showAjaxModal('".site_url('modal/popup/mail_on_student_course_status_changing_modal/active/'.$row->id.'/'.$filter_data['selected_category_id'].'/'.$filter_data['selected_instructor_id'].'/'.$filter_data['selected_price'].'/'.$filter_data['selected_status'])."', '".$course_status_changing_message."')";
                }
                $delete_course_url = "confirm_modal('".site_url('admin/student_course_actions/delete/'.$row->id)."')";
                $download_html = '';
                if (!empty($row->document)) {
                    $download_html = '<li><a class="dropdown-item" href="'.site_url('home/download_document/'.$row->document).'">'.get_phrase("download_document").'</a></li>';
                }
                $action = '
                <div class="dropright dropright">
                <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
                </button>
                <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="'.$view_course_on_frontend_url.'" target="_blank">'.get_phrase("view_course_on_frontend").'</a></li>
                <li><a class="dropdown-item" href="'.$edit_this_course_url.'">'.get_phrase("edit_this_course").'</a></li>
                '.$download_html.'
                <li><a class="dropdown-item" href="javascript::" onclick="'.$course_status_changing_action.'">'.$course_status_changing_message.'</a></li>
                <li><a class="dropdown-item" href="javascript::" onclick="'.$delete_course_url.'">'.get_phrase("delete").'</a></li>
                </ul>
                </div>
                ';
                $app_rej = '';
                if ($row->ins_status == 'pending') {
                   $student_project_reject = "confirm_modal('".site_url('user/reject_student_request/'.$row->request_id.'/'.$table)."')";
                   $student_project_approve = "confirm_modal('".site_url('user/accept_student_request/'.$row->request_id.'/'.$table)."')";
                   $app_rej = '<li><a class="dropdown-item" href="javascript::" onclick="'.$student_project_approve.'">'.get_phrase("accept").'</a></li><li><a class="dropdown-item" href="javascript::" onclick="'.$student_project_reject.'">'.get_phrase("reject").'</a></li>';
                }
                $action = '
                <div class="dropright dropright">
                <button type="button" class="btn btn-sm btn-outline-primary btn-rounded btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-dots-vertical"></i>
                </button>
                <ul class="dropdown-menu">
                <li><a class="dropdown-item" href="'.$view_course_on_frontend_url.'" target="_blank">'.get_phrase("view_course_on_frontend").'</a></li>
                '.$download_html.$app_rej.'
                </ul>
                </div>
                ';

                if ($row->ins_status == 'rejected') {
                    $action = '--';
                }
                $nestedData['#'] = $key+1;

                $nestedData['title'] = '<strong>'.$row->title.'</strong>';
                

                $nestedData['category'] = '<span class="badge badge-dark-lighten">'.$category_details['name'].'</span>';

                $nestedData['status'] = '<span class="badge '.$status_badge.'">'.get_phrase($row->status).'</span>';

                $nestedData['student'] = '<strong>'.$student_details['first_name'].' '.$student_details['last_name'].'</strong>';

                $nestedData['actions'] = $action;
                $nestedData['request_status'] = ucfirst($row->ins_status);
                $nestedData['course_id'] = $row->id;

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );
        echo json_encode($json_data);
    }

    public function accept_student_request($param1='',$table='instructor_requested_projects'){
        $this->db->where('id',$param1);

        $updater = array('status'=>'approved');
        if ($this->session->userdata('is_instructor')) {
            $updater['added_date'] = strtotime(date("Y-m-d H:i:s"));
        }else{
            $updater['last_modified'] = strtotime(date("Y-m-d H:i:s"));
        }
        $this->db->update($table,$updater);
        $this->session->set_flashdata('flash_message', get_phrase('request_has_been_accepted'));
        redirect(site_url('user/dashboard'));
    }

    public function reject_student_request($param1='',$table='instructor_requested_projects'){
        
        if ($this->session->userdata('is_instructor')) {
            $this->db->where('id',$param1);
            $this->db->delete($table);
            
        }else{
            $this->db->where('id',$param1);
            $updater = array('status'=>'rejected');
            $updater['last_modified'] = strtotime(date("Y-m-d H:i:s"));
            $this->db->update($table,$updater);

            $this->db->where('id',$param1);
            $course_detail = $this->db->get($table)->row_array();
            $this->send_approval_request_to_next_mentor($course_detail['course_id']);
        }
        $this->session->set_flashdata('flash_message', get_phrase('request_has_been_rejected'));
        redirect(site_url('user/dashboard'));
    }

    public function send_approval_request_to_next_mentor($course_id){
        $this->db->where('course_id',$course_id);
        $this->db->where('status','waiting');
        $this->db->order_by('id ASC');
        $mentor_course_detail = $this->db->get('add_mentor')->row_array();
        if (empty($mentor_course_detail)) {
            return true;
        }
        $this->db->select('email');
        $this->db->where('id',$mentor_course_detail['user_id']);
        $user_details = $this->db->get('users')->row_array();

        $this->email_model->sent_mail_to_mentor($course_id,$user_details['email']);

        $updater = array('status'=>'pending');
        $updater['last_modified'] = strtotime(date("Y-m-d H:i:s"));
        $this->db->where('id',$mentor_course_detail['id']);
        $this->db->update('add_mentor',$updater);

        return true;
    }

    public function invite_students(){
        $course_id = $this->input->post('course_id');
        $to_email = $this->input->post('to_email');
        $type = 'master';
        $to_email = array_filter($to_email);
        $mail_count= count($to_email);
        $table = 'course';

        for($i=0;$i<$mail_count;$i++)
        {
            $mail_id = TRIM($to_email[$i]);

            /*$this->db->where('email',$mail_id);
            $this->db->where('course_id',$course_id);

            if($type == 'master'){
                $dataExists = $this->db->get('master_project_requests')->row_array();
                if(empty($dataExists)){
                    $this->db->where('email',$mail_id);
                    $userData = $this->db->get('users')->row_array();
                    $this->db->where('user_id',$userData['id']);
                    $this->db->where('course_id',$course_id);  
                    $dataExists = $this->db->get('enrol')->row_array();  // Check student was already enrol in this project or not (self).                
                }
            }else{
                $dataExists = $this->db->get('requested_projects')->row_array();    
            }
            
            if(!empty($dataExists)) continue; // If student is already enrol then do not send *///invitation
            $this->email_model->sent_mail_to_friends($course_id,$mail_id,$table);
            /*$data = array();
            $data['course_id'] = $course_id;
            $data['email'] = $mail_id;
            $data['requested_user_id'] = $this->session->userdata('user_id');
            $data['status'] = 'pending';
            $data['added_date'] = strtotime(date("Y-m-d H:i:s"));
            $this->user_model->add_master_invited_friend($data);  */
        }
        $this->session->set_flashdata('flash_message', site_phrase('mail_successfully_sent_to_students'));
        redirect(site_url('user/courses'), 'refresh');        
    }
}
