
<h2><?php  echo $this->session->flashdata('flash_message'); ?>
<?php  echo $this->session->flashdata('error_message'); ?>
</h2>

<!-- ACTUAL LESSON ADDING FORM -->
<form action="<?php echo site_url('admin/user_upload_assignment/'); ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="lesson_id" value="<?php echo $lesson_details['id']; ?>"> 
    <input type="hidden" name="lesson_url" value="<?php echo $lesson_url; ?>">  
    <input type="hidden" name="course_id" value="<?php echo $course_id; ?>">
    <?php echo get_phrase('upload_your_assignment'); ?><br><br>
                 <input type="file" id="attachment" name="attachment" ><br><br>


    <div class="text-center">
        <button class = "btn btn-success" type="submit" name="button"><?php echo get_phrase('upload_assignment'); ?></button>
    </div>
</form>
