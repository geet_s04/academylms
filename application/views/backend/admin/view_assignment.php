<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('view_assignment'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('view_assignments_for'); ?></h4>
              <div class="table-responsive-sm mt-4">
                  <?php if (count($view_assignment->result_array()) > 0): ?>
                      <table class="table table-striped table-centered mb-0">
                          <thead>
                              <tr>
                                  <th><?php echo get_phrase('lesson'); ?></th>
                                  <th><?php echo get_phrase('submission_date'); ?></th>
                                  <th><?php echo get_phrase('status'); ?></th>
                                  <th><?php echo get_phrase('actions'); ?></th>
                                  <th><?php echo get_phrase('download_assignment'); ?></th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach ($view_assignment->result_array() as $assign):
                                  $user_data = $this->db->get_where('users', array('id' => $assign['user_id']))->row_array();
                                  $lesson_data = $this->db->get_where('lesson', array('id' => $assign['lesson_id']))->row_array();?>
                                  <tr class="gradeU">
                                      
                                      <td>
                                          <b><?php echo $lesson_data['title'];?></b><br>
                                      </td>
                                      <td><?php echo date('D, d-M-Y', $assign['date_added']); ?></td>
                                      <td><?php 
                                      if($assign['status'] == 0){
                                          echo get_phrase('pending');
                                      }elseif($assign['status'] == 1){
                                            echo get_phrase('approved');
                                      }else{
                                        echo get_phrase('rejected');
                                      } ?></td>
                                      <td>
                                          <button type="button" class="btn btn-sm btn-outline-success" onclick="approve_modal('<?php echo site_url('admin/assignment_approve/'.$assign['id']); ?>');"> <i class="mdi mdi-check"></i> </button>
                                          <button type="button" class="btn btn-sm btn-outline-danger" onclick="showAjaxModal('<?php echo site_url('modal/popup/suggestion_add/'.$assign['id']); ?>', '<?php echo get_phrase('add_reject_suggestion'); ?>')"> <i class="dripicons-trash"></i> </button>
                                      </td>
                                      <td>
                                          <a href="<?php echo site_url("uploads/user_files/".$assign['attachment'])?>" target="_blank">  <button type="button" class="btn btn-outline-primary btn-icon btn-rounded btn-sm" "> Download </button></a>
                                      </td>
                                  </tr>
                              <?php endforeach; ?>
                          </tbody>
                      </table>
                  <?php endif; ?>
                  <?php if (count($view_assignment->result_array()) == 0): ?>
                      <div class="img-fluid w-100 text-center">
                        <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                        <?php echo get_phrase('no_data_found'); ?>
                      </div>
                  <?php endif; ?>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<script type="text/javascript">
    function update_date_range()
    {
        var x = $("#selectedValue").html();
        $("#date_range").val(x);
    }
</script>
