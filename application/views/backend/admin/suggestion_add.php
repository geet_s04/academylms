<form action="<?php echo site_url('admin/assignment_reject/'.$param2); ?>" method="post">
    <div class="form-group">
        <textarea class="form-control" name="suggestion" id="suggestion"></textarea>
        <small class="text-muted"><?php echo get_phrase('give_suggestion_for_reject'); ?></small>
    </div>
    <div class="text-right">
        <button class = "btn btn-success" type="submit" name="button"><?php echo get_phrase('submit'); ?></button>
    </div>
</form>
