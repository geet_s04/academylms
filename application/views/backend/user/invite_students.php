<form action="<?php echo site_url('user/invite_students'); ?>" method="post">
    <div class="form-group">
       <div id = "requirement_area">
        <div class="d-flex mt-2">
            <div class="flex-grow-1 px-3">
                <div class="form-group">
                    <input type="email" class="form-control" name="to_email[]" id="requirements" placeholder="<?php echo get_phrase('add_email'); ?>" required>
                    <input type="hidden" id="data" name="data" value="1" />
                </div>
            </div>
            <div class="">
                <button type="button" class="btn btn-success btn-sm" style="" name="button" onclick="appendRequirement()"> <i class="fa fa-plus"></i> </button>
            </div>
        </div>
        <div id = "blank_requirement_field">
            <div class="d-flex mt-2">
                <div class="flex-grow-1 px-3">
                    <div class="form-group">
                        <input type="email" class="form-control" name="to_email[]" id="requirements" placeholder="<?php echo get_phrase('add_email'); ?>">
                    </div>
                </div>
                <div class="">
                    <button type="button" class="btn btn-danger btn-sm" style="margin-top: 0px;" name="button" onclick="removeRequirement(this)"> <i class="fa fa-minus"></i> </button>
                </div>
            </div>
        </div>
    </div>
        <input type="hidden" name="course_id" value="<?php echo $param2; ?>" />
        <input type="hidden" name="type" value="<?php echo $param3; ?>" />
        <small class="text-muted"><?php echo get_phrase('provide_email_of_students'); ?></small>
    </div>
    <div class="text-right">
        <button class = "btn btn-success" type="submit" name="button"><?php echo get_phrase('submit'); ?></button>
    </div>
</form>
<script type="text/javascript">
var blank_requirement = jQuery('#blank_requirement_field').html();
var data = 2;
function appendRequirement() {
    if(data < 6){
  jQuery('#requirement_area').append(blank_requirement);
  data = data + 1;
    }
}
function removeRequirement(requirementElem) {
  jQuery(requirementElem).parent().parent().remove();
  data = data - 1;
}

</script>