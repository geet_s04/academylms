<form method="post" id="emailList" action="" style="width: 450px;">
    <div class="form-group">
       <div id = "Email_area">
        <div class="mt-2">
            <input type="hidden" name="course_id" id="course_id" value="<?php echo $param2; ?>">
            <div class="form-group row">
                <div class="col-md-4">
                    <input type="text" class="form-control" name="to_name[]" id="Emails" placeholder="<?php echo get_phrase('name'); ?>" value="<?php echo $this->session->userdata('name'); ?>" required disabled>
                </div>
                <div class="col-md-4">
                    <input type="email" class="form-control" name="to_email[]" id="Emails" placeholder="<?php echo get_phrase('email'); ?>" value="<?php echo $this->session->userdata('email'); ?>" required disabled>
                </div>
                <div class="col-md-3">
                    <input type="text" class="form-control" name="to_phone[]" id="phone" pattern="[0-9]{10}" placeholder="<?php echo get_phrase('phone'); ?>" value="--" required disabled>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-success btn-sm" style="" name="button" onclick="appendEmail()"> <i class="fa fa-plus"></i> </button>
                </div>
            </div>
        </div>
        <div id = "blank_Email_field">
            <div class="mt-2">
                <div class="form-group row">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="to_name[]" id="Emails" placeholder="<?php echo get_phrase('name'); ?>" required>
                    </div>
                    <div class="col-md-4">
                        <input type="email" class="form-control emailClass" name="to_email[]" id="Emails" placeholder="<?php echo get_phrase('email'); ?>" required>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="to_phone[]" id="phone" pattern="[0-9]{10}" placeholder="<?php echo get_phrase('phone'); ?>" required>
                    </div>
                    <div class="col-md-1">
                        <button type="button" class="btn btn-success btn-sm" style="" name="button" onclick="removeEmail(this)"> <i class="fa fa-minus"></i> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <small class="text-muted"><?php echo get_phrase('provide_details_of_your_friend'); ?></small>
    </div>
    <div class="text-right">
        <button class = "btn btn-success showprice" onclick="gropPurchaseAction('showprice'); return false;"><?php echo get_phrase('show_price'); ?></button>
        <?php 
        $action = 'add_to_cart';
        $previous_cart_items = $this->session->userdata('cart_items');
        if (in_array($param2, $previous_cart_items)) {
            $action = 'added_to_cart'; ?>
                <a class = "btn btn-success buynow" href="<?php echo site_url('home/shopping_cart'); ?>"><?php echo get_phrase('buy_now'); ?></a>
            <?php
        }else { ?>
            <button class = "btn btn-success buynow" onclick="gropPurchaseAction('buynow');"><?php echo get_phrase('buy_now'); ?></button>
        <?php } ?>
        <button class = "btn btn-success addtocart" onclick="gropPurchaseAction('<?php echo $action; ?>');"><?php echo get_phrase($action); ?></button>
    </div>
    <div class="text-right mt-3">
        <div class='courseprice'></div>
    </div>
</form>
<script type="text/javascript">
var blank_Email = jQuery('#blank_Email_field').html();
var data = 1;
function appendEmail() {
    if(data < 6){
      jQuery('#Email_area').append(blank_Email);
      data = data + 1;
    }
}
function removeEmail(EmailElem) {
  jQuery(EmailElem).parent().parent().remove();
  data = data - 1;
  var showpricehtml = $('.courseprice').html();
  var addedmembers = $('.emailClass').length;
  if(showpricehtml != '' && addedmembers > 0) {
    gropPurchaseAction('showprice');
  }else if(addedmembers == 0) {
    $('.courseprice').html('')
  }
}
function phonenumber()
{

  var phoneno = /^\d{10}$/;
  if((inputtxt.value.match(phoneno))
        {
      return true;
        }
      else
        {
        alert("message");
        return false;
        }
}
function gropPurchaseAction(action){
    var course_id = $('#course_id').val();
    var formdata = $('#emailList').serialize();

    $.ajax({
        url: '<?php echo site_url('home/group_purchase_action');?>',
        type : 'POST',
        data : $('#emailList').serialize() + "&action="+action,
        success: function(response)
        {
            if(action == 'showprice' && response != 'fail') {
                $('.courseprice').html(response);
            }else if (action == 'buynow' && response != 'fail') {
                window.location.replace("<?php echo site_url('home/shopping_cart'); ?>");
            }else if((action == 'add_to_cart' || action == 'added_to_cart') && response != 'fail'){
                $('#modal_ajax').modal('toggle'); 
            }
            return true;
        }
    });
    return false;
}

</script>