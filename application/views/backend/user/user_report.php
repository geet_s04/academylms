<div class="row ">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
                <h4 class="page-title"> <i class="mdi mdi-apple-keyboard-command title_icon"></i> <?php echo get_phrase('user_report'); ?></h4>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>

<div class="row">
    <div class="col-xl-12">
        <div class="card">
            <div class="card-body">
              <h4 class="mb-3 header-title"><?php echo get_phrase('user_reports'); ?></h4>

              <div class="table-responsive-sm mt-4">
                  <?php if (count($user_report) > 0): ?>
                      <table class="table table-striped table-centered mb-0">
                          <thead>
                              <tr>
                                  <th><?php echo get_phrase('course'); ?></th>
                                  <th><?php echo get_phrase('view_report'); ?></th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php 
                               foreach ($user_report as $enrol):
                                // $user_data = $this->db->get_where('users', array('id' => $enrol['user_id']))->row_array();
                                  $course_data = $this->db->get_where('course', array('id' => $enrol['id']))->row_array();?>
                                  <tr class="gradeU">
                                      
                                      <td><strong><a href="" target="_blank"><?php echo $course_data['title']; ?></a></strong></td>
                                      <td>
                                          <a href="<?php echo site_url('user/view_assignment/'.$course_data['id']); ?>">  <button type="button" class="btn btn-outline-danger btn-icon btn-rounded btn-sm" "> View </button></a>
                                      </td>
                                  </tr>
                              <?php
                            endforeach; ?>
                          </tbody>
                      </table>
                  <?php endif; ?>
                  <?php if (count($user_report) == 0): ?>
                      <div class="img-fluid w-100 text-center">
                        <img style="opacity: 1; width: 100px;" src="<?php echo base_url('assets/backend/images/file-search.svg'); ?>"><br>
                        <?php echo get_phrase('no_data_found'); ?>
                      </div>
                  <?php endif; ?>
              </div>
            </div> <!-- end card body-->
        </div> <!-- end card -->
    </div><!-- end col-->
</div>
<script type="text/javascript">
    function update_date_range()
    {
        var x = $("#selectedValue").html();
        $("#date_range").val(x);
    }
</script>
