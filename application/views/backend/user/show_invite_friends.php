<?php 
	$this->db->select('GROUP_CONCAT(email) as email_ids');
	$this->db->where('course_id',$param2);
    $this->db->where('status','approved');
    $this->db->where('requested_user_id',$param3);
    $table = 'requested_projects';
    if ($param4 == 'master') {
        $table = 'master_project_requests';
    }
    $all_email_ids = $this->db->get($table)->row_array();

    $email_list = $all_email_ids['email_ids'];
    $email_list_array = explode(',',$email_list);
    $email_string = "'".implode("','",$email_list_array)."'";
    $this->db->where(' email IN ('.$email_string.') OR id = '.$param3);
    $user_list = $this->db->get('users')->result_array();
    echo '<div class="row">';
    foreach($user_list as $user_info) {
 		echo '<div class="col-2  mb-2"></div><div class="col-3 mb-2 ">'.$user_info['first_name'].' '.$user_info['last_name'].'</div><div class="col-4 mb-2">('.$user_info['email'].')</div><div class="col-3 mb-2"></div>';
    }
    echo '</div>';
?>