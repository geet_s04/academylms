<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home'); ?>"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item">
                            <a href="#">
                                <?php echo $page_title; ?>
                            </a>
                        </li>
                    </ol>
                </nav>
                <h1 class="category-name">
                    <?php echo site_phrase('new_password'); ?>
                </h1>
            </div>
        </div>
    </div>
</section>
<section class="category-course-list-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9">
              <div class="user-dashboard-box mt-3">
                  <div class="user-dashboard-content w-100 login-form">
                      <div class="content-title-box">
                          <div class="title"><?php echo site_phrase('new_password'); ?></div>
                          <div class="subtitle"><?php echo site_phrase('enter_new_password'); ?>.</div>
                      </div>
                      <form action="<?php echo site_url('login/reset_new_password'); ?>" class="passwordform" method="post">
                          <div class="content-box">
                              <div class="basic-group">
                                  <div class="form-group">
                                      <label for="login-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo site_phrase('password'); ?>:</label>
                                      <input type="password" class="form-control password" name = "password" placeholder="<?php echo site_phrase('password'); ?>" value="" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="login-confirm-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo site_phrase('confirm_password'); ?>:</label>
                                      <input type="password" class="form-control confirm_password" name = "confirm_password" placeholder="<?php echo site_phrase('confirm_password'); ?>" value="" required>
                                      <span class="text-danger confirm_password_error d-none"><?php echo site_phrase('confirm_password_and_password_should_be_same'); ?></span>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                              <button type="submit" class="btn"><?php echo site_phrase('continue'); ?></button>
                          </div>
                          <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id'); ?>">
                      </form>
                  </div>
              </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  $(".passwordform").submit(function(e){
    var password = $('.password').val();
    var confirm_password = $('.confirm_password').val();

    if(confirm_password != password){
      $('.confirm_password_error').removeClass('d-none');
      return false;
    }
    return true;
  });
</script>
