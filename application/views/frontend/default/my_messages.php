<style type="text/css">
    /* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>

<?php
    $this->db->where('email = "'.$this->session->userdata('email').'" OR requested_user_id = '.$this->session->userdata('user_id'));
    $this->db->where('status','approved');
    $member_exists = $this->db->get('master_project_requests')->row_array();
    $instructor_list = $this->user_model->get_instructor_student_list()->result_array();
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo site_phrase('my_messages'); ?></h1>
                <?php echo get_user_links('my_messages'); ?>
            </div>
        </div>
    </div>
</section>


<section class="message-area">
    <div class="container">
        <div class="row no-gutters align-items-stretch">
            <div class="col-lg-5">
                <div class="message-sender-list-box">
                    <button class="btn compose-btn" type="button" id="NewMessage" onclick="NewMessage(event)">Compose</button>
                    <button class="btn compose-btn" type="button" id="NewGroup" onclick="NewGroup(event)">Create Group</button>
                    <hr>

                    <div class="tab">
    <?php $segment = $this->uri->segment(3, 0);
    $active = $class = '';
    $id = 'defaultOpen';
    if ($segment == 'group') {
        $display_style = 'style = "display:block;"';
        $active = 'active';
        $id = '';
    }

 ?>
  <button class="tablinks" onclick="openCity(event, 'London')" id="<?php echo $id; ?>">Messages</button>
  <button class="tablinks <?php echo $active; ?>" onclick="openCity(event, 'Paris')">Groups</button>
</div>

<div id="London" class="tabcontent">
 <ul class="message-sender-list">

                        <?php
                        $current_user = $this->session->userdata('user_id');
                        $this->db->where('sender', $current_user);
                        $this->db->or_where('receiver', $current_user);
                        $message_threads = $this->db->get('message_thread')->result_array();
                        foreach ($message_threads as $row):

                            // defining the user to show
                            if ($row['sender'] == $current_user)
                            $user_to_show_id = $row['receiver'];
                            if ($row['receiver'] == $current_user)
                            $user_to_show_id = $row['sender'];

                            $last_messages_details =  $this->crud_model->get_last_message_by_message_thread_code($row['message_thread_code'])->row_array();
                            ?>
                            <a href="<?php echo site_url('home/my_messages/read_message/'.$row['message_thread_code']); ?>">
                                <li>
                                    <div class="message-sender-wrap">
                                        <div class="message-sender-head clearfix">
                                            <div class="message-sender-info d-inline-block">
                                                <div class="sender-image d-inline-block">
                                                    <img src="<?php echo $this->user_model->get_user_image_url($user_to_show_id);?>" alt="" class="img-fluid">
                                                </div>
                                                <div class="sender-name d-inline-block">
                                                    <?php
                                                    $user_to_show_details = $this->user_model->get_all_user($user_to_show_id)->row_array();
                                                    echo $user_to_show_details['first_name'].' '.$user_to_show_details['last_name'];
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="message-time d-inline-block float-right"><?php echo date('D, d-M-Y', $last_messages_details['timestamp']); ?></div>
                                        </div>
                                        <div class="message-sender-body">
                                            <?php echo $last_messages_details['message']; ?>
                                        </div>
                                    </div>
                                </li>
                            </a>
                        <?php endforeach; ?>
                    </ul>
</div>
<div id="Paris" class="tabcontent" <?php echo $display_style; ?>>
  <ul class="message-sender-list">

                        <?php
                          $current_user = $this->session->userdata('user_id');
                     

                          $this->db->select('chat_group.*');
                          $this->db->from('chat_group');
                          $this->db->join('group_member', 'group_member.group_thread_code=chat_group.room_thread_code','inner');
                          $this->db->where('group_member.member_id', $current_user);
                          $query=$this->db->get();
                          $data=$query->result_array();


                        foreach ($data as $row):
                            $last_messages_details =  $this->crud_model->get_last_group_message_by_message_thread_code($row['room_thread_code'])->row_array();

                            ?>
                            <a href="<?php echo site_url('home/my_messages/group/'.$row['room_thread_code']); ?>">
                                <li>
                                    <div class="message-sender-wrap">
                                        <div class="message-sender-head clearfix">
                                            <div class="message-sender-info d-inline-block">
                                                <div class="sender-image d-inline-block">
                                                    <img src="<?php echo $this->user_model->get_user_image_url($user_to_show_id);?>" alt="" class="img-fluid">
                                                </div>
                                                <div class="sender-name d-inline-block">
                                                    <?php
                                                   
                                                    echo $row['group_name'];
                                                    ?>
                                                </div>
                                            </div>
                                            <?php if (!empty($last_messages_details)) { ?>
                                                <div class="message-time d-inline-block float-right"><?php echo date('D, d-M-Y', $last_messages_details['chat_date']); ?></div>    
                                            <?php } ?>
                                            
                                        </div>
                                        <div class="message-sender-body">
                                            <?php echo $last_messages_details['message']; ?>
                                        </div>
                                    </div>
                                </li>
                            </a>
                        <?php endforeach; ?>
                    </ul>
</div>
            
                </div>
            </div>
            <div class="col-lg-7">
                <div class="message-details-box" id = "toggle-1">
                    <?php 

                    $segment = $this->uri->segment(3, 0);

                    if($segment == 'group')
                    {
                        include 'group_inner.php';                        
                    }
                    else
                    {
                        include 'inner_messages.php';
                    }

                    ?>
                </div>
                <div class="message-details-box" id = "toggle-2" style="display: none;">
                    <div class="new-message-details"><div class="message-header">
                        <div class="sender-info">
                            <span class="d-inline-block">
                                <i class="far fa-user"></i>
                            </span>
                            <span class="d-inline-block"><?php echo site_phrase('new_message'); ?></span>
                        </div>
                    </div>
                    <form class="" action="<?php echo site_url('home/my_messages/send_new'); ?>" method="post">
                        <div class="message-body">
                            <div class="form-group">
                                <select class="form-control select2" name = "receiver">
                                    <?php foreach ($instructor_list as $instructor):
                                        if ($instructor['id'] == $this->session->userdata('user_id'))
                                            continue;
                                        ?>
                                        <option value="<?php echo $instructor['id']; ?>"><?php echo $instructor['first_name'].' '.$instructor['last_name']; ?></option>
                                    <?php endforeach; ?>
                                    <?php if(!empty($member_exists)): 
                                        include('get_friends_list.php');
                                     endif; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea name="message" class="form-control"></textarea>
                            </div>
                            <button type="submit" class="btn send-btn"><?php echo site_phrase('send'); ?></button>
                            <button type="button" class="btn cancel-btn" onclick = "CancelNewMessage(event)">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="message-details-box" id = "toggle-3" style="display: none;">
                    <div class="new-message-details"><div class="message-header">
                        <div class="sender-info">
                            <span class="d-inline-block">
                                <i class="far fa-user"></i>
                            </span>
                            <span class="d-inline-block"><?php echo site_phrase('new_group'); ?></span>
                        </div>
                    </div>
                    <form class="" action="<?php echo site_url('home/new_group'); ?>" method="post">
                        <div class="message-body">
                            <div class="form-group">
                                <input type="text" class="form-control" name="group_name" placeholder="Enter Group Name">
                            </div>

                            <div class="form-group">
                                <input type="text" name="tags" placeholder="Add Members" class="typeahead tm-input form-control tm-input-info"/>
                                <input type="hidden" class="instructor_ids" name="instructor_ids" />
                            </div>
                            <button type="submit" class="btn send-btn"><?php echo site_phrase('Create'); ?></button>
                            <button type="button" class="btn cancel-btn" onclick = "CancelNewMessage(event)">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>

<script type="text/javascript">
  $(document).ready(function () {
    var tagApi = $(".tm-input").tagsManager();
    jQuery(".typeahead").typeahead({
      name: 'tags',
      display: 'name',
      displayKey: 'name',
      source: function (query, process) {
        return $.get(geturl(), { query: query }, function (data) {
          data = $.parseJSON(data);
          return process(data);
        });
      },
      afterSelect :function (item){
        var fetch_val = $('.instructor_ids').val();
        if(fetch_val != '') 
            $('.instructor_ids').val(fetch_val+','+item.id);
        else
            $('.instructor_ids').val(item.id);
        tagApi.tagsManager("pushTag", item.name);
      }
    });
    function geturl(){
        var category = $('#sub_category_id :selected').text();
        var category_string = '';
        if (category != '' && category != 'undefined') {
            var category_string = '?category='+category;
        }
        <?php echo "var url = '" . site_url('home/search_all_name') . "';" ?>
        url = url+category_string;
        return url;
    }
    $('.tm-input').on('tm:spliced tm:popped', function () {
        var key = $(this).attr('tagidtoremove');
        key = arguments[2] - 1;
        var fetch_val = $('.instructor_ids').val();
        var fetch_array = fetch_val.split(',');
        removeItem(fetch_array, fetch_array[key]);
        $('.instructor_ids').val(fetch_array.join(','));
        
    });
})
function removeItem(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}
</script>


<script type="text/javascript">
function NewMessage(e){

    e.preventDefault();
    $('#toggle-1').hide();
    $('#toggle-2').show();
    $('#toggle-3').hide();
    //$('#NewMessage').removeAttr('onclick');
}

function NewGroup(e){

    e.preventDefault();
    $('#toggle-1').hide();
    $('#toggle-2').hide();
    $('#toggle-3').show();
    //$('#NewGroup').removeAttr('onclick');
}

function CancelNewMessage(e){

    e.preventDefault();
    $('#toggle-2').hide();
    $('#toggle-1').show();
    $('#toggle-3').hide();
    $('#NewMessage').attr('onclick','NewMessage(event)');
    $('#NewGroup').attr('onclick','NewGroup(event)');
}

</script>

<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>