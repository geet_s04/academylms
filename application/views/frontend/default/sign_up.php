<section class="category-header-area">
    <div class="container-lg">
        <div class="row">
            <div class="col">
                <nav>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo site_url('home'); ?>"><i class="fas fa-home"></i></a></li>
                        <li class="breadcrumb-item">
                            <a href="#">
                                <?php echo $page_title; ?>
                            </a>
                        </li>
                    </ol>
                </nav>
                <h1 class="category-name">
                    <?php echo site_phrase('register_yourself'); ?>
                </h1>
            </div>
        </div>
    </div>
</section>

<section class="category-course-list-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-9">
              <div class="user-dashboard-box mt-3">
                  <div class="user-dashboard-content w-100 login-form hidden">
                      <div class="content-title-box">
                          <div class="title"><?php echo site_phrase('login'); ?></div>
                          <div class="subtitle"><?php echo site_phrase('provide_your_valid_login_credentials'); ?>.</div>
                      </div>
                      <form action="<?php echo site_url('login/validate_login/user'); ?>" method="post">
                          <div class="content-box">
                              <div class="basic-group">
                                  <div class="form-group">
                                      <label for="login-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo site_phrase('email'); ?>:</label>
                                      <input type="email" class="form-control" name = "email" id="login-email" placeholder="<?php echo site_phrase('email'); ?>" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="login-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo site_phrase('password'); ?>:</label>
                                      <input type="password" class="form-control" name = "password" placeholder="<?php echo site_phrase('password'); ?>" required>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                              <button type="submit" class="btn"><?php echo site_phrase('login'); ?></button>
                          </div>
                          <div class="forgot-pass text-center">
                              <span>or</span>
                              <a href="javascript::" onclick="toggoleForm('forgot_password')"><?php echo site_phrase('forgot_password'); ?></a>
                          </div>
                          <div class="account-have text-center">
                              <?php echo site_phrase('do_not_have_an_account'); ?>? <a href="javascript::" onclick="toggoleForm('registration')"><?php echo site_phrase('sign_up'); ?></a>
                          </div>
                          <?php if(!empty($authUrl)) { ?>
                            <div class="text-center">
                              <span><?php echo site_phrase('or'); ?></span>
                              <a href="<?php echo $authUrl; ?>"><img src="<?php echo site_url('assets/frontend/default/img/google_sign_in.png') ?>"></a>
                            </div>
                          <?php } ?>
                      </form>
                  </div>
                  <div class="user-dashboard-content w-100 register-form">
                      <div class="content-title-box">
                          <div class="title"><?php echo site_phrase('registration_form'); ?></div>
                          <div class="subtitle"><?php echo site_phrase('sign_up_and_start_learning'); ?>.</div>
                      </div>
                      <form action="<?php echo site_url('login/register'); ?>" method="post" id="registerform">
                          <div class="content-box">
                              <div class="basic-group">
                                  <div class="form-group">
                                    <a href="<?php echo site_url('home/instructor_sign_up'); ?>"><?php echo site_phrase('sign_up'); ?></a>
                                      <label for="user_type" class="mr-3"><span class="input-field-icon"></span> <?php echo site_phrase('as_instructor'); ?> </label>
                                      <a href="<?php echo site_url('home/mentor_sign_up'); ?>"><?php echo site_phrase('sign_up'); ?></a>
                                      <label for="user_type" class="mr-3"><span class="input-field-icon"></span> <?php echo site_phrase('as_mentor'); ?> </label>
                                          
                                  </div>
                                  <div class="form-group">
                                      <label for="first_name"><span class="input-field-icon"><i class="fas fa-user"></i></span> <?php echo site_phrase('first_name'); ?>:</label>
                                      <input type="text" class="form-control" name = "first_name" id="first_name" placeholder="<?php echo site_phrase('first_name'); ?>" value="" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="last_name"><span class="input-field-icon"><i class="fas fa-user"></i></span> <?php echo site_phrase('last_name'); ?>:</label>
                                      <input type="text" class="form-control" name = "last_name" id="last_name" placeholder="<?php echo site_phrase('last_name'); ?>" value="" required>
                                  </div>
                                  <div class="form-group">
                                      <?php 
                                        $collageData = $this->crud_model->get_all_colleges();
                                        $collageList = $collageData->result_array();
                                      ?>
                                      <label for="college"><span class="input-field-icon"><i class="fas fa-building"></i></span> <?php echo site_phrase('name_of_college_/_university'); ?>:</label>
                                   <!-- <input type="text" class="form-control" name = "college" id="college" placeholder="<?php //echo site_phrase('name_of_college_/_university'); ?>" value="" required> -->
                                      <select class="form-control pb-1" name="college" required>
                                         <option value="">--Select--</option>
                                         <?php 
                                          foreach ($collageList as $key => $collage) {
                                            echo '<option value="'.$collage['name'].'">'.$collage['name'].'</option>';
                                          }
                                         ?>
                                       </select>
                                  </div>
                                  <div class="form-group">
                                      <label for="registration-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo site_phrase('email'); ?>:</label>
                                      <input type="email" class="form-control" name = "email" id="registration-email" placeholder="<?php echo site_phrase('email'); ?>" value="" required>
                                  </div>
                                  <div class="form-group">
                                      <label for="registration-password"><span class="input-field-icon"><i class="fas fa-lock"></i></span> <?php echo site_phrase('password'); ?>:</label>
                                      <input type="password" class="form-control" name = "password" id="registration-password" placeholder="<?php echo site_phrase('password'); ?>" value="" required>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                              <button type="submit" class="btn"><?php echo site_phrase('sign_up'); ?></button>
                              <p>or sign in with</p>
                          </div>
                          <?php if(!empty($authUrl)) { ?>
                            <div class="text-center">
                              <a href="<?php echo $authUrl; ?>"><img style="border: 2px solid #fcd21c;" src="<?php echo site_url('assets/frontend/'.get_frontend_settings('theme').'/img/google_sign_in.png') ?>"></a>
                            
                          <?php } ?>
                          <?php if(!empty($fbauthUrl)) { ?>
                              <a href="<?php echo $fbauthUrl; ?>"><img style="border: 2px solid #fcd21c;" src="<?php echo site_url('assets/frontend/'.get_frontend_settings('theme').'/img/fb_sign_in.png') ?>"></a>
                            
                          <?php } ?>
                          <?php if(!empty($oauthURL)) { ?>
                              <a href="<?php echo $oauthURL; ?>"><img style="border: 2px solid #fcd21c;" src="<?php echo site_url('assets/frontend/'.get_frontend_settings('theme').'/img/linkedin.png') ?>"></a>
                           <?php } ?>
                           </div>
                          <div class="account-have text-center">
                              <?php echo site_phrase('already_have_an_account'); ?>? <a href="javascript::" onclick="toggoleForm('login')"><?php echo site_phrase('login'); ?></a>
                          </div>
                      </form>
                  </div>

                  <div class="user-dashboard-content w-100 forgot-password-form hidden">
                      <div class="content-title-box">
                          <div class="title"><?php echo site_phrase('forgot_password'); ?></div>
                          <div class="subtitle"><?php echo site_phrase('provide_your_email_address_to_get_password'); ?>.</div>
                      </div>
                      <form action="<?php echo site_url('login/forgot_password/frontend'); ?>" method="post">
                          <div class="content-box">
                              <div class="basic-group">
                                  <div class="form-group">
                                      <label for="forgot-email"><span class="input-field-icon"><i class="fas fa-envelope"></i></span> <?php echo site_phrase('email'); ?>:</label>
                                      <input type="email" class="form-control" name = "email" id="forgot-email" placeholder="<?php echo site_phrase('email'); ?>" value="" required>
                                      <small class="form-text text-muted"><?php echo site_phrase('provide_your_email_address_to_get_password'); ?>.</small>
                                  </div>
                              </div>
                          </div>
                          <div class="content-update-box">
                              <button type="submit" class="btn"><?php echo site_phrase('reset_password'); ?></button>
                          </div>
                          <div class="forgot-pass text-center">
                              <?php echo site_phrase('want_to_go_back'); ?>? <a href="javascript::" onclick="toggoleForm('login')"><?php echo site_phrase('login'); ?></a>
                          </div>
                      </form>
                  </div>
              </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">

  $('input[name="user_type"]').change(function(e){
      var user_type = $(this).val();
      if (user_type == 'student') {
          $('.highest_qu,.expertise_area').hide();
      }else{
        $('.highest_qu,.expertise_area').show();
      }
  });
  $('.tagsInput').change(function(e){
  
  });
  function toggoleForm(form_type) {
    if (form_type === 'login') {
      $('.login-form').show();
      $('.forgot-password-form').hide();
      $('.register-form').hide();
    }else if (form_type === 'registration') {
      $('.login-form').hide();
      $('.forgot-password-form').hide();
      $('.register-form').show();
    }else if (form_type === 'forgot_password') {
      $('.login-form').hide();
      $('.forgot-password-form').show();
      $('.register-form').hide();
    }
  }
    $("#registerform").submit(function(e){
    var user_type = $('input[name="user_type"]:checked').val();
    var   in_valid = false;
    if(user_type == 'instructor'){
      $('.qualification').html('');
      $('.expertise').html('');
        var highest_qualification = $('#highest_qu').val();
        if (highest_qualification == "") {
          $('.qualification').show();
            $('.qualification').html('This field is required');
            in_valid = true;
        }else{
          $('.qualification').show();
          var total_qa_array = highest_qualification.split(',');
          var no_qualification = total_qa_array.length;
          if (no_qualification < 5) {
            $('.qualification').html('Minimum five input required');
            in_valid = true;
          }
        }
        var expertise_area = $('#expertise_area').val();
        if (expertise_area == "") {
          $('.expertise').show();
            $('.expertise').text('This field is required');
            in_valid = true;
        }else{
          $('.expertise').show();
          var expertise_area_array = expertise_area.split(',');
          var no_expertise_area = expertise_area_array.length;
          if (no_expertise_area < 5) {
            $('.expertise').html('Minimum five input required');
            in_valid = true;
          }
        }
        if(in_valid) return false;
    }
    return true;
  });
</script>
