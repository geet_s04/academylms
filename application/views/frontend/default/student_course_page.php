<?php
$course_details = $this->crud_model->get_student_course_by_id($course_id)->row_array();
$student_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
$student_names = '<ul><li>'.$student_details['first_name'].' '.$student_details['last_name'].'</li>';
if ($student_details['email'] != $this->session->userdata('email') && !$this->session->userdata('is_instructor') && !$this->session->userdata('is_mentor')) {
  check_for_student_project_access($course_id);
}
if ($this->session->userdata('is_instructor')) {
  check_for_instructor_project_access($course_id);
}
if ($this->session->userdata('is_mentor')) {
  check_for_mentor_project_access($course_id);
}
$enrol_students = $this->user_model->get_all_enrol_student_list($course_id)->result_array();
foreach ($enrol_students as $key => $value) {
  $student_names .= '<li>'.$value['student_name'].'</li>';
}
$student_names .= '</ul>';
?>
<section class="course-header-area">
  <div class="container">
    <div class="row align-items-end">
      <div class="col-lg-8">
        <div class="course-header-wrap">
          <h1 class="title"><?php echo $course_details['title']; ?></h1>
          <p class="subtitle"><?php echo $course_details['short_description']; ?></p>
          <div class="created-row">
            <span class="created-by">
              <?php echo site_phrase('created_by'); ?>
              <a href="<?php echo site_url('home/instructor_page/'.$course_details['user_id']); ?>"><?php echo $student_details['first_name'].' '.$student_details['last_name']; ?></a>
            </span>
            <?php if ($course_details['last_modified'] > 0): ?>
              <span class="last-updated-date"><?php echo site_phrase('last_updated').' '.date('D, d-M-Y', $course_details['last_modified']); ?></span>
            <?php else: ?>
              <span class="last-updated-date"><?php echo site_phrase('last_updated').' '.date('D, d-M-Y', $course_details['date_added']); ?></span>
            <?php endif; ?>
            <span class="comment"><i class="fas fa-comment"></i><?php echo ucfirst($course_details['language']); ?></span>
          </div>
        </div>
    </div>
    <div class="col-lg-4 d-none">
      <div class="course-sidebar natural"></div>
    </div>
  </div>
</div>
</section>
<section class="course-content-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">

        <div class="what-you-get-box">
          <div class="what-you-get-title"><?php echo site_phrase('what_will_i_learn'); ?>?</div>
          <ul class="what-you-get__items">
            <?php foreach (json_decode($course_details['outcomes']) as $outcome): ?>
              <?php if ($outcome != ""): ?>
                <li><?php echo $outcome; ?></li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
        </div>
        <br>
        <div class="course-curriculum-box">
          <div class="course-curriculum-title clearfix">
            <div class="title"><?php echo site_phrase('curriculum_for_this_course'); ?></div>
            <div class="details">
                <ul>
                <?php 
                if (!empty($course_details['course'])) {
                  echo '<li>'.site_phrase('course_label').' : '.$course_details['course'].'</li>';
                }
                if (!empty($course_details['semester'])) {
                  echo '<li>'.site_phrase('semester').' : '.$course_details['semester'].'</li>';
                }
                if (!empty($course_details['subject'])) {
                  echo '<li>'.site_phrase('subject').' : '.$course_details['subject'].'</li>';
                }
                if (!empty($course_details['document'])) {
                  $download_url = '<a href="'.site_url('home/download_document/'.$course_details['document']).'">'.$course_details['document'].'</a>';
                  echo '<li>'.site_phrase('document').' : '.$download_url.'</li>';
                } 
                ?>
              </ul>
            </div>
          </div>
        </div>
    <div class="requirements-box">
      <div class="requirements-title"><?php echo site_phrase('requirements'); ?></div>
      <div class="requirements-content">
        <ul class="requirements__list">
          <?php foreach (json_decode($course_details['requirements']) as $requirement): ?>
            <?php if ($requirement != ""): ?>
              <li><?php echo $requirement; ?></li>
            <?php endif; ?>
          <?php endforeach; ?>
        </ul>
      </div>
    </div>
    <?php if (!empty($enrol_students)): ?>
      <div class="course-curriculum-box mt-3">
        <div class="course-curriculum-title clearfix">
          <div class="title"><?php echo site_phrase('group_members'); ?></div>
          <div class="details"><?php echo $student_names; ?> </div>
        </div>
      </div>  
    <?php endif; ?>
    
    <div class="description-box view-more-parent">
      <div class="view-more" onclick="viewMore(this,'hide')">+ <?php echo site_phrase('view_more'); ?></div>
      <div class="description-title"><?php echo site_phrase('description'); ?></div>
      <div class="description-content-wrap">
        <div class="description-content">
          <?php echo $course_details['description']; ?>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
</div>
</div>
</section>
    <style media="screen">
    .embed-responsive-16by9::before {
      padding-top : 0px;
    }
    </style>
    <script type="text/javascript">
    function handleCartItems(elem) {
      url1 = '<?php echo site_url('home/handleCartItems');?>';
      url2 = '<?php echo site_url('home/refreshWishList');?>';
      $.ajax({
        url: url1,
        type : 'POST',
        data : {course_id : elem.id},
        success: function(response)
        {
          $('#cart_items').html(response);
          if ($(elem).hasClass('addedToCart')) {
            $(elem).removeClass('addedToCart')
            $(elem).text("<?php echo site_phrase('add_to_cart'); ?>");
          }else {
            $(elem).addClass('addedToCart')
            $(elem).text("<?php echo site_phrase('added_to_cart'); ?>");
          }
          $.ajax({
            url: url2,
            type : 'POST',
            success: function(response)
            {
              $('#wishlist_items').html(response);
            }
          });
        }
      });
    }

    function handleBuyNow(elem) {

      url1 = '<?php echo site_url('home/handleCartItemForBuyNowButton');?>';
      url2 = '<?php echo site_url('home/refreshWishList');?>';
      urlToRedirect = '<?php echo site_url('home/shopping_cart'); ?>';
      var explodedArray = elem.id.split("_");
      var course_id = explodedArray[1];

      $.ajax({
        url: url1,
        type : 'POST',
        data : {course_id : course_id},
        success: function(response)
        {
          $('#cart_items').html(response);
          $.ajax({
            url: url2,
            type : 'POST',
            success: function(response)
            {
              $('#wishlist_items').html(response);
              toastr.warning('<?php echo site_phrase('please_wait').'....'; ?>');
              setTimeout(
              function()
              {
                window.location.replace(urlToRedirect);
              }, 1500);
            }
          });
        }
      });
    }

    function handleEnrolledButton() {
      $.ajax({
        url: '<?php echo site_url('home/isLoggedIn');?>',
        success: function(response)
        {
          if (!response) {
            window.location.replace("<?php echo site_url('login'); ?>");
          }
        }
      });
    }

    function pausePreview() {
      player.pause();
    }
    </script>
