<?php if (!isset($group_thread_code)):?>
    <div class="text-center empty-box"><?php echo site_phrase('select_a_message_thread_to_read_it_here'); ?>.</div>
<?php endif; ?>
<?php
    if (isset($group_thread_code)):
    $message_thread_details = $this->db->get_where('chat_group', array('room_thread_code' => $group_thread_code))->row_array();
    $grp_member = $this->db->get_where('group_member', array('group_thread_code' => $group_thread_code))->result_array();
    // if ($this->session->userdata('user_id') == $message_thread_details['sender']){
    //     $user_to_show_id = $message_thread_details['receiver'];
    // }
    // else{
    //     $user_to_show_id = $message_thread_details['sender'];
    // }
    $user_to_show_details = $this->user_model->get_all_user($user_to_show_id)->row_array();
    $messages = $this->db->get_where('group_chat', array('group_thread_code' => $group_thread_code))->result_array();
    $user_names = array();
    ?>
    <div class="message-details d-show">
        <div class="message-header">
                <span class="sender-info">
                    <span class="d-inline-block">
                        <img src="<?php echo $this->user_model->get_user_image_url($user_to_show_id);?>" alt="">
                    </span>
                    <span class="d-inline-block" >
                    <?php echo $message_thread_details['group_name'];  ?>
                    <?php echo '<span style="float: right;padding-left: 20px;"><select class="form-control" name="user_mem" >'; 
                    echo '<option value="0" selected disabled>Group Members</option>';
                    foreach($grp_member as $member_name){
                        $mem = $this->user_model->get_user($member_name['member_id'],true)->row_array();
                        echo '<option value='.$mem['id'].'>'.$mem['first_name'].' '.$mem['last_name'].'</option>';
                    }
                    echo '</select></span>'; ?>
                </span>
                </span>
        </div>
        <div class="message-content">
            <?php foreach ($messages as $message): ?>
                <?php if ($message['sender'] == $this->session->userdata('user_id')): ?>
                    <div class="message-box-wrap me">
                        <div class="message-box">
                            <div class="user_name"><?php echo site_phrase('you'); ?></div>
                            <div class="time"><?php echo date('D, d-M-Y', $message['chat_date']); ?></div>
                            <div class="message"><?php echo $message['message']; ?></div>
                        </div>
                    </div>
                <?php else:
                    if (array_key_exists($message['sender'], $user_details)) {
                        $sender_name = $user_names[$message['sender']];
                    }else{
                        $user_details = $this->user_model->get_user($message['sender'])->row_array();
                    $user_names[$message['sender']] = $sender_name = $user_details['first_name'].' '.$user_details['last_name'];    
                    }
                 ?>
                    <div class="message-box-wrap">
                        <div class="message-box">
                            <div class="user_name"><?php echo $sender_name; ?></div>
                            <div class="time"><?php echo date('D, d-M-Y', $message['chat_date']); ?></div>
                            <div class="message"><?php echo $message['message']; ?></div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <div class="message-footer">
            <form class="" action="<?php echo site_url('home/my_messages/send_group_reply/'.$group_thread_code); ?>" method="post">
                <textarea class="form-control" name="message" placeholder="<?php echo site_phrase('type_your_message'); ?>..."></textarea>
                <button class="btn send-btn" type="submit"><?php echo site_phrase('send'); ?></button>
            </form>
        </div>
    </div>
<?php endif; ?>
