<style type="text/css">
    /* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
</style>

<?php
$my_courses = $this->user_model->get_requested_projects('','','master_project_requests')->result_array();
$requested_courses = $this->user_model->get_requested_projects_by_userid()->result_array();
$friends_requested_courses = $this->user_model->get_requested_projects_by_userid('','','master_project_requests')->result_array();
$student_courses = $this->user_model->get_requested_projects()->result_array();
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo site_phrase('project_request'); ?></h1>
                <?php echo get_user_links('requested_projects'); ?>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container">
    <div class="tab">
      <?php $segment = $this->uri->segment(3, 0);
      $active = $class = '';
      $id = 'defaultOpen';
      if ($segment == 'group') {
          $display_style = 'style = "display:block;"';
          $active = 'active';
          $id = '';
      }

  ?>
    <button class="tablinks" onclick="openCity(event, 'London')" id="<?php echo $id; ?>">Received</button>
    <button class="tablinks <?php echo $active; ?>" onclick="openCity(event, 'Paris')">Sent</button>
  </div>
  <div id="London" class="tabcontent">
        <div class="row no-gutters" id = "my_courses_area">
            <?php if(!empty($my_courses)): ?>
            <table class="table">
                <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>Project Title</th>
                      <th>View</th>
                      <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        $sr = 0;
                        foreach ($student_courses as $my_course):
                        $sr++;
                        $course_details = $this->crud_model->get_student_course_by_id($my_course['course_id'])->row_array();
                    ?>
                    <tr class="course_request_<?php echo $my_course['id'] ?>">
                      <td><?php echo $sr; ?></td>
                      <td><?php echo $course_details['title']; ?></td>
                      <td><?php echo '<a target="_blank" href="'.site_url('home/student_course/'.rawurlencode(slugify($course_details['title'])).'/'.$course_details['id']).'">View Project</a>'; ?></td>
                      <td class="action_row"><?php if($my_course['status'] == 'pending') { ?><button id="request_<?php echo $my_course['course_id']; ?>" class="btn-success accept_request" onclick="project_request(this.id,1)"><?php echo site_phrase('Accept')?></button><button id="request_<?php echo $my_course['course_id']; ?>" onclick="project_request(this.id,0)"  class="btn-danger ml-1 reject_request"><?php echo site_phrase('reject')?></button><?php }else{ echo site_phrase('accepted'); } ?></td>
                    </tr>
                    <?php endforeach;
                      foreach ($my_courses as $my_course):
                          $sr++;
                          $course_details = $this->crud_model->get_course_by_id($my_course['course_id'])->row_array();
                    ?>  
                      <tr class="course_request_<?php echo $my_course['id'] ?>">
                      <td><?php echo $sr; ?></td>
                      <td><?php echo $course_details['title']; ?></td>
                      <td><?php echo '<a target="_blank" href="'.site_url('home/course/'.rawurlencode(slugify($course_details['title'])).'/'.$course_details['id']).'">View Project</a>'; ?></td>
                      <td class="action_row"><?php if($my_course['status'] == 'pending') { ?><button id="request_<?php echo $my_course['course_id']; ?>" class="btn-success accept_request" onclick="project_master_request(this.id,1)"><?php echo site_phrase('Accept')?></button><button id="request_<?php echo $my_course['course_id']; ?>" onclick="project_master_request(this.id,0)"  class="btn-danger ml-1 reject_request"><?php echo site_phrase('reject')?></button><?php }else{ echo site_phrase('accepted'); } ?></td>
                    </tr>      
                  <?php endforeach; ?>
                </tbody>
            </table>
            <?php else: echo site_phrase('no_record_found'); ?>
          <?php endif;?>
        </div>
    </div>
    <div id="Paris" class="tabcontent">
    <div class="row no-gutters" id = "my_courses_area">
            <?php if(!empty($requested_courses)): ?>
            <table class="table">
                <thead>
                    <tr>
                      <th>Sr No.</th>
                      <th>Email</th>
                      <th>Project Title</th>
                      <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                   
                    <?php 
                    $sr = 0;
                      foreach ($requested_courses as $instructor_req_course):
                          $sr++;
                          $req_course_details = $this->crud_model->get_student_course_by_id($instructor_req_course['course_id'])->row_array();
                    ?>  
                      <tr class="course_request_<?php echo $instructor_req_course['id'] ?>">
                      <td><?php echo $sr; ?></td>
                      <td><?php echo $instructor_req_course['email']; ?></td>
                      <td><?php echo $req_course_details['title']; ?></td>
                      <td><?php echo $instructor_req_course['status'] ?></td>
                      
                    </tr>      
                  <?php endforeach; foreach ($friends_requested_courses as $friend_req_course):
                          $sr++;
                          $req_course_details1 = $this->crud_model->get_student_course_by_id($friend_req_course['course_id'])->row_array();
                    ?>  
                      <tr class="course_request_<?php echo $friend_req_course['id'] ?>">
                      <td><?php echo $sr; ?></td>
                      <td><?php echo $friend_req_course['email']; ?></td>
                      <td><?php echo $req_course_details1['title']; ?></td>
                      <td><?php echo $friend_req_course['status'] ?></td>
                      
                    </tr>  
                    <?php endforeach;?>
                </tbody>
            </table>
            <?php else: echo site_phrase('no_record_found'); ?>
          <?php endif;?>
        </div>
    </div>
    </div>
</section>


<script type="text/javascript">
function project_request(id,status) {
    var response = confirm('Are you sure you want to process this request?');
    if(response) {
      $.ajax({
          type : 'POST',
          url : '<?php echo site_url('home/approve_reject_requested_projects'); ?>',
          data : {course_id : id,status : status},
          success : function(response){
            location.reload();
          }
      });
    }
}
function project_master_request(id,status) {
  var response = confirm('Are you sure you want to process this request?');
    if(response) {
      $.ajax({
          type : 'POST',
          url : '<?php echo site_url('home/approve_reject_master_requested_projects'); ?>',
          data : {course_id : id,status : status},
          success : function(response){
            location.reload();
          }
      });
    }
}
</script>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>
