<?php
$instructor_details = $this->user_model->get_all_user($instructor_id)->row_array();
$social_links  = json_decode($instructor_details['social_links'], true);
$course_ids = $this->crud_model->get_instructor_wise_courses($instructor_id, 'simple_array');
?>
<section class="instructor-header-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="instructor-name"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></h1>
                <h2 class="instructor-title"><?php echo $instructor_details['title']; ?></h2>
            </div>
        </div>
    </div>
</section>

<section class="instructor-details-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="instructor-left-box text-center">
                    <div class="instructor-image">
                        <img src="<?php echo $this->user_model->get_user_image_url($instructor_details['id']);?>" alt="" class="img-fluid">
                    </div>
                    <div class="rating">
                    <?php
                        $total_rating =  $this->crud_model->get_ratings('user', $instructor_id, true)->row()->rating;
                        $number_of_ratings = $this->crud_model->get_ratings('user', $instructor_id)->num_rows();
                        if ($number_of_ratings > 0) {
                            $average_ceil_rating = ceil($total_rating / $number_of_ratings);
                        }else {
                            $average_ceil_rating = 0;
                        }
                        $student_rating = $this->crud_model->get_ratings('user', $instructor_id,false,$this->session->userdata('user_id'))->row_array();
                        
                        for($i = 1; $i < 6; $i++):?>
                        <?php if ($i <= $average_ceil_rating): ?>
                        <i class="fas fa-star filled"></i>
                        <?php else: ?>
                        <i class="fas fa-star"></i>
                        <?php endif; ?>
                        <?php endfor; ?>
                    <span class="d-inline-block average-rating"><?php echo $average_ceil_rating; ?></span>
                </div>
                <div class="rating-number">
                    <?php echo $student_rating['rating'].' '.site_phrase('ratings'); ?>
                </div>
                    <div class="instructor-social">
                        <ul>
                            <li><a href="<?php echo $social_links['twitter']; ?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="<?php echo $social_links['facebook']; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?php echo $social_links['linkedin']; ?>" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="instructor-right-box">

                    <div class="biography-content-box view-more-parent">
                        <div class="view-more" onclick="viewMore(this,'hide')"><b><?php echo site_phrase('show_full_biography'); ?></b></div>
                        <div class="biography-content mb-5">
                            <?php echo $instructor_details['biography']; ?>
                            <?php if($this->session->userdata('user_id') > 0) :?>
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <form class="" action="" method="post">
                                        <input type="hidden" name="instructor_id" value="<?php echo $instructor_id; ?>">
                                        <div class="form-group select">
                                            <div class="styled-select">
                                                <select class="form-control" name="star_rating" id="star_rating_of_course_<?php echo $course_details['id']; ?>">
                                                    <option value="1" <?php if ($student_rating['rating'] == 1): ?>selected=""<?php endif; ?>>1 <?php echo site_phrase('out_of'); ?> 5</option>
                                                    <option value="2" <?php if ($student_rating['rating'] == 2): ?>selected=""<?php endif; ?>>2 <?php echo site_phrase('out_of'); ?> 5</option>
                                                    <option value="3" <?php if ($student_rating['rating'] == 3): ?>selected=""<?php endif; ?>>3 <?php echo site_phrase('out_of'); ?> 5</option>
                                                    <option value="4" <?php if ($student_rating['rating'] == 4): ?>selected=""<?php endif; ?>>4 <?php echo site_phrase('out_of'); ?> 5</option>
                                                    <option value="5" <?php if ($student_rating['rating'] == 5): ?>selected=""<?php endif; ?>>5 <?php echo site_phrase('out_of'); ?> 5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group add_top_30 d-none">
                                            <textarea name="review" id ="review_of_a_course_<?php echo $course_details['id']; ?>" class="form-control" style="height:120px;" placeholder="<?php echo site_phrase('write_your_review_here'); ?>"><?php echo $user_specific_rating['review']; ?></textarea>
                                        </div>
                                        <button type="" class="btn btn-block" onclick="publishRating('<?php echo $course_details['id']; ?>')" name="button"><?php echo site_phrase('publish_rating'); ?></button>
                                    </form>
                                </div>
                                <div class="col-12 col-md-6"></div>
                            </div>
                        <?php endif; ?>
                        </div>
                    </div>

                    <div class="instructor-stat-box">
                        <ul>
                            <li>
                                <div class="small"><?php echo site_phrase('total_student'); ?></div>
                                <div class="num">
                                    <?php
                                    $this->db->select('user_id');
                                    $this->db->distinct();
                                    $this->db->where_in('course_id', $course_ids);
                                    echo $this->db->get('enrol')->num_rows();?>
                                </div>
                            </li>
                            <li>
                                <div class="small"><?php echo site_phrase('courses'); ?></div>
                                <div class="num"><?php echo sizeof($course_ids); ?></div>
                            </li>
                            <li>
                                <div class="small"><?php echo site_phrase('reviews'); ?></div>
                                <div class="num"><?php echo $this->crud_model->get_instructor_wise_course_ratings($instructor_id, 'course')->num_rows(); ?></div>
                            </li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
// Get selected rating
    $('.ratings:checked').each(function() {
        selectedRating = $(this).attr('value');
    });
</script>