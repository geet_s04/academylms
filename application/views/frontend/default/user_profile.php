<?php
    $social_links = json_decode($user_details['social_links'], true);
 ?>
 <section class="page-header-area my-course-area">
     <div class="container">
         <div class="row">
             <div class="col">
                 <h1 class="page-title"><?php echo site_phrase('user_profile'); ?></h1>
                 <?php echo get_user_links('user_profile'); ?>
             </div>
         </div>
     </div>
 </section>

<section class="user-dashboard-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="user-dashboard-box">
                    <div class="user-dashboard-sidebar">
                        <div class="user-box">
                            <img src="<?php echo base_url().'uploads/user_image/'.$this->session->userdata('user_id').'.jpg';?>" alt="" class="img-fluid">
                            <div class="name">
                                <div class="name"><?php echo $user_details['first_name'].' '.$user_details['last_name']; ?></div>
                            </div>
                        </div>
                        <div class="user-dashboard-menu">
                            <ul>
                                <li class="active"><a href="<?php echo site_url('home/profile/user_profile'); ?>"><?php echo site_phrase('profile'); ?></a></li>
                                <li><a href="<?php echo site_url('home/profile/user_credentials'); ?>"><?php echo site_phrase('account'); ?></a></li>
                                <li><a href="<?php echo site_url('home/profile/user_photo'); ?>"><?php echo site_phrase('photo'); ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="user-dashboard-content">
                        <div class="content-title-box">
                            <div class="title"><?php echo site_phrase('profile'); ?></div>
                            <div class="subtitle"><?php echo site_phrase('add_information_about_yourself_to_share_on_your_profile'); ?>.</div>
                        </div>
                        <form action="<?php echo site_url('home/update_profile/update_basics'); ?>" method="post" id="registerform">
                            <div class="content-box">
                                <div class="basic-group">
                                    <div class="form-group">
                                        <label for="FristName"><?php echo site_phrase('first_name'); ?>:</label>
                                        <input type="text" class="form-control" name = "first_name" id="FristName" placeholder="<?php echo site_phrase('first_name'); ?>" value="<?php echo $user_details['first_name']; ?>">
                                    </div>
                                    <div class="form-group">
                                    <label for="LastName"><?php echo site_phrase('last_name'); ?>:</label>
                                        <input type="text" class="form-control" name = "last_name" placeholder="<?php echo site_phrase('last_name'); ?>" value="<?php echo $user_details['last_name']; ?>">
                                    </div>
                                    <?php if($user_details['is_instructor'] == '1' || $user_details['is_mentor'] == '1') { ?>
                                    <div class="form-group highest_qu">
                                      <label for="highest_qu"><span class="input-field-icon"><i class="fa fa-graduation-cap"></i></span> <?php echo site_phrase('highest_qualification'); ?>:</label>
                                      <input type="text" class="form-control" name = "highest_qualification" id="highest_qu" placeholder="<?php echo site_phrase('highest_qualification'); ?>" value="<?php  echo $user_details['qualification']; ?>" required>
                                      <span class="text-danger mt-3 qualification hidden"></span>
                                    </div>
                                    <div class="form-group expertise_area">
                                      <label for="expertise_area"><span class="input-field-icon"><i class="fas fa-user"></i></span> <?php echo site_phrase('expertise_area'); ?>:</label>
                                      <input type="text" class="form-control tagsInput" name = "expertise_area" id="expertise_area" placeholder="<?php echo site_phrase('expertise_area'); ?>" value="<?php  echo $user_details['expertise_area']; ?>">
                                      <span class="text-danger mt-3 expertise hidden"></span>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label for="College"><?php echo site_phrase('name_of_college_/_university'); ?>:</label>
                                        <select class="form-control pb-1" name="college" required>
                                         <?php  if(!empty($user_details['college'])){
                                             $usc = $user_details['college'];
                                             ?>
                                         <option value="<?php echo $user_details['college']; ?>" selected disabled><?php echo $user_details['college']; ?></option> <?php } ?>
                                       <option value="">--Select--</option>
                                       <?php  foreach ($colleges->result_array() as $key => $college): ?>   
                                       <option value="<?php echo $college['name']; ?>"><?php echo $college['name']; ?></option>
                                        <?php endforeach; ?>
                                       </select>
                                    </div>
                                    <?php if(! ($this->session->userdata('is_instructor') || $this->session->userdata('is_mentor') )) : ?>
                                    <div class="form-group">
                                        <label for="Course"><?php echo site_phrase('name_of_course'); ?>:</label>
                                       <select class="form-control pb-1" name="course" required>
                                         <?php  if(!empty($user_details['course'])){
                                             $usc = $user_details['course'];
                                             ?>
                                         <option value="<?php echo $user_details['course']; ?>" selected disabled><?php echo $user_details['course']; ?></option> <?php } ?>
                                       <option value="">--Select--</option>
                                       <?php  foreach ($courses->result_array() as $key => $course): ?>   
                                       <option value="<?php echo $course['name']; ?>"><?php echo $course['name']; ?></option>
                                        <?php endforeach; ?>
                                       </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="Semester"><?php echo site_phrase('semester'); ?>:</label>
                                       <select class="form-control pb-1" name="semester" required>
                                         <?php  if(!empty($user_details['semester'])){
                                             $ssc = $user_details['semester'];
                                             ?>
                                         <option value="<?php echo $user_details['semester']; ?>" selected disabled><?php echo $user_details['semester']; ?></option> <?php } ?>
                                       <option value="">--Select--</option>
                                       <?php  foreach ($semesters->result_array() as $key => $semester): ?>   
                                       <option value="<?php echo $semester['name']; ?>"><?php echo $semester['name']; ?></option>
                                        <?php endforeach; ?>
                                       </select>
                                    </div>
                                  <?php endif; ?>
                                    <div class="form-group">
                                        <label for="Biography"><?php echo site_phrase('biography'); ?>:</label>
                                        <textarea class="form-control author-biography-editor" name = "biography" id="Biography"><?php echo $user_details['biography']; ?></textarea>
                                    </div>
                                </div>
                                <div class="link-group">
                                    <div class="form-group">
                                        <input type="text" class="form-control" maxlength="60" name = "twitter_link" placeholder="<?php echo site_phrase('twitter_link'); ?>" value="<?php echo $social_links['twitter']; ?>">
                                        <small class="form-text text-muted"><?php echo site_phrase('add_your_twitter_link'); ?>.</small>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" maxlength="60" name = "facebook_link" placeholder="<?php echo site_phrase('facebook_link'); ?>" value="<?php echo $social_links['facebook']; ?>">
                                        <small class="form-text text-muted"><?php echo site_phrase('add_your_facebook_link'); ?>.</small>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" maxlength="60" name = "linkedin_link" placeholder="<?php echo site_phrase('linkedin_link'); ?>" value="<?php echo $social_links['linkedin']; ?>">
                                        <small class="form-text text-muted"><?php echo site_phrase('add_your_linkedin_link'); ?>.</small>
                                    </div>
                                </div>
                            </div>
                            <div class="content-update-box">
                                <button type="submit" class="btn">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $("#registerform").submit(function(e){
    var in_valid = false;
    if($('.expertise').length == 1){
      //$('.qualification').html('');
      $('.expertise').html('');
        /*var highest_qualification = $('#highest_qu').val();
        if (highest_qualification == "") {
          $('.qualification').show();
            $('.qualification').html('This field is required');
            //in_valid = true;
        }else{
          $('.qualification').show();
          var total_qa_array = highest_qualification.split(',');
          var no_qualification = total_qa_array.length;
          if (no_qualification < 5) {
           // $('.qualification').html('Minimum five input required');
            //in_valid = true;
          }
        }*/
        var expertise_area = $('#expertise_area').val();
        if (expertise_area == "") {
          $('.expertise').show();
            $('.expertise').text('This field is required');
            in_valid = true;
        }else{
          $('.expertise').show();
          var expertise_area_array = expertise_area.split(',');
          var no_expertise_area = expertise_area_array.length;
          if (no_expertise_area < 5) {
            $('.expertise').html('Minimum five input required');
            in_valid = true;
          }
        }
        if(in_valid) return false;
    }
    return true;
  });
</script>