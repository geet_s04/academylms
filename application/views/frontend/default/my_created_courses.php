<?php
$my_enrol_courses = $this->user_model->my_enrol_student_courses()->result_array();
$my_courses = $this->user_model->my_student_courses('')->result_array();

$categories = array();
foreach ($my_courses as $my_course) {
    if (!in_array($my_course['category_id'], $categories)) {
        array_push($categories, $my_course['category_id']);
    }
}
?>
<section class="page-header-area my-course-area">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-title"><?php echo site_phrase('my_courses'); ?></h1>
                <?php echo get_user_links('my_created_projects'); ?>
            </div>
        </div>
    </div>
</section>

<section class="my-courses-area">
    <div class="container">
        <div class="row align-items-baseline">
            <div class="col-lg-6">
                <div class="my-course-filter-bar filter-box">
                    <span><?php echo site_phrase('filter_by'); ?></span>
                    <div class="btn-group">
                        <a class="btn btn-outline-secondary dropdown-toggle all-btn" href="#"data-toggle="dropdown">
                            <?php echo site_phrase('categories'); ?>
                        </a>

                        <div class="dropdown-menu">
                            <?php foreach ($categories as $category):
                                $category_details = $this->crud_model->get_categories($category)->row_array();
                                ?>
                                <a class="dropdown-item" href="#" id = "<?php echo $category; ?>" onclick="getCoursesByCategoryId(this.id)"><?php echo $category_details['name']; ?></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <!-- <div class="btn-group">
                        <a class="btn btn-outline-secondary dropdown-toggle" href="#"data-toggle="dropdown">
                            <?php echo site_phrase('instructors'); ?>
                        </a>

                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#"><?php echo $instructor_details['first_name'].' '.$instructor_details['last_name']; ?></a>

                        </div>
                    </div> -->
                    <div class="btn-group">
                        <a href="<?php echo site_url('home/my_created_courses'); ?>" class="btn reset-btn" disabled><?php echo site_phrase('reset'); ?></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="my-course-search-bar">
                    <form action="">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="<?php echo site_phrase('search_my_courses'); ?>" onkeyup="getCoursesBySearchString(this.value)">
                            <div class="input-group-append">
                                <button class="btn" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="row no-gutters" id = "my_courses_area">
            <?php foreach ($my_courses as $my_course):
                $course_details = $this->crud_model->get_student_course_by_id($my_course['id'])->row_array();
                $this->db->where('course_id',$my_course['id']);
                $this->db->where('email = "'.$this->session->userdata('email').'" OR requested_user_id = '.$this->session->userdata('user_id'));
                $member_exists = $this->db->get('requested_projects')->row_array();
                //$instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();?>

                <div class="col-lg-3 course_div_<?php echo $course_details['id']; ?>">
                    <div class="course-box-wrap">
                            <div class="course-box">
                                <a href="<?php echo site_url('home/student_course/'.rawurlencode(slugify($course_details['title'])).'/'.$course_details['id']) ?>">
                                    <div class="course-image">
                                        <img src="<?php echo $this->crud_model->get_student_course_thumbnail_url($my_course['id']); ?>" alt="" class="img-fluid">
                                        <span class="play-btn"></span>
                                    </div>
                                </a>

                                <div class="course_info_view" id = "course_info_view_<?php echo $my_course['course_id'];  ?>">
                                  <div class="course-details">
                                      <a href="<?php echo site_url('home/student_course/'.rawurlencode(slugify($course_details['title'])).'/'.$course_details['id']) ?>"><h5 class="title"><?php echo ellipsis($course_details['title']); ?></h5></a>
                                  </div>
                                  <div class="course-details">
                                  <h5 class="title">Status: <?php echo site_phrase($course_details['status']); ?></h5>
                                  </div>
                                  <div class="btn-group">
                                    <a class="btn btn-outline-secondary dropdown-toggle all-btn" href="#" data-toggle="dropdown"> <?php echo site_phrase('action'); ?> </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?php echo site_url('home/student_course/'.rawurlencode(slugify($course_details['title'])).'/'.$course_details['id']) ?>" id="view" ><?php echo site_phrase('view'); ?></a>
                                        <a class="dropdown-item" href="<?php echo site_url('home/edit_student_course/'.$course_details['id']) ?>" id="edit" ><?php echo site_phrase('edit'); ?></a>
                                        <a class="dropdown-item delete_course" href="" onclick="deleteCourse(this.id)"  href="#"  id="course_<?php echo $course_details['id']; ?>"><?php echo site_phrase('delete'); ?></a>
                                        <?php if(!empty($member_exists)): ?>
                                            <a class="dropdown-item" href="#"  onclick="showAjaxModal('<?php echo site_url('modal/popup/show_invite_friends/'.$course_details['id'].'/'.$this->session->userdata('user_id'));?>', '<?php echo site_phrase('group_members'); ?>')" id="friends" ><?php echo site_phrase('show_members'); ?></a>
                                        <?php endif; ?>
                                        <a class="dropdown-item" href="#"  onclick="showAjaxModal('<?php echo site_url('modal/popup/invite_friends/'.$course_details['id']);?>', '<?php echo site_phrase('invite_friends'); ?>')" id="friends" ><?php echo site_phrase('invite_friends'); ?></a>
                                        <a class="dropdown-item" href="#"  onclick="showAjaxModal('<?php echo site_url('modal/popup/invite_instructors/'.$course_details['id']);?>', '<?php echo site_phrase('invite_instructors'); ?>')" id="instructors" ><?php echo site_phrase('invite_instructors'); ?></a>
                                    </div>
                                  </div>
                                </div>
                            </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php if(!empty($my_enrol_courses)) : ?>
<section class="my-student-courses-area">
    <div class="container">
        <h3 class="mb-5"><?php echo site_phrase('requested_project') ?></h3>
        <div class="row no-gutters" id = "my_student_courses_area">
            <?php foreach ($my_enrol_courses as $my_course):
                $course_details = $this->crud_model->get_student_course_by_id($my_course['id'])->row_array();
                $instructor_details = $this->user_model->get_all_user($course_details['user_id'])->row_array();
                $this->db->where('course_id',$my_course['id']);
                $master_user_id = $my_course['user_id'];
                $this->db->where('email = "'.$this->session->userdata('email').'" OR requested_user_id = '.$this->session->userdata('user_id'));
                $member_exists = $this->db->get('requested_projects')->row_array();

                ?>

                <div class="col-lg-3 course_div_<?php echo $course_details['id']; ?>">
                    <div class="course-box-wrap">
                            <div class="course-box">
                                <a href="">
                                    <div class="course-image">
                                        <img src="<?php echo $this->crud_model->get_course_thumbnail_url($my_course['course_id']); ?>" alt="" class="img-fluid">
                                        <span class="play-btn"></span>
                                    </div>
                                </a>

                                <div class="course_info_view" id = "course_info_view_<?php echo $my_course['course_id'];  ?>">
                                  <div class="course-details">
                                      <a href=""><h5 class="title"><?php echo ellipsis($course_details['title']); ?></h5></a>
                                  </div>
                                  <div class="course-details">
                                  <h5 class="title">Status: <?php echo site_phrase($course_details['status']); ?></h5>
                                  </div>
                                  <div class="btn-group">
                                    <a class="btn btn-outline-secondary dropdown-toggle all-btn" href="#" data-toggle="dropdown"> <?php echo site_phrase('action'); ?> </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?php echo site_url('home/student_course/'.rawurlencode(slugify($course_details['title'])).'/'.$course_details['id']) ?>" id="view" ><?php echo site_phrase('view'); ?></a>
                                        <a class="dropdown-item" href="<?php echo site_url('home/edit_student_course/'.$course_details['id']) ?>" id="edit" ><?php echo site_phrase('edit'); ?></a>
                                        <?php if(!empty($member_exists)): ?>
                                            <a class="dropdown-item" href="#"  onclick="showAjaxModal('<?php echo site_url('modal/popup/show_invite_friends/'.$course_details['id'].'/'.$master_user_id);?>', '<?php echo site_phrase('group_members'); ?>')" id="friends" ><?php echo site_phrase('show_members'); ?></a>
                                        <?php endif; ?>
                                        <!--<a class="dropdown-item" href="#"  onclick="showAjaxModal('<?php echo site_url('modal/popup/invite_friends/'.$course_details['id']);?>', '<?php echo site_phrase('invite_friends'); ?>')" id="friends" ><?php echo site_phrase('invite_friends'); ?></a>
                                        <a class="dropdown-item" href="#"  onclick="showAjaxModal('<?php echo site_url('modal/popup/invite_instructors/'.$course_details['id']);?>', '<?php echo site_phrase('invite_instructors'); ?>')" id="instructors" ><?php echo site_phrase('invite_instructors'); ?></a> -->
                                    </div>
                                  </div>
                                </div>
                            </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<?php endif; ?>

<script type="text/javascript">
function deleteCourse(course_id){
    var response = confirm('Are you sure want to delete?');
    if(response){
        var course_id_sent = course_id.split('_')[1];
        $.ajax({
            type : 'POST',
            url : '<?php echo site_url('home/student_course_actions/delete'); ?>',
            data : {course_id : course_id_sent},
            success : function(response){
                $('.course_div_'+course_id_sent).remove();
            }
        });
    }
}
function getCoursesByCategoryId(category_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_student_courses_by_category'); ?>',
        data : {category_id : category_id},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCoursesBySearchString(search_string) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/my_student_courses_by_search_string'); ?>',
        data : {search_string : search_string},
        success : function(response){
            $('#my_courses_area').html(response);
        }
    });
}

function getCourseDetailsForRatingModal(course_id) {
    $.ajax({
        type : 'POST',
        url : '<?php echo site_url('home/get_course_details'); ?>',
        data : {course_id : course_id},
        success : function(response){
            $('#course_title_1').append(response);
            $('#course_title_2').append(response);
            $('#course_thumbnail_1').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_thumbnail_2').attr('src', "<?php echo base_url().'uploads/thumbnails/course_thumbnails/';?>"+course_id+".jpg");
            $('#course_id_for_rating').val(course_id);
            // $('#instructor_details').text(course_id);
            console.log(response);
        }
    });
}
</script>
